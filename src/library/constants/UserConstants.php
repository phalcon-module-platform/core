<?php

namespace PMP\Core\Library\Constants;

class UserConstants {

    /**
     * @var int User account is confirmed
     */
    const status_unknown = 0;

    /**
     * @var string User account is confirmed
     */
    const status_unknown_text = 'USER_ACCOUNT_STATUS_UNKNOWN';

    /**
     * @var int User account is confirmed
     */
    const status_active = 1;

    /**
     * @var string
     */
    const status_active_text = 'USER_ACCOUNT_STATUS_CONFIRMED';

    /**
     * 
     * @var int Reset , recover password status
     */
    const status_reset = 2;

    /**
     * 
     * @var string
     */
    const status_reset_text = 'USER_ACCOUNT_PASSWORD_RESET_SUCCESS';

    /**
     * @var int User account is disabled
     */
    const status_disabled = 3;

    /**
     * @var text
     */
    const status_disabled_text = 'USER_ACCOUNT_DISABLED';

    /** 
     * @var int User account is disabled
     */
    const status_register = 4;

    /**
     * @var text
     */
    const status_register_text = 'USER_ACCOUNT_TO_ACTIVATE';

    /**
     * @var int
     */
    const role_admin = 1;

    /**
     * @var int
     */
    const role_guest = 2;

    /**
     * @var int
     */
    const role_user = 3;

    /**
     * @var string
     */
    const role_admin_name = 'admin';

    /**
     * @var string
     */
    const role_admin_title = 'ROLE_TITLE_ADMIN';

    /**
     * @var string
     */
    const role_guest_name = 'guest';

    /**
     * @var string
     */
    const role_guest_title = 'ROLE_TITLE_GUEST';

    /**
     * @var string
     */
    const role_user_name = 'user';

    /**
     * @var string
     */
    const role_user_title = 'ROLE_TITLE_USER';

    /**
     * Get User Account status state as String
     * 
     * @param int $status Account status
     * @return string  Account status state 
     */
    public static function getAccountStatus($status = false) {

        $st = [
            self::status_active => self::status_active_text,
            self::status_disabled => self::status_disabled_text,
            self::status_reset => self::status_reset_text,
            self::status_register => self::status_register_text
        ];

        return !empty($st[$status]) ? $st[$status] : self::status_unknown_text;
    }

}
