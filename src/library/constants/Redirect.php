<?php

namespace PMP\Core\Library\Constants;

/**
 * Links
 */
class Redirect {
   
    /**
     * @var string Session Expire
     */
   const SE = '/'; 
   
    /**
     * @var string link to redirect after user login
     */
   const AFTER_LOGIN = 'dashboard'; 
    
    
}
