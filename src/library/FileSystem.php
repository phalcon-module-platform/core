<?php

namespace PMP\Core\Library;

use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config;
use PMP\Core\Config\SystemPaths;

/**
 * Files
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class FileSystem {

    /**
     * Creates a directory if does not exists
     * 
     * @param string $path
     * 
     * @return string the created directory
     */
    public static function makeDir($path) {

        if (!file_exists($path)) {

            mkdir($path, 0755, true);
        }

        return $path;
    }

    /**
     * Creates a directories from list
     * 
     * @param array $paths
     * 
     * @return array List with created directories
     */
    public static function makeDirs($paths = []) {

        $created = [];

        foreach ($paths as $dir) {

            if (file_exists($dir) && is_dir($dir)) {
                continue;
            }

            $create = mkdir($dir, 0755, true);

            if (empty($create)) {
                continue;
            }

            $created[] = $dir;
        }

        return $created;
    }

    /**
     * Write file with defined contents. Directory is created if not exists
     * 
     * @param string $filename Description
     * @param mixed $contents Description
     * @param bool when TRUE it append contents to existing file
     * 
     * @return int number of bytes written
     */
    public static function writeFile($filename, $contents = false, $append = false) {

        $dir = dirname($filename);

        self::makeDir($dir);

        return ($append === true) ?
                file_put_contents($filename, $contents, FILE_APPEND) :
                file_put_contents($filename, $contents);
    }

    /**
     * Pickup JSON file using Phalcon Adapter Json
     * 
     * @param string $file_path path to file with extension
     * 
     * @return \Phalcon\Config\Adapter\Json or empty Config
     */
    public static function readJsonFile($file_path = '') {

        $explode = explode('.', $file_path);

        $ext = end($explode);

        if (empty($ext) || $ext !== 'json') {
            return new Config([]);
        }

        if (!file_exists($file_path)) {
            return new Config([]);
        }

        return new ConfigJson($file_path);
    }

    /**
     * Checks if file exists and delete it
     * 
     * @param string $file
     */
    public static function deleteFile($file = false) {

        if (empty($file) || !file_exists($file)) {

            return false;
        }

        return unlink($file);
    }

    /**
     * Recursively scan directory and delete all files and directories
     * With security reason , the function working only in temp folder
     * 
     * @param string $directory
     * 
     * @return array list with deleted files and directories
     */
    public static function deleteDirFiles($directory, &$deleted = []) {

        if (empty($directory) || !is_dir($directory)) {

            return false;
        }

        $path = explode('/', $directory);

        if (!in_array(SystemPaths::STORAGE_FOLDER, $path)) {

            return false;
        }

        $files = new \DirectoryIterator($directory);

        /* @var $file \DirectoryIterator */
        foreach ($files as $file) {

            if ($file->isDot()) {
                continue;
            }

            if ($file->isDir()) {

                self::deleteDirFiles($file->getPathname(), $deleted);

                rmdir($file->getPathname());

                $deleted[] = $file->getPathname();
            }

            if ($file->isFile()) {

                unlink($file->getPathname());

                $deleted[] = $file->getPathname();
            }
        }

        return $deleted;
    }

}
