<?php

namespace PMP\Core\Library;

/**
 * Widget Manager Plugin
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class WidgetManager {

   /**
    * Loads Widget Class
    * 
    * @param mixed $widgetClass Classname or [Classname => StaticFunction]
    * @param array $parameters Class parameters
    */
   public static function load($widgetClass, $parameters = []) {


      if (is_array($widgetClass)) {

         $getclass = array_keys($widgetClass)[0];

         $className = \Phalcon\Text::camelize($getclass, '_');

         $function = array_values($widgetClass)[0];

         $class = __NAMESPACE__ . '\Widgets\\' . $className;

         return $class::$function($parameters);
         
      }

      $className = \Phalcon\Text::camelize($widgetClass, '_');

      return new Widgets . '\\' . $className($parameters);

   }

}
