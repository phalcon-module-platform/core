<?php

namespace PMP\Core\Library;

/**
 * Elements Attributes Manager Plugin
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class AttributesManager {

    /**
     * Resolve data attributes
     * 
     * @return array Data attributes
     */
    public static function resolveDataAttributes($dataAttributes = []) {

        $dataAttrArray = [];

        foreach ($dataAttributes as $key => $value) {

            $dataAttrArray['data-' . $key] = (is_array($value)) ? \json_encode($value, \JSON_NUMERIC_CHECK) : $value;
        }

        return $dataAttrArray;
    }

    /**
     * Search and add Tooltip or Popover attributes
     * 
     * @param array $options Button options
     * 
     * @return arrat data attributes
     */
    public static function resolveTooltipPopover($options = []) {

        if (!isset($options['data'])) {
            $options['data'] = [];
        }

        //add tooltip title
        if (!empty($options['tooltip'])) {

            $options['data']['original-title'] = !empty($options['tooltip']['title']) ?
                    $options['tooltip']['title'] :
                    'Tooltip';

            $options['data']['placement'] = !empty($options['tooltip']['placement']) ?
                    $options['tooltip']['placement'] :
                    'top';

            //
        } else if (!empty($options['popover'])) {

            !empty($options['popover']['title']) ?
                            $options['data']['title'] = $options['popover']['title'] :
                            false;

            !empty($options['popover']['content']) ?
                            $options['data']['content'] = $options['popover']['content'] :
                            false;

            $options['data']['toggle'] = 'popover-hover';

            $options['data']['placement'] = !empty($options['popover']['placement']) ?
                    $options['popover']['placement'] :
                    'top';
        }

        return $options['data'];
    }

    /**
     * Generate random number 
     * 
     * @param string $type id, name etc.
     */
    public static function randomIdName($type = '') {


        return $type . '-' . rand(999, 99999);
    }

}
