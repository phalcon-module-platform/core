<?php

namespace PMP\Core\Library;

class QueryTools {

    /**
     * Build query for Massive update base on array key => value
     * 
     * @param array $array Source array
     * @param array $options Option as column name for IN clause , column name for where clause and table name
     * 
     * - column_name column name to set 
     * - column_when  column name for IN clause
     * - column_where  column name for where clause
     * - table column name for IN clause
     * 
     * @return string Description
     * 
     */
    public static function queryMassiveUpdateFromArray($array, $options = []) {

        $update = "UPDATE " .$options['table'] . " SET " . $options['column_name'] . " = (CASE ";

        $ids = [];

        foreach ($array as $key => $value) {

            $update .= "WHEN " . $options['column_when'] . " = '" . $value . "' THEN '" . $key . "' ";

            $ids[] = "'" . $value . "'";
        }

        $update .= "END) WHERE " . $options['column_where'] . " IN(" . implode(',', $ids) . ") ";
        
        return $update;
    }

}
