<?php

/*
 * 
 */

namespace PMP\Core\Library;

class DateTimeCalculations {

    /**
     * Calculate time ago and feature  based on date
     * 
     * @param int $timestamp timestamp
     * 
     * @return string  eg. 1 year 2 days
     */
    public static function timeAgo($timestamp) {

        if (empty($timestamp)) {
            return false;
        }
        $time = time();

        $feature = $timestamp > $time;

        $etime = $feature ?
                $timestamp - $time :
                $time - $timestamp;

        $a = [
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        ];

        foreach ($a as $secs => $str) {
            $d = $etime / $secs;
            if ($d >= 1) {
                $r = round($d);
                return $feature ?
                        'after ' . $r . ' ' . $str . ($r > 1 ? 's' : '') :
                        $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            }
        }
        
    }

}
