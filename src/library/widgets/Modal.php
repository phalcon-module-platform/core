<?php

namespace PMP\Core\Library\Widgets;

use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Modal extends Tag {

   /**
    * Generate link to modal
    * @param array $options link options
    * 
    * <a href="javascript:;" class="my-modal" data-toggle="modal" data-backdrop="false" data-target="#my-modal">
    */
   public static function link($options) {

      $class = !empty($options['class']) ? $options['class'] : '';

      $attributes = [
          'class' => $class,
          'href' => 'javascript:;',
          'data-toggle' => 'modal',
          'data-target' => isset($options['target']) ? '#'.$options['target'] : '',
          'data-backdrop' => isset($options['backdrop']) ? 'true' : 'false'
      ];

      //add aditional data attributes if are set
      if (!empty($options['data'])) {
         $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
      }
      
      $text = !empty($options['text']) ? $options['text'] : 'Modal';

      return Tag::tagHtml('a', $attributes).$text.Tag::tagHtmlClose('a'); 

   }

}
