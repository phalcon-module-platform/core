<?php

namespace PMP\Core\Library\Widgets;

use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Panel extends Tag {

   /**
    * @var string Main panel class : panel panel-white
    */
   protected static $panel = 'panel panel-white card';

   /**
    * @var string Default border thinkes
    */
   protected static $borderColorThinkes = '500';

   /**
    * @var string Default panel title size
    */
   protected static $titleSize = 'h6';

   /**
    * @var string Default panel header class
    */
   protected static $classPanelHeader = 'panel-heading ';

   /**
    * @var string Default panel body class
    */
   protected static $classPanelBody = 'panel-body card-content ';

   /**
    * @var string Default panel footer class
    */
   protected static $classPanelFooter = 'panel-footer ';

   public function __construct($parameters = []) {
      
   }

   public static function start($options = []) {

      $class = self::$panel . ' ';
      $class .= !empty($options['borders']) ?
              self::panelBorders($options['borders'], $options['style']) :
              '';

      $class .= !empty($options['class']) ? ' ' . $options['class'] : '';

      $attributes = [
          'class' => $class
      ];

      if (!empty($options['data'])) {
         $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
      }

      return Tag::tagHtml('div', $attributes);

   }

   public static function end() {

      return Tag::tagHtmlClose('div');

   }

   /**
    * Start Panel Body section
    * 
    * @param array $options class and/or data attributes [ class:"custom class", "data":["url":"test/test","title":"Title"] ]
    * 
    * @return string Panel Body HTML section
    * 
    * <div class="panel-body custom class" data-url="test/test" data-title="Title">
    * 
    */
   public static function bodyStart($options = []) {

      $class = self::$classPanelBody;
      $class .= !empty($options['class']) ? ' ' . $options['class'] : '';

      $attributes = [
          'class' => $class
      ];

      if (!empty($options['data'])) {
         $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
      }

      return Tag::tagHtml('div', $attributes);

   }

   public static function bodyEnd() {

      return Tag::tagHtmlClose('div');

   }

   /**
    * Generate panel heading with elements inside
    * 
    * @param array $options Panel header options
    * - toggle : generating the click toggle inside the panel title.Used when grouping more than one panel with accordition controll
    * - actions : generating action buttons 
    * 
    * @return string Panel heading with or without elements
    * 
    * @example if [toggle] option is set eg. ['toggle':['parent':'#accordion-controls']]
    * <div class="panel-heading">
    *    <h6 class="panel-title">
    *       <a data-toggle="collapse" data-parent="#accordion-controls" href="#accordion-controls-group1">Panel Title</a>
    *    </h6>
    * </div>
    * 
    * @example if [actions] option is set. eg. ['actions':['collapse','reload','refresh']]
    * <div class="panel-heading ">
    *    <span class="panel-title h6">Panel Title</span>
    *    <div class="heading-element right-align">
    *       <ul class="icons-list">
    *          <li><a data-action="collapse"></a></li>
    *          <li><a data-action="reload"></a></li>
    *          <li><a data-action="refresh"></a></li>
    *       </ul>
    *    </div>
    * </div>
    * 
    */
   public static function heading($options = []) {

      $html = '';
      $html .= self::headerStart($options);

      //if toggle, generate the click toggle inside the panel title
      if (isset($options['toggle'])) {

         $html .= Tag::tagHtml(self::$titleSize, ['class' => 'panel-title']);

         $toggleAttr = [
             'data-toggle' => 'collapse',
             'class' => isset($options['collapsed']) ? 'collapsed' : '',
             'href' => !empty($options['href']) ? $options['href'] : 'javascript:;'
         ];

         if (!empty($options['toggle'])) {
            $toggleAttr = array_merge($toggleAttr, AttributesManager::resolveDataAttributes($options['toggle']));
         }

         $html .= Tag::tagHtml('a', $toggleAttr) . $options['title'] . Tag::tagHtmlClose('a');
         
         $html .= Tag::tagHtmlClose(self::$titleSize);
         
      } else if (isset($options['actions'])) {

         $html .= Tag::tagHtml('span', ['class' => 'panel-title ' . self::$titleSize]) . $options['title'] . Tag::tagHtmlClose('span');

         $html .= !empty($options['actions']) ? self::actionIcons($options['actions']) : '';
         
      } else if (isset($options['html'])) {

         $html .= $options['html'];
      }

      $html .= self::headerEnd();

      return $html;

   }

   /**
    * Start panel header 
    * 
    * @param array $options Panel header options
    * - class
    * - data : array with datat attributes to be applyed [attr1 => value1,attr2 => value2]
    * 
    * @return string Starting a div TAG
    * 
    * <div class="panel-heading custom class" data-attr1="value1" data-attr2="value2">
    */
   public static function headerStart($options = []) {

      // get default panel heading class
      $class = self::$classPanelHeader . ' ';
      $class .= (!empty($options['class'])) ? $options['class'] : '';

      $attributes = [
          'class' => $class
      ];

      //check for data attributes on parent div element
      if (!empty($options['data'])) {
         $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
      }

      return Tag::tagHtml('div', $attributes);

   }

   public static function headerEnd() {

      return Tag::tagHtmlClose('div');

   }

   /**
    * Panel Footer
    * 
    * @param array $options [ class:"custom class", "data":["url":"test/test","title":"Title"],"elements":"some html buttons","html":"Some text" ]
    * 
    * @return string Panel Footer HTML section
    * 
    * <div class="panel-footer custom class" data-url="test/test" data-title="Title">
    * 
    * </div>
    */
   public static function footer($options = []) {

      $html = self::footerStart();

      if (!empty($options['elements'])) {

         $html .= Tag::tagHtml('div', ['class' => 'heading-element']);
         $html .= Tag::tagHtml('div', ['class' => 'heading-btn right-align']);
         $html .= is_array($options['elements']) ? implode('', $options['elements']) : $options['elements'];
         $html .= Tag::tagHtmlClose('div');
         $html .= Tag::tagHtmlClose('div');
         //   
      } else if (!empty($options['html'])) {

         $html .= $options['html'];
      }

      $html .= self::footerEnd();

      return $html;

   }

   /**
    * Start Panel Footer section
    * 
    * @param array $options class and/or data attributes [ class:"custom class", "data":["url":"test/test","title":"Title"]]
    * 
    * @return string Panel Footer HTML section
    * 
    * <div class="panel-footer custom class" data-url="test/test" data-title="Title">
    */
   public static function footerStart($options = []) {

      $class = self::$classPanelFooter;
      $class .= !empty($options['class']) ? ' ' . $options['class'] : '';

      $attributes = [
          'class' => $class
      ];

      if (!empty($options['data'])) {
         $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
      }

      return Tag::tagHtml('div', $attributes);

   }

   //get the panel footer
   public static function footerEnd() {

      return Tag::tagHtmlClose('div');

   }

   /**
    * Resolve class/style attributes from array
    * 
    * @param array $borders Which borders to resolve
    * 
    * @return string class/style
    */
   private static function panelBorders($borders = [], $style) {

      if (empty($borders)) {
         return;
      }

      array_walk($borders, function(&$value, $key, $style) {

         $border = '';

         $border .= 'border-' . $key . '-' . $style . '-' . self::$borderColorThinkes;

         $border .= (!empty($value)) ? ' border-' . $key . '-' . $value : '';

         $value = $border;
      }, $style);

      return implode(' ', $borders);

   }

   /**
    * Add action icons into panel right side
    * 
    * @param array action List with actions (collapse,reload,refresh)
    * 
    * @return string html element
    * 
    * <div class="heading-element right-align">
    *    <ul class="icons-list">
    *       <li><a data-action="collapse"></a></li>
    *       .....
    *    </ul>
    * </div>
    */
   private static function actionIcons($actions) {

      $html = '';
      $html .= Tag::tagHtml('div', ['class' => 'heading-element right-align']);
      $html .= Tag::tagHtml('ul', ['class' => 'icons-list']);

      foreach ($actions as $action) {

         $html .= Tag::tagHtml('li') .
                 Tag::tagHtml('a', ['data-action' => $action]) . Tag::tagHtmlClose('a') .
                 Tag::tagHtmlClose('li');
      }

      $html .= Tag::tagHtmlClose('ul');
      $html .= Tag::tagHtmlClose('div');

      return $html;

   }

}
