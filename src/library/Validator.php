<?php

namespace PMP\Core\Library;

class Validator {

    /**
     * Validate email address.Check also MX records
     * 
     * @return bool true if email is valid
     */
    public static function email($email) {

        $record = 'MX';

        $emailStruct = \filter_var($email, FILTER_VALIDATE_EMAIL);

        if ($emailStruct) {

            list($user, $domain) = explode('@', $emailStruct);

            $exist = \checkdnsrr($domain, $record);

            return ($exist) ? true : false;
        } else {

            return false;
        }
    }

    /**
     * Validate EAN number
     * 
     * @param number $ean EAN number
     * 
     * @return bool true if EAN is a valid number
     */
    public static function ean($ean = '') {

        if (empty($ean) || !is_numeric($ean)) {
            return false;
        }

        $reverted = \strrev($ean);
        // Split number into checksum and number
        $checksum = substr($reverted, 0, 1);

        $number = substr($reverted, 1);
        
        $total = 0;

        for ($i = 0, $max = strlen($number); $i < $max; $i++) {
            if (($i % 2) == 0) {

                $total += ($number[$i] * 3);
            } else {
                $total += $number[$i];
            }
        }
        $mod = ($total % 10);

        $calculated_checksum = (10 - $mod);

        return ($calculated_checksum == $checksum) ? true : false;
    }

}
