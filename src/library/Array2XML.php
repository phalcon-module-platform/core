<?php

namespace PMP\Core\Library;

use \DomDocument;
use \Exception;

class Array2XML {

    /**
     * @var DOMDocument
     */
    private static $xml = null;

    /**
     * @var string 
     */
    private static $encoding = 'UTF-8';

    /**
     * Initialize the root XML node [optional]
     * 
     * @param string $version
     * @param string $encoding
     * @param bool $format_output
     */
    public static function init($version = '1.0', $encoding = 'UTF-8', $format_output = true) {

        self::$xml = new DomDocument($version, $encoding);

        self::$xml->formatOutput = $format_output;

        self::$encoding = $encoding;
    }

    /**
     * Convert an Array to XML
     * 
     * @param string $node_name - name of the root node to be converted
     * @param array $arr - array to be converted
     * 
     * @return DomDocument
     */
    public static function &createXML($node_name, $arr = []) {

        $xml = self::getXMLRoot();

        $xml->appendChild(self::convert($node_name, $arr));

        self::$xml = null;

        return $xml;
    }

    /**
     * Convert an Array to XML.
     *
     * @param string $node_name Name of the root node to be converted.
     * @param array $arr Array to be converted.
     *
     * @throws \Exception
     * @return \DOMNode
     */
    private static function &convert($node_name, $arr = array()) {

        $xml = self::getXMLRoot();

        $node = $xml->createElement($node_name);

        if (is_array($arr)) {

            if (isset($arr['@attributes'])) {

                foreach ($arr['@attributes'] as $key => $value) {

                    if (!self::isValidTagName($key)) {

                        throw new Exception('[Array2XML] Illegal character in attribute name. attribute: ' . $key . ' in node: ' . $node_name);
                    }

                    $node->setAttribute($key, self::bool2str($value));
                }

                unset($arr['@attributes']);
            }

            if (isset($arr['@value'])) {

                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));

                unset($arr['@value']);

                return $node;
            } else if (isset($arr['@cdata'])) {

                $node->appendChild($xml->createCDATASection(self::bool2str($arr['@cdata'])));

                unset($arr['@cdata']);

                return $node;
            } else if (isset($arr['@xml'])) {

                $fragment = $xml->createDocumentFragment();

                $fragment->appendXML($arr['@xml']);

                $node->appendChild($fragment);

                unset($arr['@xml']);

                return $node;
            }
        }

        //create subnodes using recursion
        if (is_array($arr)) {

            foreach ($arr as $key => $value) {

                if (!self::isValidTagName($key)) {

                    throw new Exception('[Array2XML] Illegal character in tag name. tag: ' . $key . ' in node: ' . $node_name);
                }
                if (is_array($value) && is_numeric(key($value))) {

                    foreach ($value as $k => $v) {

                        $node->appendChild(self::convert($key, $v));
                    }
                } else {

                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]);
            }
        }

        if (!is_array($arr)) {

            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }

        return $node;
    }

    /*
     * Get the root XML node, if there isn't one, create it.
     */

    private static function getXMLRoot() {

        if (empty(self::$xml)) {

            self::init();
        }
        return self::$xml;
    }

    /*
     * Get string representation of boolean value
     */

    private static function bool2str($v) {

        $v = $v === true ? 'true' : $v;

        $v = $v === false ? 'false' : $v;

        return $v;
    }

    /*
     * Check if the tag name or attribute name contains illegal characters
     */

    private static function isValidTagName($tag) {

        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';

        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }

}
