<?php

namespace PMP\Core\Library;

class ArrayWalk {

    /**
     * @var string array key name containing items
     */
    protected $items = 'items';

    /**
     * @var string Parent key name
     */
    protected $id;

    /**
     * @var string name of the key in sub array by which to compare  
     */
    protected $parent_id;

    /**
     * @var array  
     */
    protected $subArray = [];

    /**
     *  @var array Main array to rewrite 
     */
    protected $mainArray;

    /**
     * 
     *      
     */
    public function __construct($id, $parent_id, $items = false) {

        $this->id = $id;

        $this->parent_id = $parent_id;

        if (!empty($items)) {

            $this->items = $items;
        }
    }

    protected function arrayKeySearch(&$mainarray, $i, $items) {

        if ($mainarray[$this->id] == $items[$this->parent_id]) {

            if (!empty($items['id'])) {
                
                $mainarray[$this->items][$items['id']] = $items;
            } else {
                
                $mainarray[$this->items][] = $items;
            }
        }
    }

    public function setArrays($mainarray, $subarray) {

        $this->mainArray = $mainarray;

        $this->subArray = $subarray;
    }

    public function makeArray() {

        /* foreach from items, search in main array and rewrite it as add the items */
        foreach ($this->subArray as $subvalues) {

            array_walk($this->mainArray, array(get_class(), 'arrayKeySearch'), $subvalues);
        }
    }

    public function getCombined() {

        return $this->mainArray;
    }

}
