<?php

namespace PMP\Core\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use PMP\Core\Library\Constants\UserConstants;

class User extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="full_name", type="string", length=100, nullable=true)
     */
    protected $full_name;

    /**
     *
     * @var integer
     * @Column(column="role", type="integer", length=11, nullable=true)
     */
    protected $role;

    /**
     *
     * @var string
     * @Column(column="password", type="string", length=60, nullable=false)
     */
    protected $password;

    /**
     *
     * @var string
     * @Column(column="intro", type="string", nullable=true)
     */
    protected $intro;

    /**
     *
     * @var string
     * @Column(column="where_did", type="string", nullable=true)
     */
    protected $where_did;

    /**
     *
     * @var string
     * @Column(column="recover_hash", type="string", length=120, nullable=true)
     */
    protected $recover_hash;

    /**
     *
     * @var integer
     * @Column(column="status", type="integer", length=11, nullable=true)
     */
    protected $status;

    /**
     *
     * @var string
     * @Column(column="email", type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     *
     * @var string
     * @Column(column="last_visit", type="string", nullable=true)
     */
    protected $last_visit;

    /**
     *
     * @var string
     * @Column(column="date_created", type="string", nullable=true)
     */
    protected $date_created;

    /**
     *
     * @var string
     * @Column(column="date_updated", type="string", nullable=true)
     */
    protected $date_updated;

    /**
     *
     * @var string
     * @Column(column="recover_expire", type="string", nullable=true)
     */
    protected $recover_expire;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field full_name
     *
     * @param string $full_name
     * @return $this
     */
    public function setFullName($full_name) {
        $this->full_name = $full_name;

        return $this;
    }

    /**
     * Method to set the value of field role
     *
     * @param integer $role
     * @return $this
     */
    public function setRole($role) {
        $this->role = $role;

        return $this;
    }

    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Method to set the value of field recover_hash
     *
     * @param string $recover_hash
     * @return $this
     */
    public function setRecoverHash($recover_hash) {
        $this->recover_hash = $recover_hash;

        return $this;
    }

    /**
     * Method to set the value of field status
     *
     * @param integer $status
     * @return $this
     */
    public function setStatus($status) {
        $this->status = $status;

        return $this;
    }

    /**
     * Method to set the value of field email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Method to set the value of field intro
     *
     * @param string $intro
     * @return $this
     */
    public function setIntro($intro) {
        $this->intro = $intro;

        return $this;
    }

    /**
     * Method to set the value of field where_did
     *
     * @param string $where_did
     * @return $this
     */
    public function setWhereDid($where_did) {
        $this->where_did = $where_did;

        return $this;
    }

    /**
     * Method to set the value of field last_visit
     *
     * @param string $last_visit
     * @return $this
     */
    public function setLastVisit($last_visit) {
        $this->last_visit = $last_visit;

        return $this;
    }

    /**
     * Method to set the value of field date_created
     *
     * @param string $date_created
     * @return $this
     */
    public function setDateCreated($date_created) {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Method to set the value of field date_updated
     *
     * @param string $date_updated
     * @return $this
     */
    public function setDateUpdated($date_updated) {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Method to set the value of field recover_expire
     *
     * @param string $recover_expire
     * @return $this
     */
    public function setRecoverExpire($recover_expire) {
        $this->recover_expire = $recover_expire;

        return $this;
    }

    /**
     *  Check if user is admin and returns boolean result
     */
    public function isAdmin() {
        return $this->getRole() == UserConstants::role_admin;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field full_name
     *
     * @return string
     */
    public function getFullName() {
        return $this->full_name;
    }

    /**
     * Returns the value of field role
     *
     * @return integer
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Returns the value of field recover_hash
     *
     * @return string
     */
    public function getRecoverHash() {
        return $this->recover_hash;
    }

    /**
     * Returns the value of field status
     *
     * @return integer
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * Returns the value of field email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Returns the value of field intro
     *
     * @return string
     */
    public function getIntro() {
        return $this->intro;
    }

    /**
     * Returns the value of field where_did
     *
     * @return string
     */
    public function getWhereDid() {
        return $this->where_did;
    }

    /**
     * Returns the value of field last_visit
     *
     * @return string
     */
    public function getLastVisit() {
        return $this->last_visit;
    }

    /**
     * Returns the value of field date_created
     *
     * @return string
     */
    public function getDateCreated() {
        return $this->date_created;
    }

    /**
     * Returns the value of field date_updated
     *
     * @return string
     */
    public function getDateUpdated() {
        return $this->date_updated;
    }

    /**
     * Returns the value of field date_updated
     *
     * @return string
     */
    public function getRecoverExpire() {
        return $this->recover_expire;
    }

    /**
     * @return string The first error message
     */
    public function getFirstMessage() {

        $errors = $this->getMessages();

        return !empty($errors[0]) ? $errors[0]->getMessage() : false;
    }

    /**
     * Gets the UserProfileDetails model. New record is created if Profile Details not exists
     * 
     * @return \PMP\Core\Models\UserProfileDetails
     */
    public function getProfileDetails() {

        $profileDetails = $this->getRelated('UserProfileDetails');

        if (empty($profileDetails)) {

            $profileDetails = new UserProfileDetails([
                'user_id' => $this->getId()
            ]);

            $profileDetails->save();
        }

        return $profileDetails;
    }
    /**
     * Gets the UserSecurity model. New record is created if not exists
     * 
     * @return \PMP\Core\Models\UserSecurity
     */
    public function getSecurity() {

        $security = $this->getRelated('UserSecurity');

        if (empty($security)) {

            $security = new UserSecurity();

            $security->setUserId($this->getId());

            $security->setLoginHistory(1);
            
            $security->setSessionTime(3600);

            $security->save();
        }

        return $security;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {

        $validator = new Validation();

        $validator->add('email', new EmailValidator([
                    'model' => $this,
                    'message' => 'REGISTER_ERROR_EMAIL',
                        ])
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("user");
        $this->hasMany('id', 'PMP\Core\Models\UserLoginsHistory', 'user_id', ['alias' => 'UserLoginsHistory']);
        $this->belongsTo('role', 'PMP\Core\Models\SystemRoles', 'id', ['alias' => 'SystemRoles']);
        $this->belongsTo('id', 'PMP\Core\Models\UserProfileDetails', 'user_id', ['alias' => 'UserProfileDetails']);
        $this->belongsTo('id', 'PMP\Core\Models\UserSecurity', 'user_id', ['alias' => 'UserSecurity']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'user';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return User[]|User|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return User|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'full_name' => 'full_name',
            'role' => 'role',
            'password' => 'password',
            'recover_hash' => 'recover_hash',
            'recover_expire' => 'recover_expire',
            'status' => 'status',
            'email' => 'email',
            'intro' => 'intro',
            'where_did' => 'where_did',
            'last_visit' => 'last_visit',
            'date_created' => 'date_created',
            'date_updated' => 'date_updated'
        ];
    }

}
