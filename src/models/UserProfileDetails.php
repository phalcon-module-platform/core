<?php

namespace PMP\Core\Models;

class UserProfileDetails extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=11, nullable=true)
     */
    protected $user_id;

    /**
     *
     * @var string
     * @Column(column="first_name", type="string", length=50, nullable=true)
     */
    protected $first_name;

    /**
     *
     * @var string
     * @Column(column="last_name", type="string", length=50, nullable=true)
     */
    protected $last_name;

    /**
     *
     * @var string
     * @Column(column="photo", type="string", nullable=true)
     */
    protected $photo;

    /**
     *
     * @var string
     * @Column(column="company", type="string", length=160, nullable=true)
     */
    protected $company;

    /**
     *
     * @var string
     * @Column(column="position", type="string", length=160, nullable=true)
     */
    protected $position;

    /**
     *
     * @var string
     * @Column(column="website_address", type="string", length=160, nullable=true)
     */
    protected $website_address;

    /**
     *
     * @var string
     * @Column(column="about_me", type="string", nullable=true)
     */
    protected $about_me;

    /**
     *
     * @var string
     * @Column(column="date_created", type="string", nullable=true)
     */
    protected $date_created;

    /**
     *
     * @var string
     * @Column(column="date_updated", type="string", nullable=true)
     */
    protected $date_updated;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Method to set the value of field first_name
     *
     * @param string $first_name
     * @return $this
     */
    public function setFirstName($first_name) {
        $this->first_name = $first_name;

        return $this;
    }

    /**
     * Method to set the value of field last_name
     *
     * @param string $last_name
     * @return $this
     */
    public function setLastName($last_name) {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Method to set the value of field photo
     *
     * @param string $photo
     * @return $this
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Method to set the value of field company
     *
     * @param string $company
     * @return $this
     */
    public function setCompany($company) {
        $this->company = $company;

        return $this;
    }

    /**
     * Method to set the value of field position
     *
     * @param string $position
     * @return $this
     */
    public function setPosition($position) {
        $this->position = $position;

        return $this;
    }

    /**
     * Method to set the value of field website_address
     *
     * @param string $website_address
     * @return $this
     */
    public function setWebsiteAddress($website_address) {
        $this->website_address = $website_address;

        return $this;
    }

    /**
     * Method to set the value of field about_me
     *
     * @param string $about_me
     * @return $this
     */
    public function setAboutMe($about_me) {
        $this->about_me = $about_me;

        return $this;
    }

    /**
     * Method to set the value of field date_created
     *
     * @param string $date_created
     * @return $this
     */
    public function setDateCreated($date_created) {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Method to set the value of field date_updated
     *
     * @param string $date_updated
     * @return $this
     */
    public function setDateUpdated($date_updated) {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * Returns the value of field first_name
     *
     * @return string
     */
    public function getFirstName() {
        return $this->first_name;
    }

    /**
     * Returns the value of field last_name
     *
     * @return string
     */
    public function getLastName() {
        return $this->last_name;
    }

    /**
     * Returns the value of field photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Returns the value of field company
     *
     * @return string
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * Returns the value of field position
     *
     * @return string
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * Returns the value of field website_address
     *
     * @return string
     */
    public function getWebsiteAddress() {
        return $this->website_address;
    }

    /**
     * Returns the value of field about_me
     *
     * @return string
     */
    public function getAboutMe() {
        return $this->about_me;
    }

    /**
     * Returns the value of field date_created
     *
     * @return string
     */
    public function getDateCreated() {
        return $this->date_created;
    }

    /**
     * Returns the value of field date_updated
     *
     * @return string
     */
    public function getDateUpdated() {
        return $this->date_updated;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("user_profile_details");
        $this->belongsTo('user_id', 'PMP\Core\Common\Models\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'user_profile_details';
    }

    public function beforeCreate() {

        $date_created = date('Y-m-d H:i:s');

        $this->setDateCreated($date_created);

        $this->setDateUpdated($date_created);
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserProfileDetails[]|UserProfileDetails|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserProfileDetails|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'photo' => 'photo',
            'company' => 'company',
            'position' => 'position',
            'website_address' => 'website_address',
            'about_me' => 'about_me',
            'date_created' => 'date_created',
            'date_updated' => 'date_updated'
        ];
    }

}
