<?php

namespace PMP\Core\Models;

class UserLoginsHistory extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=11, nullable=true)
     */
    protected $user_id;

    /**
     *
     * @var string
     * @Column(column="login_date", type="string", nullable=false)
     */
    protected $login_date;

    /**
     *
     * @var string
     * @Column(column="logout_date", type="string", nullable=true)
     */
    protected $logout_date;

    /**
     *
     * @var integer
     * @Column(column="time_spent", type="integer", length=11, nullable=true)
     */
    protected $time_spent;

    /**
     *
     * @var string
     * @Column(column="ip_address", type="string", length=50, nullable=true)
     */
    protected $ip_address;

    /**
     *
     * @var string
     * @Column(column="latitude", type="string", length=50, nullable=true)
     */
    protected $latitude;

    /**
     *
     * @var string
     * @Column(column="longtitude", type="string", length=50, nullable=true)
     */
    protected $longtitude;

    /**
     *
     * @var string
     * @Column(column="city", type="string", length=160, nullable=true)
     */
    protected $city;

    /**
     *
     * @var string
     * @Column(column="country", type="string", length=160, nullable=true)
     */
    protected $country;

    /**
     *
     * @var string
     * @Column(column="country_code", type="string", length=6, nullable=true)
     */
    protected $country_code;

    /**
     *
     * @var string
     * @Column(column="user_agent", type="string", nullable=true)
     */
    protected $user_agent;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Method to set the value of field login_date
     *
     * @param string $login_date
     * @return $this
     */
    public function setLoginDate($login_date) {
        $this->login_date = $login_date;

        return $this;
    }

    /**
     * Method to set the value of field logout_date
     *
     * @param string $logout_date
     * @return $this
     */
    public function setLogoutDate($logout_date) {
        $this->logout_date = $logout_date;

        return $this;
    }

    /**
     * Method to set the value of field time_spent
     *
     * @param integer $time_spent
     * @return $this
     */
    public function setTimeSpent($time_spent) {
        $this->time_spent = $time_spent;

        return $this;
    }

    /**
     * Method to set the value of field ip_address
     *
     * @param string $ip_address
     * @return $this
     */
    public function setIpAddress($ip_address) {
        $this->ip_address = $ip_address;

        return $this;
    }

    /**
     * Method to set the value of field latitude
     *
     * @param string $latitude
     * @return $this
     */
    public function setLatitude($latitude) {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Method to set the value of field longtitude
     *
     * @param string $longtitude
     * @return $this
     */
    public function setLongtitude($longtitude) {
        $this->longtitude = $longtitude;

        return $this;
    }

    /**
     * Method to set the value of field city
     *
     * @param string $city
     * @return $this
     */
    public function setCity($city) {
        $this->city = $city;

        return $this;
    }

    /**
     * Method to set the value of field country
     *
     * @param string $country
     * @return $this
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Method to set the value of field country_code
     *
     * @param string $country_code
     * @return $this
     */
    public function setCountryCode($country_code) {
        $this->country_code = $country_code;

        return $this;
    }

    /**
     * Method to set the value of field user_agent
     *
     * @param string $user_agent
     * @return $this
     */
    public function setUserAgent($user_agent) {
        $this->user_agent = $user_agent;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * Returns the value of field login_date
     *
     * @return string
     */
    public function getLoginDate() {
        return $this->login_date;
    }

    /**
     * Returns the value of field logout_date
     *
     * @return string
     */
    public function getLogoutDate() {
        return $this->logout_date;
    }

    /**
     * Returns the value of field time_spent
     *
     * @return integer
     */
    public function getTimeSpent() {
        return $this->time_spent;
    }

    /**
     * Returns the value of field ip_address
     *
     * @return string
     */
    public function getIpAddress() {
        return $this->ip_address;
    }

    /**
     * Returns the value of field latitude
     *
     * @return string
     */
    public function getLatitude() {
        return $this->latitude;
    }

    /**
     * Returns the value of field longtitude
     *
     * @return string
     */
    public function getLongtitude() {
        return $this->longtitude;
    }

    /**
     * Returns the value of field city
     *
     * @return string
     */
    public function getCity() {
        return $this->city;
    }

    /**
     * Returns the value of field country
     *
     * @return string
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Returns the value of field country_code
     *
     * @return string
     */
    public function getCountryCode() {
        return $this->country_code;
    }

    /**
     * Returns the value of field user_agent
     *
     * @return string
     */
    public function getUserAgent() {
        return $this->user_agent;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("user_logins_history");
        $this->belongsTo('user_id', 'PMP\Core\Models\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'user_logins_history';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLoginsHistory[]|UserLoginsHistory|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserLoginsHistory|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'login_date' => 'login_date',
            'logout_date' => 'logout_date',
            'time_spent' => 'time_spent',
            'ip_address' => 'ip_address',
            'latitude' => 'latitude',
            'longtitude' => 'longtitude',
            'city' => 'city',
            'country' => 'country',
            'country_code' => 'country_code',
            'user_agent' => 'user_agent'
        ];
    }

}
