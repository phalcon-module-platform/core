<?php

namespace PMP\Core\Models;

class SystemLanguages extends \Phalcon\Mvc\Model {

   /**
    *
    * @var integer
    * @Primary
    * @Identity
    * @Column(column="id", type="integer", length=11, nullable=false)
    */
   protected $id;

   /**
    *
    * @var string
    * @Column(column="name", type="string", length=50, nullable=true)
    */
   protected $name;

   /**
    *
    * @var string
    * @Column(column="code", type="string", length=6, nullable=true)
    */
   protected $code;

   /**
    *
    * @var string
    * @Column(column="short_name", type="string", length=6, nullable=true)
    */
   protected $short_name;

   /**
    *
    * @var string
    * @Column(column="flag", type="string", length=50, nullable=true)
    */
   protected $flag;

   /**
    *
    * @var string
    * @Column(column="encoding", type="string", length=12, nullable=true)
    */
   protected $encoding;

   /**
    *
    * @var string
    * @Column(column="class", type="string", length=120, nullable=true)
    */
   protected $class;

   /**
    * Method to set the value of field id
    *
    * @param integer $id
    * @return $this
    */
   public function setId($id) {
      $this->id = $id;

      return $this;

   }

   /**
    * Method to set the value of field name
    *
    * @param string $name
    * @return $this
    */
   public function setName($name) {
      $this->name = $name;

      return $this;

   }

   /**
    * Method to set the value of field code
    *
    * @param string $code
    * @return $this
    */
   public function setCode($code) {
      $this->code = $code;

      return $this;

   }

   /**
    * Method to set the value of field short_name
    *
    * @param string $short_name
    * @return $this
    */
   public function setShortName($short_name) {
      $this->short_name = $short_name;

      return $this;

   }

   /**
    * Method to set the value of field flag
    *
    * @param string $flag
    * @return $this
    */
   public function setFlag($flag) {
      $this->flag = $flag;

      return $this;

   }

   /**
    * Method to set the value of field encoding
    *
    * @param string $encoding
    * @return $this
    */
   public function setEncoding($encoding) {
      $this->encoding = $encoding;

      return $this;

   }

   /**
    * Method to set the value of field class
    *
    * @param string $class
    * @return $this
    */
   public function setClass($class) {
      $this->class = $class;

      return $this;

   }

   /**
    * Returns the value of field id
    *
    * @return integer
    */
   public function getId() {
      return $this->id;

   }

   /**
    * Returns the value of field name
    *
    * @return string
    */
   public function getName() {
      return $this->name;

   }

   /**
    * Returns the value of field code
    *
    * @return string
    */
   public function getCode() {
      return $this->code;

   }

   /**
    * Returns the value of field short_name
    *
    * @return string
    */
   public function getShortName() {
      return $this->short_name;

   }

   /**
    * Returns the value of field flag
    *
    * @return string
    */
   public function getFlag() {
      return $this->flag;

   }

   /**
    * Returns the value of field encoding
    *
    * @return string
    */
   public function getEncoding() {
      return $this->encoding;

   }

   /**
    * Returns the value of field class
    *
    * @return string
    */
   public function getClass() {
      return $this->class;

   }

   /**
    * Initialize method for model.
    */
   public function initialize() {
      $this->setSource("_system_languages");
      $this->hasMany('id', 'PMP\Core\Models\SystemTranslation', 'language_id', ['alias' => 'SystemTranslation']);

   }

   /**
    * Returns table name mapped in the model.
    *
    * @return string
    */
   public function getSource() {
      return '_system_languages';

   }

   /**
    * Allows to query a set of records that match the specified conditions
    *
    * @param mixed $parameters
    * @return SystemLanguages[]|SystemLanguages|\Phalcon\Mvc\Model\ResultSetInterface
    */
   public static function find($parameters = null) {
      return parent::find($parameters);

   }

   /**
    * Allows to query the first record that match the specified conditions
    *
    * @param mixed $parameters
    * @return SystemLanguages|\Phalcon\Mvc\Model\ResultInterface
    */
   public static function findFirst($parameters = null) {
      return parent::findFirst($parameters);

   }

   /**
    * Independent Column Mapping.
    * Keys are the real names in the table and the values their names in the application
    *
    * @return array
    */
   public function columnMap() {
      return [
          'id' => 'id',
          'name' => 'name',
          'code' => 'code',
          'short_name' => 'short_name',
          'flag' => 'flag',
          'encoding' => 'encoding',
          'class' => 'class'
      ];

   }

}
