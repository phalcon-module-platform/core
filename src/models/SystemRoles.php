<?php

namespace PMP\Core\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use PMP\Core\Library\Constants\UserConstants;

class SystemRoles extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=50, nullable=false)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="description", type="string", length=255, nullable=false)
     */
    protected $description;

    /**
     *
     * @var integer
     * @Column(column="protected", type="integer", length=11, nullable=false)
     */
    protected $protected;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field protected
     *
     * @param integer $protected
     * @return $this
     */
    public function setProtected($protected) {
        $this->protected = $protected;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field protected
     *
     * @return integer
     */
    public function getProtected() {
        return $this->protected;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_roles");
        $this->hasMany('id', 'PMP\Core\Models\SystemAccess', 'fkirole', ['alias' => 'SystemAccess']);
        $this->hasMany('id', 'PMP\Core\Models\User', 'role', ['alias' => 'User']);
    }

    /**
     * @return string The first error message
     */
    public function getFirstMessage() {

        $errors = $this->getMessages();

        return !empty($errors[0]) ? $errors[0]->getMessage() : false;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {

        $validator = new Validation();

        $validator->add('name', new UniquenessValidator([
            'model' => $this,
            'message' => 'UPDATE_ERROR_RECORD_DUPLICATE',
                ])
        );

        $validator->add('name', new PresenceOf([
            'model' => $this,
            'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
        ]));

        $validator->add('description', new UniquenessValidator([
            'model' => $this,
            'message' => 'UPDATE_ERROR_RECORD_DUPLICATE',
                ])
        );

        $validator->add('description', new PresenceOf([
            'model' => $this,
            'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
        ]));

        return $this->validate($validator);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_roles';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemRoles[]|SystemRoles|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Gets only user role where can be edited
     * @return SystemRoles[]|SystemRoles|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function findAccessibleRoles() {

        return parent::find([
                    'columns' => '*',
                    'conditions' => 'id <> :id:',
                    'bind' => [
                        'id' => UserConstants::role_admin
                    ]
        ]);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemRoles|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'name' => 'name',
            'description' => 'description',
            'protected' => 'protected'
        ];
    }

}
