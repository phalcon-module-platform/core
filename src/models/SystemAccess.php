<?php

namespace PMP\Core\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;

class SystemAccess extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="fkimodule", type="integer", length=11, nullable=true)
     */
    protected $fkimodule;

    /**
     *
     * @var integer
     * @Column(column="fkirole", type="integer", length=11, nullable=true)
     */
    protected $fkirole;

    /**
     *
     * @var integer
     * @Column(column="fkicontroller", type="integer", length=11, nullable=true)
     */
    protected $fkicontroller;

    /**
     *
     * @var integer
     * @Column(column="fkiaction", type="integer", length=11, nullable=true)
     */
    protected $fkiaction;

    /**
     *
     * @var string
     * @Column(column="access", type="string", length=50, nullable=true)
     */
    protected $access;

    /**
     *
     * @var string
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field fkimodule
     *
     * @param integer $fkimodule
     * @return $this
     */
    public function setFkimodule($fkimodule) {
        $this->fkimodule = $fkimodule;

        return $this;
    }

    /**
     * Method to set the value of field fkirole
     *
     * @param integer $fkirole
     * @return $this
     */
    public function setFkirole($fkirole) {
        $this->fkirole = $fkirole;

        return $this;
    }

    /**
     * Method to set the value of field fkicontroller
     *
     * @param integer $fkicontroller
     * @return $this
     */
    public function setFkicontroller($fkicontroller) {
        $this->fkicontroller = $fkicontroller;

        return $this;
    }

    /**
     * Method to set the value of field fkiaction
     *
     * @param integer $fkiaction
     * @return $this
     */
    public function setFkiaction($fkiaction) {
        $this->fkiaction = $fkiaction;

        return $this;
    }

    /**
     * Method to set the value of field access
     *
     * @param string $access
     * @return $this
     */
    public function setAccess($access) {
        $this->access = $access;

        return $this;
    }

    /**
     * Method to set the value of field created
     *
     * @param string $created
     * @return $this
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field fkimodule
     *
     * @return integer
     */
    public function getFkimodule() {
        return $this->fkimodule;
    }

    /**
     * Returns the value of field fkirole
     *
     * @return integer
     */
    public function getFkirole() {
        return $this->fkirole;
    }

    /**
     * Returns the value of field fkicontroller
     *
     * @return integer
     */
    public function getFkicontroller() {
        return $this->fkicontroller;
    }

    /**
     * Returns the value of field fkiaction
     *
     * @return integer
     */
    public function getFkiaction() {
        return $this->fkiaction;
    }

    /**
     * Returns the value of field access
     *
     * @return string
     */
    public function getAccess() {
        return $this->access;
    }

    /**
     * Returns the value of field created
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @return string The first error message
     */
    public function getFirstMessage() {

        $errors = $this->getMessages();

        return !empty($errors[0]) ? $errors[0]->getMessage() : false;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {

        $validator = new Validation();

        $validator->add(['fkimodule', 'fkirole', 'fkicontroller', 'fkiaction'], new UniquenessValidator([
            'model' => $this,
            'message' => 'UPDATE_ERROR_RECORD_DUPLICATE',
                ])
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_access");
        $this->belongsTo('fkiaction', 'PMP\Core\Models\SystemActions', 'id', ['alias' => 'SystemActions']);
        $this->belongsTo('fkicontroller', 'PMP\Core\Models\SystemControllers', 'id', ['alias' => 'SystemControllers']);
        $this->belongsTo('fkimodule', 'PMP\Core\Models\SystemModules', 'id', ['alias' => 'SystemModules']);
        $this->belongsTo('fkirole', 'PMP\Core\Models\SystemRoles', 'id', ['alias' => 'SystemRoles']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_access';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemAccess[]|SystemAccess|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemAccess|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'fkimodule' => 'fkimodule',
            'fkirole' => 'fkirole',
            'fkicontroller' => 'fkicontroller',
            'fkiaction' => 'fkiaction',
            'access' => 'access',
            'created' => 'created'
        ];
    }

}
