<?php

namespace PMP\Core\Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness as UniquenessValidator;
use PMP\Core\Models\SystemAccess;
use PMP\Core\Models\SystemActions;
use PMP\Core\Models\SystemModules;

class SystemControllers extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="fkimodule", type="integer", length=11, nullable=false)
     */
    protected $fkimodule;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=50, nullable=false)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="description", type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     *
     * @var string
     * @Column(column="title", type="string", length=32, nullable=false)
     */
    protected $title;

    /**
     *
     * @var integer
     * @Column(column="inmenu", type="integer", length=2, nullable=true)
     */
    protected $inmenu;

    /**
     *
     * @var string
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    /**
     *
     * @var string
     * @Column(column="updated", type="string", nullable=false)
     */
    protected $updated;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field fkimodule
     *
     * @param integer $fkimodule
     * @return $this
     */
    public function setFkimodule($fkimodule) {
        $this->fkimodule = $fkimodule;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field inmenu
     *
     * @param integer $inmenu
     * @return $this
     */
    public function setInmenu($inmenu) {
        $this->inmenu = $inmenu;

        return $this;
    }

    /**
     * Method to set the value of field created
     *
     * @param string $created
     * @return $this
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Method to set the value of field updated
     *
     * @param string $updated
     * @return $this
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field fkimodule
     *
     * @return integer
     */
    public function getFkimodule() {
        return $this->fkimodule;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Returns the value of field inmenu
     *
     * @return integer
     */
    public function getInmenu() {
        return $this->inmenu;
    }

    /**
     * Returns the value of field created
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Returns the value of field updated
     *
     * @return string
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * @return string The first error message
     */
    public function getFirstMessage() {

        $errors = $this->getMessages();

        return !empty($errors[0]) ? $errors[0]->getMessage() : false;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {

        $validator = new Validation();

        $validator->add(['name', 'fkimodule'], new UniquenessValidator([
            'model' => $this,
            'message' => 'UPDATE_ERROR_RECORD_DUPLICATE',
                ])
        );

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_controllers");
        $this->hasMany('id', SystemAccess::class, 'fkicontroller', ['alias' => 'SystemAccess']);
        $this->hasMany('id', SystemActions::class, 'fkicontroller', ['alias' => 'SystemActions']);
        $this->belongsTo('fkimodule', SystemModules::class, 'id', ['alias' => 'SystemModules']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_controllers';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemControllers[]|SystemControllers|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemControllers|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'fkimodule' => 'fkimodule',
            'name' => 'name',
            'description' => 'description',
            'title' => 'title',
            'inmenu' => 'inmenu',
            'created' => 'created',
            'updated' => 'updated'
        ];
    }

}
