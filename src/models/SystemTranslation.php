<?php

namespace PMP\Core\Models;
use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class SystemTranslation extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="language_id", type="integer", length=11, nullable=false)
     */
    protected $language_id;

    /**
     *
     * @var string
     * @Column(column="word", type="string", length=190, nullable=false)
     */
    protected $word;

    /**
     *
     * @var string
     * @Column(column="value", type="string", nullable=false)
     */
    protected $value;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field language_id
     *
     * @param integer $language_id
     * @return $this
     */
    public function setLanguageId($language_id) {
        $this->language_id = $language_id;

        return $this;
    }

    /**
     * Method to set the value of field word
     *
     * @param string $word
     * @return $this
     */
    public function setWord($word) {
        $this->word = $word;

        return $this;
    }

    /**
     * Method to set the value of field value
     *
     * @param string $value
     * @return $this
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field language_id
     *
     * @return integer
     */
    public function getLanguageId() {
        return $this->language_id;
    }

    /**
     * Returns the value of field word
     *
     * @return string
     */
    public function getWord() {
        return $this->word;
    }

    /**
     * Returns the value of field value
     *
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @return string The first error message
     */
    public function getFirstMessage() {

        $errors = $this->getMessages();

        return !empty($errors[0]) ? $errors[0]->getMessage() : false;
    }

    /**
     * Validations and business logic
     *
     * @return boolean
     */
    public function validation() {

        $validator = new Validation();

        $validator->add('value', new PresenceOf([
            'model' => $this,
            'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER',
                ])
        );


        $validator->setFilters('value', 'trim');

        return $this->validate($validator);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_translation");
        $this->belongsTo('language_id', 'PMP\Core\Models\SystemLanguages', 'id', ['alias' => 'SystemLanguages']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_translation';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemTranslation[]|SystemTranslation|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemTranslation|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Allows to query all the records that match the specified conditions
     *
     * @param string $code
     * @return array list with translation
     */
    public static function findByCode($code) {

        $translation = self::query()
                ->columns("word, value")
                ->innerJoin('PMP\Core\Models\SystemLanguages')
                ->where('code = :code:')
                ->andWhere("value <> ''")
                ->bind(['code' => $code])
                ->execute();

        return $translation->toArray();
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'language_id' => 'language_id',
            'word' => 'word',
            'value' => 'value'
        ];
    }

}
