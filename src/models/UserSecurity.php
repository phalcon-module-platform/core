<?php

namespace PMP\Core\Models;

class UserSecurity extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var integer
     * @Column(column="user_id", type="integer", length=11, nullable=false)
     */
    protected $user_id;

    /**
     *
     * @var integer
     * @Column(column="login_alert", type="integer", length=11, nullable=true)
     */
    protected $login_alert;

    /**
     *
     * @var integer
     * @Column(column="login_unathorized_alert", type="integer", length=11, nullable=true)
     */
    protected $login_unathorized_alert;

    /**
     *
     * @var integer
     * @Column(column="login_history", type="integer", length=11, nullable=true)
     */
    protected $login_history;

    /**
     *
     * @var integer
     * @Column(column="session_time", type="integer", length=11, nullable=true)
     */
    protected $session_time;

    /**
     *
     * @var string
     * @Column(column="date_updated", type="string", nullable=true)
     */
    protected $date_updated;

    /**
     *
     * @var string
     * @Column(column="date_created", type="string", nullable=true)
     */
    protected $date_created;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field user_id
     *
     * @param integer $user_id
     * @return $this
     */
    public function setUserId($user_id) {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Method to set the value of field login_alert
     *
     * @param integer $login_alert
     * @return $this
     */
    public function setLoginAlert($login_alert) {
        $this->login_alert = $login_alert;

        return $this;
    }

    /**
     * Method to set the value of field login_unathorized_alert
     *
     * @param integer $login_unathorized_alert
     * @return $this
     */
    public function setLoginUnathorizedAlert($login_unathorized_alert) {
        $this->login_unathorized_alert = $login_unathorized_alert;

        return $this;
    }

    /**
     * Method to set the value of field login_history
     *
     * @param integer $login_history
     * @return $this
     */
    public function setLoginHistory($login_history) {
        $this->login_history = $login_history;

        return $this;
    }

    /**
     * Method to set the value of field session_time
     *
     * @param integer $session_time
     * @return $this
     */
    public function setSessionTime($session_time) {
        $this->session_time = $session_time;

        return $this;
    }

    /**
     * Method to set the value of field date_updated
     *
     * @param string $date_updated
     * @return $this
     */
    public function setDateUpdated($date_updated) {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Method to set the value of field date_created
     *
     * @param string $date_created
     * @return $this
     */
    public function setDateCreated($date_created) {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field user_id
     *
     * @return integer
     */
    public function getUserId() {
        return $this->user_id;
    }

    /**
     * Returns the value of field login_alert
     *
     * @return integer
     */
    public function getLoginAlert() {
        return $this->login_alert;
    }

    /**
     * Returns the value of field login_unathorized_alert
     *
     * @return integer
     */
    public function getLoginUnathorizedAlert() {
        return $this->login_unathorized_alert;
    }

    /**
     * Returns the value of field login_history
     *
     * @return integer
     */
    public function getLoginHistory() {
        return $this->login_history;
    }

    /**
     * Returns the value of field session_time
     *
     * @return integer
     */
    public function getSessionTime() {
        return $this->session_time;
    }

    /**
     * Returns the value of field date_updated
     *
     * @return string
     */
    public function getDateUpdated() {
        return $this->date_updated;
    }

    /**
     * Returns the value of field date_created
     *
     * @return string
     */
    public function getDateCreated() {
        return $this->date_created;
    }

    public function beforeCreate() {

        $date_created = date('Y-m-d H:i:s');

        $this->setDateCreated($date_created);
    }

    public function beforeUpdate() {

        $date_updated = date('Y-m-d H:i:s');

        $this->setDateUpdated($date_updated);
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("user_security");
        $this->belongsTo('user_id', 'PMP\Core\Common\Models\User', 'id', ['alias' => 'User']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return 'user_security';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserSecurity[]|UserSecurity|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return UserSecurity|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'login_alert' => 'login_alert',
            'login_unathorized_alert' => 'login_unathorized_alert',
            'login_history' => 'login_history',
            'session_time' => 'session_time',
            'date_updated' => 'date_updated',
            'date_created' => 'date_created'
        ];
    }

}
