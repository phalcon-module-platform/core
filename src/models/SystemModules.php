<?php

namespace PMP\Core\Models;

use PMP\Core\Models\SystemAccess;
use PMP\Core\Models\SystemControllers;

class SystemModules extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="name", type="string", length=50, nullable=false)
     */
    protected $name;

    /**
     *
     * @var string
     * @Column(column="title", type="string", length=50, nullable=false)
     */
    protected $title;

    /**
     *
     * @var string
     * @Column(column="description", type="string")
     */
    protected $description;

    /**
     *
     * @var string
     * @Column(column="created", type="string", nullable=false)
     */
    protected $created;

    /**
     *
     * @var string
     * @Column(column="updated", type="string", nullable=false)
     */
    protected $updated;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field name
     *
     * @param string $name
     * @return $this
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Method to set the value of field title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Method to set the value of field description
     *
     * @param string $description
     * @return $this
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Method to set the value of field created
     *
     * @param string $created
     * @return $this
     */
    public function setCreated($created) {
        $this->created = $created;

        return $this;
    }

    /**
     * Method to set the value of field updated
     *
     * @param string $updated
     * @return $this
     */
    public function setUpdated($updated) {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Returns the value of field title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Returns the value of field description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Returns the value of field created
     *
     * @return string
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * Returns the value of field updated
     *
     * @return string
     */
    public function getUpdated() {
        return $this->updated;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_modules");
        $this->hasMany('id', SystemAccess::class, 'fkimodule', ['alias' => 'SystemAccess']);
        $this->hasMany('id', SystemControllers::class, 'fkimodule', ['alias' => 'SystemControllers']);
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_modules';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemModules[]|SystemModules|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemModules|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'name' => 'name',
            'title' => 'title',
            'description' => 'description',
            'created' => 'created',
            'updated' => 'updated'
        ];
    }

}
