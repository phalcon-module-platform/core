<?php

namespace PMP\Core\Models;

class SystemMessages extends \Phalcon\Mvc\Model {

    /**
     *
     * @var integer
     * @Primary
     * @Identity
     * @Column(column="id", type="integer", length=11, nullable=false)
     */
    protected $id;

    /**
     *
     * @var string
     * @Column(column="variable", type="string", length=50, nullable=true)
     */
    protected $variable;

    /**
     *
     * @var string
     * @Column(column="message", type="string", length=250, nullable=true)
     */
    protected $message;

    /**
     *
     * @var string
     * @Column(column="date_created", type="string", nullable=true)
     */
    protected $date_created;

    /**
     *
     * @var string
     * @Column(column="date_updated", type="string", nullable=true)
     */
    protected $date_updated;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field variable
     *
     * @param string $variable
     * @return $this
     */
    public function setVariable($variable) {
        $this->variable = $variable;

        return $this;
    }

    /**
     * Method to set the value of field message
     *
     * @param string $message
     * @return $this
     */
    public function setMessage($message) {
        $this->message = $message;

        return $this;
    }

    /**
     * Method to set the value of field date_created
     *
     * @param string $date_created
     * @return $this
     */
    public function setDateCreated($date_created) {
        $this->date_created = $date_created;

        return $this;
    }

    /**
     * Method to set the value of field date_updated
     *
     * @param string $date_updated
     * @return $this
     */
    public function setDateUpdated($date_updated) {
        $this->date_updated = $date_updated;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns the value of field variable
     *
     * @return string
     */
    public function getVariable() {
        return $this->variable;
    }

    /**
     * Returns the value of field message
     *
     * @return string
     */
    public function getMessage() {
        return $this->message;
    }

    /**
     * Returns the value of field date_created
     *
     * @return string
     */
    public function getDateCreated() {
        return $this->date_created;
    }

    /**
     * Returns the value of field date_updated
     *
     * @return string
     */
    public function getDateUpdated() {
        return $this->date_updated;
    }

    /**
     * Initialize method for model.
     */
    public function initialize() {
        $this->setSource("_system_messages");
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource() {
        return '_system_messages';
    }

    /**
     * Allows to query a set of records that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemMessages[]|SystemMessages|\Phalcon\Mvc\Model\ResultSetInterface
     */
    public static function find($parameters = null) {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return SystemMessages|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null) {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'variable' => 'variable',
            'message' => 'message',
            'date_created' => 'date_created',
            'date_updated' => 'date_updated'
        ];
    }

}
