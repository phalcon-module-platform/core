<?php

/**
 * ModulesInterface
 */

namespace PMP\Core;

use Phalcon\Config;
use Phalcon\Text;
use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Manager;
use PMP\Core\Plugins\DispatcherPlugin;
use PMP\Core\Modules\Api\Plugins\DispatcherPlugin AS ApiDispatcherPlugin;

/**
 * ModulesInterface
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 * 
 * @property \PMP\Core\Config\SystemConfig $systemConfig
 */
class ModulesInterface implements ModuleDefinitionInterface {

    /**
     * @var \Phalcon\Mvc\Router
     */
    protected $router;

    /**
     * @var string
     */
    protected $modulName;

    /**
     * @var \Phalcon\Config
     */
    protected $modulConfig;

    /**
     * @var \Phalcon\Config;
     */
    protected $moduleDirs;

    /**
     * @var array;
     */
    protected $do_not_auto_register = ['migrations', 'config', 'layouts', 'views'];

    /**
     * @var array;
     */
    protected $default_namespaces = ['Controllers', 'Models', 'Library', 'Plugins'];

    /**
     * Register Autoloaders
     *
     *
     * @param \Phalcon\DiInterface $di With a *description* of this argument, these may also
     *
     * @return \Phalcon\Loader
     */
    public function registerAutoloaders(DiInterface $di = null) {

        $this->systemConfig = $di->get('systemConfig');

        $this->router = $di->get('router');

        $this->modulName = $this->router->getModuleName();

        //sets the active module
        $this->systemConfig->setActiveModule($this->modulName);

        $this->modulConfig = $this->systemConfig->getModuleConfig($this->modulName);

        $this->moduleDirs = $this->systemConfig->getModuleFolders($this->modulName);

        //1. register core namespaces
        $coreCommonDirs = glob(__DIR__ . '/*', GLOB_ONLYDIR);

        $dirs = $this->recursivelyRegister($coreCommonDirs, ['PMP', 'Core']);

        //2. register dirs if access to application module

        $appD = glob($this->systemConfig->getAppFullDir() . '/*', GLOB_ONLYDIR);

        $appdirs = $this->recursivelyRegister($appD, ['App']);

        $dirs->merge($appdirs);

        //degfault namespaces
        $defaultNS = new Config($this->default_namespaces);

        //module namespaces
        $namespaces = $this->modulConfig->path('namespaces', new Config([]));

        //merge module namespaces into defaults one
        $defaultNS->merge($namespaces);

        //remove duplicate
        $NS = array_unique($defaultNS->toArray());

        //3. register module name spaces
        foreach ($NS as $namespace) {

            $modulName = lcfirst(Text::camelize($this->modulName, "-"));

            $dir = [$this->modulConfig->path('rootPath'), $modulName, strtolower($namespace)];

            $path = chop(implode('/', $dir), '/') . '/';

            if (!file_exists($path)) {
                continue;
            }

            $nsd = [$this->modulConfig->path('namespacesRoot'), $namespace];

            $dirs->offsetSet(implode('\\', $nsd), $path);
        }

        $loader = new Loader();

        $loader->registerNamespaces($dirs->toArray());

        $loader->registerClasses($dirs->toArray());

        $loader->registerDirs(array_values($dirs->toArray()));

        return $loader->register();
    }

    public function registerServices(DiInterface $di = null) {
        
    }

    /**
     * Registering a dispatcher
     */
    public function registerDispatcher(DiInterface $di = null) {

        $moduleConfig = $this->modulConfig;

        $di->setShared('dispatcher', function () use($moduleConfig) {

            //Create/Get an EventManager
            $eventsManager = $this->getShared('eventsManager');

            //Attach a listener
            $eventsManager->attach("dispatch", new DispatcherPlugin);

            $dispatcher = new Dispatcher();

            $dispatcher->setDefaultNamespace($moduleConfig->path('metadata.controllersNamespace'));

            //Bind the EventsManager to the dispatcher
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        });
    }

    /**
     * Registering a dispatcher
     */
    public function registerApiDispatcher(DiInterface $di = null) {

        $moduleConfig = $this->modulConfig;

        $di->setShared('dispatcher', function () use($moduleConfig) {

            //Create/Get an EventManager
            $eventsManager = new Manager();

            //Attach a listener
            $eventsManager->attach("dispatch", new ApiDispatcherPlugin);

            $dispatcher = new Dispatcher();

            $dispatcher->setDefaultNamespace($moduleConfig->path('metadata.controllersNamespace'));

            //Bind the EventsManager to the dispatcher
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        });
    }

    /**
     * Recursively register directories , 3 levels maximum
     * 
     * @param string $glob Directory to start
     * @param string $namespaces Namespaces to start
     * 
     * @return \Phalcon\Config
     */
    private function recursivelyRegister($glob, $namespaces = []) {

        $ns = implode('\\', $namespaces);

        $dirs = new Config();

        foreach ($glob as $coreDir) {

            $dir = basename($coreDir);

            if (in_array($dir, $this->do_not_auto_register)) {
                continue;
            }

            $dirs->offsetSet($ns . '\\' . ucfirst($dir), $coreDir . '/');

            $subdirs = glob($coreDir . '/*', GLOB_ONLYDIR);

            if (empty($subdirs)) {
                continue;
            }

            foreach ($subdirs as $subdir) {

                $sd = basename($subdir);

                if (in_array($sd, $this->do_not_auto_register)) {
                    continue;
                }

                $dirs->offsetSet($ns . '\\' . ucfirst($dir) . '\\' . ucfirst($sd), $subdir . '/');

                $subsubdirs = glob($subdir . '/*', GLOB_ONLYDIR);

                if (empty($subsubdirs)) {
                    continue;
                }

                foreach ($subsubdirs as $value) {

                    $ssd = basename($value);

                    if (in_array($ssd, $this->do_not_auto_register)) {
                        continue;
                    }

                    $dirs->offsetSet($ns . '\\' . ucfirst($dir) . '\\' . ucfirst($sd) . '\\' . ucfirst($ssd), $value . '/');
                }
            }
        }

        return $dirs;
    }

}
