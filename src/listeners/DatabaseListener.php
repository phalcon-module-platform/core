<?php

namespace PMP\Core\Listeners;

use Phalcon\Logger;
use Phalcon\Logger\Adapter\File as FileLogger;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Db\Profiler as DbProfiler;
use Phalcon\Db\AdapterInterface;
use Phalcon\Di\Injectable;
use Phalcon\Events\Event;
use PMP\Core\Library\FileSystem;

class DatabaseListener extends Injectable {

    /**
     * @var \Phalcon\Db\Profiler
     */
    private $_profiler;

    /**
     * @var \Phalcon\Logger\Adapter\File
     */
    private $_logger;

    /**
     * @var string log file name
     */
    private $_log = 'db-log.xml';

    /**
     * @var string
     */
    private $_path = '';

    /**
     * @param \Phalcon\Config $config Listener configuration [enable,path]
     */
    public function __construct(\Phalcon\Config $config) {

        $this->_path = $config->path('path', false);

        if (empty($this->_path)) {

            die('Please define logs folder into config file if you want to use the Db logger');
        }
        
        FileSystem::makeDir($this->_path);
        
    }

    /**
     * @return \Phalcon\Logger\Adapter\File
     */
    private function getLogger() {

        if (!empty($this->_logger)) {

            return $this->_logger;
        }

        $logger = new FileLogger($this->_path . '/' . $this->_log);

        $formatter = new LineFormatter("%message%");

        $logger->setFormatter($formatter);

        $this->_logger = $logger;

        return $logger;
    }

    /**
     * @return \Phalcon\Db\Profiler
     */
    private function getDbProfiler() {

        if (!empty($this->_profiler)) {

            return $this->_profiler;
        }

        $this->_profiler = new DbProfiler();

        return $this->_profiler;
    }

    public function afterConnect() {
        
    }

    public function beforeDisconnect() {
        
    }

    public function afterQuery(Event $event, AdapterInterface $connection) {

        $sql = $connection->getSQLStatement();

        $profile = $this->getDbProfiler()->getLastProfile();

        $this->getDbProfiler()->stopProfile($sql);

        if (preg_match('/DESCRIBE|SELECT IF/', $sql)) {
            return false;
        }

        $regex = '/(?<=\bFROM\s)(?:[\w-]+)/is';

        $matches = false;

        preg_match($regex, str_replace('`', '', $sql), $matches);

        $info = '';
        $info .= '<profile time="' . round($profile->getTotalElapsedSeconds(), 3, PHP_ROUND_HALF_UP) . '">' . PHP_EOL;
        $info .= !empty($matches[0]) ? '<table>' . $matches[0] . '</table>' . PHP_EOL : '';
        $info .= '<query>' . $sql . '</query>' . PHP_EOL;
        $info .= '</profile>';

        $this->getLogger()->log(Logger::INFO, $info);
    }

    public function beforeQuery(Event $event, AdapterInterface $connection) {

        $sql = $connection->getSQLStatement();

        $this->getDbProfiler()->startProfile($sql);
    }

    public function beginTransaction() {
        
    }

    public function createSavepoint() {
        
    }

    public function commitTransaction() {
        
    }

    public function releaseSavepoint() {
        
    }

    public function rollbackTransaction() {
        
    }

    public function rollbackSavepoint() {
        
    }

}
