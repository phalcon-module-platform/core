<?php

namespace PMP\Core;

use Phalcon\Mvc\View;
use Phalcon\DiInterface;
use PMP\Core\Providers\Engine\VoltEngine;
use PMP\Core\ModulesInterface;
use PMP\Core\Library\FileSystem;

class Module extends ModulesInterface {

    /**
     * Registers services specific to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di = null) {

        //load shared services where managed from admin panel
        foreach ($this->systemConfig->path('sharedServices', []) as $key => $value) {

            $di->setShared($key, $value->toArray());
        }

        $services = [
            \PMP\Core\Services\Db::class,
            \PMP\Core\Services\Cache::class,
            \PMP\Core\Services\Url::class,
            \PMP\Core\Services\ViewToAjax::class,
            \PMP\Core\Services\SystemMessages::class,
            \PMP\Core\Services\Session::class,
            \PMP\Core\Services\SessionProfile::class,
            \PMP\Core\Services\User::class,
            \PMP\Core\Services\Profile::class,
            \PMP\Core\Services\Acl::class,
            \PMP\Core\Services\FlashSession::class,
            \PMP\Core\Services\Assets::class,
            \PMP\Core\Services\Elements::class,
            \PMP\Core\Services\Translate::class,
            \PMP\Core\Services\Registry::class,
            \PMP\Core\Services\Console::class,
            \PMP\Core\Services\MailManagerService::class,
            \PMP\Core\Services\Filters::class,
            \PMP\Core\Services\Response::class,
        ];

        //scan folder and register app services 
        $files = glob($this->systemConfig->getAppFullDir() . '/services/*.php');

        foreach ($files as $service) {

            $filename = pathinfo($service, PATHINFO_FILENAME);

            $services[] = 'App\\Services\\' . $filename;
        }

        foreach ($services as $service) {
            $di->register(new $service());
        }

        $this->registerDispatcher($di);

        $this->registerView($di);
    }

    /**
     * Setting up the view component
     */
    private function registerView(DiInterface $di = null) {

        $config = $this->modulConfig;

        $moduleDirs = $this->moduleDirs;

        $di->setShared('view', function () use ($config, $moduleDirs) {

            $view = new View();

            $sysconfig = $this->getSystemConfig();

            $view->setViewsDir($moduleDirs->path('layoutDir'));

            //sets layout based on module
            $view->setLayoutsDir($moduleDirs->path('layoutDir'));

            $view->setPartialsDir($moduleDirs->path('partialDir'));

            $view->setTemplateBefore($config->path('layout'));
            
            //start engine
            $volt = new VoltEngine($view, $this);

            $compiledPath = FileSystem::makeDir($sysconfig->getLayoutsCachelDir());

            $volt->setOptions([
                'always' => false,
                'extension' => '.volt',
                'compiledPath' => $compiledPath,
                'compiledSeparator' => '_'
            ]);

            $volt->registerFunctions();

            $engine = $volt->getEngine();

            $view->registerEngines($engine);

            return $view;
        });
    }

}
