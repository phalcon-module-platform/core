<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PMP\Core\Scripts;

/**
 * Application
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Application {

    /**
     * @var array
     */
    private $replacement = [
        "{description}" => "\033[m",
        "{t}" => "  ",
        "{t2}" => "      ",
        "{n}" => PHP_EOL,
        "{g}" => "\033[1;32m",
        "{y}" => "\e[38;5;178m",
        "{w}" => "\033[0m",
        "{c}" => "\e[38;5;244m",
        "{b}" => "\e[38;5;86m",
    ];

    /**
     * @var \Composer\Script\Event
     */
    private $event;

    /**
     * @var string
     */
    private $vendor_dir;

    /**
     * @var string
     */
    private $base_dir;

    /**
     * @var string
     */
    private $package_name;

    /**
     * @var string
     */
    private $package_version;

    /**
     * @var []
     */
    private $arguments = [];

    /**
     * @var []
     */
    private $parsed_arguments = [];

    public function __construct($properties = []) {

        foreach ($properties as $prop => $value) {
            if (property_exists(get_class(), $prop)) {
                $this->{$prop} = $value;
            }
        }
    }

    /**
     * @return \Composer\Script\Event
     */
    public function getEvent() {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getVendorDir() {
        return $this->vendor_dir;
    }

    /**
     * @return string
     */
    public function getBaseDir() {
        return $this->base_dir;
    }

    /**
     * @return string
     */
    public function getPackageName() {
        return $this->package_name;
    }

    /**
     * @return string
     */
    public function getPackageVersion() {
        return $this->package_version;
    }

    /**
     * @return [] Parsed arguments
     */
    public function getArguments() {

        if (!empty($this->parsed_arguments)) {

            return $this->parsed_arguments;
        }

        $parsed = [];

        foreach ($this->arguments as $value) {

            $kvp = explode('=', $value);

            $filtered = array_filter($kvp);

            if (empty($filtered)) {
                continue;
            }

            $parsed[$filtered[0]] = !empty($filtered[1]) ? $filtered[1] : false;
        }

        $this->parsed_arguments = $parsed;

        return $this->parsed_arguments;
    }

    /**
     * @return mixed Argument or false if argument is not set
     */
    public function getArgument($arg = false) {

        return $arg && !empty($this->getArguments()[$arg]) ? $this->getArguments()[$arg] : false;
    }

    /**
     * Writes a message to the output.
     *
     * @param string|array $messages  The message as an array of lines or a single string
     * @param bool         $newline   Whether to add a newline or not
     */
    public function write($messages, $newline = true) {

        if (empty($messages)) {

            return false;
        }

        $colored = is_array($messages) ? $messages : [$messages];

        array_walk($colored, function(&$value) {

            $value = $this->colorMessage($value);
        });

        $this->getEvent()->getIO()->write($colored, $newline);
    }

    /**
     * Asks a question to the user.
     *
     * @param string $question The question to ask
     * @param string $default  The default answer if none is given by the user
     *
     * @throws \RuntimeException If there is no data to read in the input stream
     * @return string            The user answer
     */
    public function ask($question, $default = false) {

        $message = $this->colorMessage($question);

        return $this->getEvent()->getIO()->ask($message, $default);
    }

    /**
     * Coloring a message
     * 
     * @param string $message
     * 
     * @return string Colored message
     */
    public function colorMessage($message = '') {

        $msg = strtr($message, $this->replacement);

        return $msg;
    }

    /**
     * Output help to console
     * 
     * @param array $options
     */
    public function outputHelp($options = []) {

        $print = [];

        $title = $this->getPackageName() . ' (' . $this->getPackageVersion() . ')';

        if (!empty($options['title'])) {

            $title .= " - " . $options['title'];
        }

        $print[] = "{n}{b}" . $title;

        foreach ($options as $key => $value) {
            switch ($key) {
                case 'options':
                    $print[] = "{n}{y}Available commands:{w}";
                    foreach ($value as $key => $descr) {
                        $print[] = "{t}{g}" . $key . "{w}{t}{w}" . "{description}" . $descr . "{description}";
                    }
                    break;
                case 'usage':
                    $print[] = "{n}{y}Usage:{n}{t}{c}" . $value . "{n}";
                    break;
                case 'examples':
                    $print[] = "{y}Example:";
                    foreach ($value as $example) {
                        $print[] = "{t}{c}" . $example;
                    }
                    break;
                default:
                    break;
            }
        }

        $print[] = '{n}';

        $this->write($print);
    }

}
