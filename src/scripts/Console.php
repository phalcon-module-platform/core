<?php

namespace PMP\Core\Scripts;

use PMP\Core\Scripts\Application;
use PMP\Core\Scripts\ApplicationInterface;
use PMP\Core\Scripts\Enums\ConsoleEnums;

/**
 * Description of Console
 *
 * @author stani
 */
class Console extends Application implements ApplicationInterface {

    
    
    public function getHelp() {
        
        //get help enums
        $help = ConsoleEnums::config();

        $this->outputHelp($help);

        return true;
    }

}
