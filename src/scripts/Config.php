<?php

namespace PMP\Core\Scripts;

use Phalcon\Config\Adapter\Ini AS ConfigIni;
use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config AS PhalconConfig;
use PMP\Core\Scripts\Application;
use PMP\Core\Scripts\ApplicationInterface;
use PMP\Core\Scripts\Enums\HelpEnums;
use PMP\Core\Scripts\Enums\ConfigEnums;
use PMP\Core\Library\FileSystem;

/**
 * Application initialization
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Config extends Application implements ApplicationInterface {

    public function init() {

        if (!file_exists(ConfigEnums::CONFIG_PATH)) {

            $this->write('{n}Your {y}' . ConfigEnums::CONFIG_PATH . " {w}is missing, You need to install the application. Type: {y}'composer pmp install'{w}for more information{n}");

            return true;
        }

        //default allways ask to generate env file
        $newEnv = true;

        $newConfig = true;

        //check for existing env, read the env and ask a question to generate new one
        if (file_exists(ConfigEnums::ENV_FILE)) {

            $existsenv = new ConfigIni(ConfigEnums::ENV_FILE);

            $env = $existsenv->path('global.env');

            //gets false and do not generate if env file exists and user not answer on question
            $newEnv = $this->ask("{n}There is an existing environment {y}(" . $env . "){w}, do you want to generate new one?( y )", false);
        }

        switch ($newEnv) {
            case!false:
                $env = $this->generateEnvFile();
                break;
            default:
                break;
        }

        $env_config_file = ConfigEnums::CONFIG_PATH . $env . '.json';

        if (file_exists($env_config_file)) {

            //gets false and do not generate if config file exists and user not answer on question
            $newConfig = $this->ask("{n}{y}" . $env . ".json{w} config file already exists, do you want to generate new one( y )?", false);
        }

        switch ($newConfig) {
            case!false:
                $this->generateConfigFile($env_config_file);
                break;
            default:
                break;
        }
    }

    public function getHelp() {

        //get help enums
        $help = HelpEnums::config();

        $this->outputHelp($help);

        return true;
    }

    /**
     * Validate Answer when asking a question for creating new env file.
     * 
     * @return string the env name
     */
    protected function validateEnvironments($answer = false) {

        if (!in_array($answer, ConfigEnums::DEFAULT_ENV_TYPES)) {

            return ConfigEnums::DEFAULT_ENV;
        }

        return $answer;
    }

    /**
     * Generate new .env file
     */
    protected function generateEnvFile() {

        $message = $this->colorMessage('{n}What is your environment (' . implode(', ', ConfigEnums::DEFAULT_ENV_TYPES) . '): ');

        $env = $this->getEvent()->getIO()->askAndValidate($message, function($answer) {
            return $this->validateEnvironments($answer);
        }, null, ConfigEnums::DEFAULT_ENV);

        $envfile = [
            'global' => [
                'env' => $env
            ],
            "######  DO NOT CHANGE LINES BELLOW ########",
            'environments' => ConfigEnums::DEFAULT_ENV_TYPES
        ];

        $ini = "";

        foreach ($envfile as $key => $value) {

            $ini .= "";

            if (is_int($key)) {

                $ini .= "\n" . $value . "\n";

                continue;
            }

            if (is_array($value)) {

                $ini .= "[{$key}]\n";

                foreach ($value as $param => $option) {

                    $ini .= $param . '=' . $option . "\n";
                }

                continue;
            }

            $ini .= $key . '=' . $value . "\n";
        }

        FileSystem::writeFile(ConfigEnums::ENV_FILE, $ini);

        $this->write('{n}New environment file has been generated', true);

        return $env;
    }

    /**
     * Generate new json config file
     * 
     * @param string $file_name
     */
    protected function generateConfigFile($file_name) {

        $dbConfig = [
            "database" => [
                "adapter" => "Mysql",
                "host" => "localhost",
                "dbname" => $this->ask("{n}-> database name: ", false),
                "username" => $this->ask("{n}-> database username: ", false),
                "password" => $this->ask("{n}-> database password: ", false),
                "charset" => "utf8"
            ]
        ];

        $newConfig = new PhalconConfig($dbConfig);

        //if file exists, read the file and merge data base configuration only
        if (file_exists($file_name)) {

            $existsconfig = new ConfigJson($file_name);

            $newConfig = $existsconfig->merge($newConfig);
        }

        $save = FileSystem::writeFile($file_name, json_encode($newConfig->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        if (empty($save)) {

            $this->write('{n}File can not be saved on location: ' . $file_name, true);

            return false;
        }

        $this->write('{n}New configuration file has been generated. '
                . 'Find the path to your Phalcon Devtools to migrate the database.');
      
        $this->write('{n}{y}How to migrate the CORE tables:{y}');
        
        $this->write('{n}{w}php /full-path-to-phalcon-devtools/phalcon.php migration run --migrations app/migrations/core/{w}');
        
        $this->write('{n}{y}How to migrate the APP tables:{y}');
        
        $this->write('{n}{w}php /full-path-to-phalcon-devtools/phalcon.php migration run --migrations app/migrations/app/{w}');

        return $file_name;
    }

}
