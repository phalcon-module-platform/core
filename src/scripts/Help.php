<?php

namespace PMP\Core\Scripts;

use PMP\Core\Scripts\Application;
use PMP\Core\Scripts\ApplicationInterface;
use PMP\Core\Scripts\Enums\HelpEnums;

/**
 * Install
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Help extends Application implements ApplicationInterface {



    /**
     * Main Application Help
     */
    public function getHelp() {

        //get help enums
        $help = HelpEnums::application();

        $this->outputHelp($help);

        return true;
    }

}
