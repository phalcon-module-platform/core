<?php

namespace PMP\Core\Scripts;

use Composer\Script\Event;

/**
 * ApplicationController
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ApplicationController {

    public static function load(Event $event) {

        //gets the arguments
        $args = $event->getArguments();

        if (empty($args[0])) {

            $event->getIO()->writeError("\nuse 'composer pmp help'  for more information", true);

            return true;
        }

        $class = $args[0];

        $method = !empty($args[1]) ? $args[1] : $args[1] = '';

        unset($args[0]);
        unset($args[1]);

        $callClass = __NAMESPACE__ . '\\' . ucfirst($class);

        if (!class_exists($callClass)) {

            $event->getIO()->writeError("\nCommand does not exists, use: 'composer pmp help' for more information", true);
            
            return true;
        }

        $reflect = new \ReflectionClass($callClass);

        //pickup instance of Called Class and set properties
        $instance = $reflect->newInstance([
            'event' => $event,
            'vendor_dir' => $event->getComposer()->getConfig()->get('vendor-dir'),
            'package_name' => $event->getComposer()->getPackage()->getDescription(),
            'package_version' => $event->getComposer()->getPackage()->getFullPrettyVersion(),
            'arguments' => array_values($args)
        ]);

        //checks if method exists into callClass
        if ($method && $reflect->hasMethod($method)) {
            
            //call method from instantiated class and add the rest of arguments to it
            $reflect->getMethod($method)->invoke($instance);
            
        } else {

            $reflect->getMethod('getHelp')->invoke($instance);
        }
    }

}
