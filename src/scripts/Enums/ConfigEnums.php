<?php

namespace PMP\Core\Scripts\Enums;

/**
 * ConfigEnums
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ConfigEnums {

    /**
     * @var string
     */
    const ENV_FILE = '.env';

    /**
     * @var string
     */
    const DEFAULT_ENV = 'development';

    /**
     * @var array
     */
    const DEFAULT_ENV_TYPES = [
        'env_development' => 'development',
        'env_staging' => 'staging',
        'env_production' => 'production'
    ];

    /**
     * @var string
     */
    const CONFIG_PATH = 'app/config/';

}
