<?php

namespace PMP\Core\Scripts\Enums;

/**
 * InstallEnums
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class InstallEnums {

    /**
     * @var string
     */
    const INSTALL_ENDPOINT_URL = 'https://cdn.phalcon-lister.com/pmp/application/';

    /**
     * @var string
     */
    const INSTALL_FILE_NAME = 'application-base';

    /**
     * @var string
     */
    const INSTALL_FILE_EXTENSION = '.zip';

    /**
     * @var string
     */
    const INSTALL_FILE_CONTENT_TYPE = 'application/zip';

    /**
     * @var string
     */
    const INSTALL_FILE_STABLE_VERSION = 'stable';

    /**
     * @var string
     */
    const INSTALL_SUCCESS = 'You have succefully install the application';

    /**
     * @var string
     */
    const INSTALL_ERROR_SOURCE_EXISTS = '{y}app, public{y} {w}folder/s already exists. Please use{w} {g}update{g} {w}or{w} {g}re-install{g} {w}commands.You ca also remove existing app folder and run this command again{w}';

    /**
     * @var string
     */
    const INSTALL_ERROR_SOURCE_NOT_EXISTS = 'Requested file does not exists';

    /**
     * @var string
     */
    const INSTALL_ERROR_SAVING_SOURCE = 'File can not be saved';

    /**
     * @var string
     */
    const INSTALL_ERROR_EXTRACT_SOURCE = 'Unable to extract the file';

}
