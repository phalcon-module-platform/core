<?php

namespace PMP\Core\Scripts\Enums;

/**
 * HelpEnums
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class ConsoleEnums {


    public static function config() {

        return [
            'title' => 'Console',
            'options' => [
                'task' => '  Execute specific task'
            ],
            'usage' => 'composer pmp console task email [action] [param]',
            'examples' => [
                'composer pmp console task email send'
            ]
        ];
    }

}
