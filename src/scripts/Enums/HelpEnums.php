<?php

namespace PMP\Core\Scripts\Enums;

/**
 * HelpEnums
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class HelpEnums {


    public static function application() {

        return [
            'options' => [
                'install' => '  Install application, plugin or component',
                'config' => '   Configurator',
                'migrate' => '  Migrate database tables',
                'update' => '   Update existing plugin or component'
            ],
            'usage' => 'composer pmp [option] [action] [key=value]',
            'examples' => [
                'composer pmp init config',
                'composer pmp install application'
            ]
        ];
    }

    public static function install() {

        return [
            'title' => 'Installer',
            'options' => [
                'application' => '  Install Full application',
                'plugin' => '       Install Plugin',
                'component' => '    Install Component'
            ],
            'usage' => 'composer pmp install [option] [key=value]',
            'examples' => [
                'composer pmp init config',
                'composer pmp install application'
            ]
        ];
    }

    public static function config() {

        return [
            'title' => 'Configurator',
            'options' => [
                'init' => '  Creates new configuration and environment file'
            ],
            'usage' => 'composer pmp config [option] [key=value]',
            'examples' => [
                'composer pmp config init'
            ]
        ];
    }

}
