<?php

namespace PMP\Core\Scripts;

use ZipArchive;
use Phalcon\Http\Client\Request;
use PMP\Core\Scripts\Application;
use PMP\Core\Scripts\ApplicationInterface;
use PMP\Core\Scripts\Enums\HelpEnums;
use PMP\Core\Scripts\Enums\InstallEnums;
use PMP\Core\Library\FileSystem;

/**
 * Install
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Install extends Application implements ApplicationInterface {

    /**
     * Install base application with Admin panel
     */
    public function application() {
        
        if (file_exists('app') || file_exists('public') || file_exists('console')) {

            $this->write("{n}Folders 'app', 'console' or 'public' exists, please remove any previous installation and run 'composer pmp install application'");

            return true;
        }
        
        $this->write('Downloading.......');

        $file = InstallEnums::INSTALL_FILE_NAME . InstallEnums::INSTALL_FILE_EXTENSION;

        $provider = Request::getProvider();

        $provider->header->set('Accept', InstallEnums::INSTALL_FILE_CONTENT_TYPE);

        try {

            $response = $provider->get(InstallEnums::INSTALL_ENDPOINT_URL . $file);
            
        } catch (Exception $exc) {

            $this->write($exc->getMessage());

            return true;
        }

        /* @var $header \Phalcon\Http\Client\Header */
        $header = $response->header;

        if ($header->get('Content-Type') !== InstallEnums::INSTALL_FILE_CONTENT_TYPE) {

            $this->write(InstallEnums::INSTALL_ERROR_SOURCE_NOT_EXISTS);

            return true;
        }

        $bytes = FileSystem::writeFile($file, $response->body);

        if (empty($bytes)) {

            $this->write(InstallEnums::INSTALL_ERROR_SAVING_SOURCE);

            return true;
        }

        $this->write('Extracting files.....');

        $zip = new ZipArchive();

        $res = $zip->open($file);

        if ($res !== true) {

            $this->write(InstallEnums::INSTALL_ERROR_SAVING_SOURCE);

            return true;
        }

        $extracted = $zip->extractTo('.');

        $zip->close();

        if ($extracted !== true) {

            $this->addToOutput('The archive contents can not extracted');

            return true;
        }

        FileSystem::deleteFile($file);

        $this->write('{n}{g}Success.{g} {w}Run{w} {y}composer pmp config init{w} to finish the installation proccess');
    }

    public function plugin() {
        
    }

    public function getHelp() {


        //get help enums
        $help = HelpEnums::install();

        $this->outputHelp($help);

        return true;
    }

}
