<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PMP\Core\Scripts;

/**
 * Description of Enums
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Enums {

    /**
     * @var array
     */
    const colors = [
        'y' => '1;33',
        'g' => '1;32'
    ];

    /**
     * @var array
     */
    const replacement = [
        "{description}" => "\033[m",
        "{t}" => "  ",
        "{t2}" => "      ",
        "{n}" => PHP_EOL,
        "{g}" => "\033[1;32m",
        "{y}" => "\033[1;33m",
        "{w}" => "\033[0m",
    ];

    /**
     * @param string $message (n - new line, t - tab, t2 - double tab, description - white text)
     * 
     * @return string Colored message
     */
    public static function addToOutput($message = '') {

        if (empty($message)) {

            return '';
        }

        $msg = strtr($message, self::replacement);

        return $msg;
    }

}
