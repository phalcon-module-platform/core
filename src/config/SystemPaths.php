<?php

namespace PMP\Core\Config;

use Phalcon\Config\Adapter\Ini AS ConfigIni;
use Phalcon\Config AS PhalconConfig;
use PMP\Core\Library\FileSystem;

/**
 * Description of SystemPaths
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class SystemPaths {

    const STORAGE_FOLDER = 'storage';

    /**
     * @var Phalcon\Config\Adapter\Ini
     */
    private $env;

    /**
     * @var \Phalcon\Config
     */
    private $systemConfig;

    /**
     * @var string
     */
    protected $baseDir;

    /**
     * @var string
     */
    protected $appDir = 'app';

    /**
     * @var string
     */
    protected $packageDir;

    /**
     * @var string
     */
    protected $publicPath = 'public';

    /**
     * @var string
     */
    protected $uploadPath = 'uploads';

    /**
     * @var string
     */
    private $templateName = false;

    /**
     * @return string Template name
     */
    public function getTemplateName() {

        if ($this->templateName !== false) {
            return $this->templateName;
        }

        $this->templateName = $this->path('assets.template');

        return $this->templateName;
    }

    /**
     * @param string $publicPath Name of the public path
     */
    public function setPublicPath($publicPath = false) {

        if (!empty($publicPath)) {
            $this->publicPath = $publicPath;
        }
    }

    /**
     * 
     * @param \Phalcon\Config $systemConfig
     */
    public function setSystemConfig(\Phalcon\Config $systemConfig) {

        $this->systemConfig = $systemConfig;
    }

    /**
     * @return \Phalcon\Config\Adapter\Ini
     */
    public function readEnv() {

        if (!empty($this->env)) {

            return $this->env;
        }

        if (!file_exists($this->getBaseDir() . '/.env')) {

            die('You must setup your .env file. Learn how to do it from setup.txt located into project root folder');
        }

        $this->env = new ConfigIni($this->getBaseDir() . '/.env');

        return $this->env;
    }

    /**
     * @return string 
     */
    public function getEnv() {

        return $this->readEnv()->path('global.env');
    }

    /**
     * @return bool true if environment is development 
     */
    public function isEnvDevelopment() {

        return $this->getEnv() === $this->readEnv()->path('environments.env_development');
    }

    /**
     * @return bool true if environment is production 
     */
    public function isEnvProduction() {

        return $this->getEnv() === $this->readEnv()->path('environments.env_production');
    }

    /**
     * @return bool true if environment is staging 
     */
    public function isEnvStaging() {

        return $this->getEnv() === $this->readEnv()->path('environments.env_staging');
    }

    /**
     * System Configuration
     * 
     * @return \Phalcon\Config
     */
    public function getSystemConfig() {
        return $this->systemConfig;
    }

    /**
     * 
     * @return string Full path to system configurable file
     */
    public function getSystemConfigurableFile() {

        return $this->getAppFullDir() . '/config/system.json';
    }

    /**
     * Check whenever system config file exists
     */
    public function configCacheExists() {

        return file_exists($this->getSystemConfigurableFile());
    }

    /**
     * 
     * @return string Full path to system components
     */
    public function getSystemConfigurableComponentsFile() {

        return $this->getAppFullDir() . '/config/components.json';
    }

    /**
     * 
     * @return string Full path to JSON environment file eg. development.json, production.json
     */
    public function getSystemConfigurableEnvFile() {

        $file = $this->getAppFullDir() . '/config/' . $this->getEnv() . '.json';

        return $file;
    }

    /**
     * @return string Full path to config directory
     */
    public function getConfigDir() {

        return $this->getAppFullDir() . '/config';
    }

    /**
     * @return string Full path to config resources directory
     */
    public function getConfigResourcesDir() {

        return $this->getConfigDir() . '/resources';
    }

    /**
     * @return string application path name
     */
    public function getAppDir() {
        return $this->appDir;
    }

    /**
     * @return string full path to application directory (no trailing slash)
     */
    public function getAppFullDir() {

        return $this->getBaseDir() . '/' . $this->getAppDir();
    }

    /**
     * @return string full path to application modules folder incl. trailing slash
     */
    public function getAppModulesDir() {

        return $this->getAppFullDir() . '/modules/';
    }

    /**
     * @return string full path to core modules folder incl. trailing slash
     */
    public function getCoreModulesDir() {

        return $this->getPackageDir() . '/modules/';
    }

    /**
     * @return string full path to Base directory  (no trailing slash)
     */
    public function getBaseDir() {
        return $this->baseDir;
    }

    /**
     * @return string public path ,The name of HTTP doc folder
     */
    public function getPublicPath() {
        return $this->publicPath;
    }

    /**
     * @return string real path to public folder
     */
    public function getPublicPathReal() {
        return $this->getBaseDir() . '/' . $this->publicPath;
    }

    /**
     * @return string real path to email logs folder
     */
    public function getEmailLogsPath() {

        return FileSystem::makeDir($this->path('logs.email.path'));
    }

    /**
     * @return string real path to email logs folder
     */
    public function getRequestsLogsPath() {
        return $this->path('logs.requests.path');
    }

    /**
     * @return string full path to public uploads folder
     */
    public function getRealUploadPath() {

        $path = [
            $this->getBaseDir(),
            $this->getPublicPath(),
            $this->getUploadPath()
        ];

        $dir = \implode('/', $path);

        return FileSystem::makeDir($dir);
    }

    /**
     * @return string full path to session files incl. trailing slash
     */
    public function getSessionSavePath() {


        return FileSystem::makeDir($this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/sessions/');
    }

    /**
     * @return string full path to logs debug folder incl. trailing slash
     */
    public function getDebugLogsPath() {


        return FileSystem::makeDir($this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/debug/');
    }

    /**
     * @return string uploads path name eg/uploads
     */
    public function getUploadPath() {

        return $this->uploadPath;
    }

    /**
     * @return string  incl. trailing slash
     */
    public function getPartialDir() {

        return $this->getAppFullDir() . '/layouts/';
    }

    /**
     * @return string  incl. trailing slash
     */
    public function getCorePartialDir() {

        return $this->getPackageDir() . '/layouts/';
    }

    /**
     * @return string common view dir  incl. trailing slash
     */
    public function getCommonViewDir() {

        return $this->getAppFullDir() . '/common/views/';
    }

    /**
     * @return string Full path to Layouts cache directory  incl. trailing slash
     */
    public function getLayoutsCachelDir() {

        return $this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/cache/layouts/';
    }

    /**
     * @return string Full path to Layouts configuration directory  incl. trailing slash
     */
    public function getLayoutsConfigDir() {

        return $this->getConfigDir() . '/layouts/';
    }

    /**
     * @return string Full path to Cache directory  incl. trailing slash
     */
    public function getCachelDir() {

        return $this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/cache/';
    }

    /**
     * @return string Full path to vendor directory  incl. trailing slash
     */
    public function getVendorsDir() {

        return $this->getBaseDir() . '/vendor/';
    }

    /**
     * @return string Full path to package directory
     */
    public function getPackageDir() {

        return !empty($this->packageDir) ? $this->packageDir : dirname(__FILE__, 2);
    }

    /**
     * @return string Full path to package directory
     */
    public function setPackageDir($dir) {

        return $this->packageDir = $dir;
    }

    /**
     * @return array Path ready for replacement
     */
    public function getPathReplacements() {

        $callBackParams = [
            '{baseDir}' => $this->getBaseDir(),
            '{appDir}' => $this->getAppFullDir(),
            '{packageDir}' => $this->getPackageDir(),
            '{vendorDir}' => $this->getVendorsDir()
        ];

        return $callBackParams;
    }

    /**
     * @return string Full path to Models Cache directory  incl. trailing slash
     */
    public function getModelsCachelDir() {

        return $this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/cache/models/';
    }

    /**
     * Gets all the paths Phalcon Webtools need
     * 
     * @return \Phalcon\Config
     */
    public function getWebtoolsPaths() {

        $paths = [
            'controllersDir' => '{baseDir}/{appDir}/common/controllers/',
            'modelsDir' => '{baseDir}/{appDir}/common/models/',
            'migrationsDir' => '{baseDir}/{appDir}/migrations/',
            'componentsDir' => '{baseDir}/{appDir}/common/components/',
            'viewsDir' => '{baseDir}/{appDir}/common/views/',
            'pluginsDir' => '{baseDir}/{appDir}/common/plugins/',
            'libraryDir' => '{baseDir}/{appDir}/common/library/',
            'layoutDir' => '{baseDir}/{appDir}/common/layouts/',
            'partialDir' => '{baseDir}/{appDir}/common/layouts/'
        ];

        array_walk($paths, function(&$value, $key, $replacement) {

            $value = strtr($value, $replacement);
        }, ['{appDir}' => $this->getAppDir(), '{baseDir}' => $this->getBaseDir()]);

        return new PhalconConfig($paths);
    }

    /**
     * @param string $path Value from System configuration
     * 
     * @return mixed
     */
    public function path($path) {

        if (empty($this->systemConfig)) {
            return false;
        }

        return $this->systemConfig->path($path);
    }

}
