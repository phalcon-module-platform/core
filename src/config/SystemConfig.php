<?php

namespace PMP\Core\Config;

use PMP\Core\Config\SystemPaths;
use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config AS ConfigArray;
use Phalcon\Text;
use PMP\Core\Library\FileSystem;

/**
 * class SystemConfig
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class SystemConfig extends SystemPaths {

    /**
     * @var \Phalcon\Config
     */
    private $modules;

    /**
     * @var string
     */
    private $active_module;

    /**
     * @var \Phalcon\Config
     */
    private $aclConfig;

    /**
     * @var \Phalcon\Config
     */
    private $logsConfig;

    /**
     * @var string
     */
    private $aclUsageFile = 'file';

    /**
     * @var string
     */
    private $aclUsageDb = 'db';

    /**
     * @param array $paths [appDir=app, baseDir=server_root, publicPath=public]
     */
    public function __construct($paths = []) {

        foreach ($paths as $key => $value) {

            if (property_exists(get_class(), $key)) {

                $this->{$key} = $value;
            }
        }
    }

    /**
     * @param string $baseDir Full path to project
     */
    public function setBaseDir($baseDir = '') {
        $this->baseDir = $baseDir;
    }

    /**
     * @param string $name
     */
    public function setActiveModule($name) {

        $this->active_module = $name;
    }

    /**
     * @return string Active module name
     */
    public function getActiveModule() {

        return $this->active_module;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getActiveModuleConfig() {

        return $this->getModuleConfig($this->active_module);
    }

    /**
     * Check whenever layout configuration exist for the active module
     * 
     * @return string Active module name
     */
    public function getActiveModuleLayoutConfig() {

        $config = $this->getActiveModuleConfig();

        if (empty($config) || empty($config->path('layout'))) {

            return new ConfigArray();
        }

        $exist = $this->path('layouts.' . $config->path('layout'));

        if ($exist) {

            return $exist;
        }

        $layoutconfig = FileSystem::readJsonFile($this->getLayoutsConfigDir() . $config->path('layout') . '.json');

        $layoutconfig->path('assets')->offsetSet('publicPath', $this->getPublicPathReal());

        //add to config
        $newConfig = new ConfigArray([
            'layouts' => [
                $config->path('layout') => $layoutconfig->toArray()
            ]
        ]);

        $this->updateSystemConfig($newConfig);

        return $layoutconfig;
    }

    /**
     * @return \Phalcon\Config All the modules 
     */
    public function getModules() {

        if (!empty($this->modules)) {

            return $this->modules;
        }

        //gets modules from configuration file
        $modules = new ConfigJson($this->getConfigResourcesDir() . '/modules.json');

        //get core modules
        $coreModules = $this->getCoreModules();

        //merge any change from modules configuration file to core modules
        $coreModules->merge($modules);

        $arrModules = $coreModules->toArray();

        array_walk($arrModules, function(&$value, $key, $replacement) {

            if (!empty($value['path'])) {

                $value['path'] = strtr($value['path'], $replacement);
            }
            if (!empty($value['rootPath'])) {

                $value['rootPath'] = strtr($value['rootPath'], $replacement);
            }

            return $value;
        }, $this->getPathReplacements());

        $this->modules = new ConfigArray($arrModules);

        return $this->modules;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getCoreModules() {


        //gets modules from configuration file
        $modules = new ConfigJson($this->getPackageDir() . '/modules.json');

        $arrModules = $modules->toArray();

        return new ConfigArray($arrModules);
    }

    /**
     * @param string $name Module name
     * 
     * @return \Phalcon\Config Module Configuration
     */
    public function getModuleConfig($name) {

        return $this->getModules()->path($name, false);
    }

    /**
     * Match the module namespace and check if it's core module
     */
    public function isCoreModule() {

        $nspaths = explode('\\', $this->getModuleConfig($this->active_module)->path('namespacesRoot'));

        return !empty($nspaths[0]) && preg_match('/core/', strtolower($nspaths[0])) ? true : false;
    }

    /**
     * Match the module namespace and check if it's app module
     */
    public function isAppModule() {

        $nspaths = explode('\\', $this->getModuleConfig($this->active_module)->path('namespacesRoot'));

        return !empty($nspaths[0]) && preg_match('/app/', strtolower($nspaths[0])) ? true : false;
    }

    /**
     * Check if module is reused from Application
     * 
     * @return string name space to set in dispatcher if Controller exists
     */
    public function isReusedModule($dispatched_module, $dispatched_controller) {

        $class = "App\\Modules\\" . ucfirst($dispatched_module) . "\\Controllers\\" . ucfirst($dispatched_controller) . "Controller";

        return class_exists($class) ?
                "App\\Modules\\" . ucfirst($dispatched_module) . "\\Controllers" :
                false;
    }

    /**
     * Check if console is reused from Application
     * 
     * @param string $dispatched_task_name Description
     * 
     * @return string name space to set in dispatcher if Tasks exists
     */
    public function isReusedConsole($dispatched_task_name) {

        $taskName = ucfirst(Text::camelize($dispatched_task_name, "-"));

        $class = "Console\\Tasks\\" . $taskName . "Task";

        return class_exists($class) ?
                "Console\\Tasks" :
                false;
    }

    /**
     * @param string $name Module name
     * 
     * @return \Phalcon\Config Module Configuration
     */
    public function getModuleFolders() {

        $camelizeFix = lcfirst(Text::camelize($this->active_module, "-"));

        $dirs = [
            'configDir' => $this->getAppFullDir() . '/' . 'modules' . '/' . $camelizeFix . '/' . 'config' . '/',
            'layoutDir' => $this->getAppFullDir() . '/' . 'layouts' . '/' . $this->getTemplateName() . '/',
            'viewDir' => $this->getAppFullDir() . '/' . 'modules' . '/' . $camelizeFix . '/' . 'layouts',
            'partialDir' => $this->getPartialDir() . $this->getTemplateName() . '/'
        ];

        return new ConfigArray($dirs);
    }

    /**
     * Gets from ACL config
     * @return bool Checks whenever ACL must be persistent
     */
    public function getAclUsePersistent() {

        return !empty($this->path('acl.persistent'));
    }

    /**
     * Gets from ACL config
     * @return bool Checks whenever ACL must be used from file
     */
    public function getAclUsageFromFile() {

        return $this->path('cache.acl.use') === $this->aclUsageFile;
    }

    /**
     * Gets from ACL config
     * @return bool Checks whenever ACL must be used from Database
     */
    public function getAclUsageFromDb() {

        return $this->path('cache.acl.use') === $this->aclUsageDb;
    }

    /**
     * @return string full path to Access list JSON file
     */
    public function getAclFile() {

        return $this->getAppFullDir() . '/config/acl.json';
    }

    /**
     * @return bool If acl JSON file exists
     */
    public function isAclFileExists() {

        return file_exists($this->getAclFile());
    }

    /**
     * @return \Phalcon\Config Assets Configuration
     */
    public function getAssetsConfig() {

        $asets = $this->getActiveModuleLayoutConfig();

        //lauout based assets or global assets
        $layoutAssets = $asets->path('assets') ?
                $asets->path('assets') :
                $this->path('assets');

        if (!empty($layoutAssets)) {

            return $layoutAssets;
        }

        $conf = new ConfigJson($this->getConfigResourcesDir() . '/assets.json');

        $conf->offsetSet('publicPath', $this->getPublicPathReal());

        return $conf;
    }

    /**
     * @return string session unique ID
     */
    public function getSessionUniqueId() {

        return $this->getSessionConfig()->path('uniqueId');
    }

    /**
     * @return string session name
     */
    public function getSessionName() {

        return $this->getSessionConfig()->path('name');
    }

    /**
     * @return string name of email provider
     */
    public function getEmailProvider() {

        return $this->path('global.emailProvider');
    }

    /**
     * Gets mail provider configuration Gmail, Swift
     * 
     * @return \Phalcon\Config Email provider Configuration
     */
    public function getEmailProviderConfig($provider) {

        return $this->getSystemConfig()->path('emailProviders.' . $provider, false);
    }

    /**
     * @return \Phalcon\Config Email providers Configuration
     */
    public function getEmailProvidersConfig() {

        if (!empty($this->path('emailProviders'))) {

            return $this->path('emailProviders');
        }

        $getprConfig = new ConfigJson($this->getConfigResourcesDir() . '/emailProviders.json');

        $prConfig = $getprConfig->toArray();

        array_walk($prConfig, function(&$value) {

            if (empty($value['path'])) {

                return false;
            }

            $value['path'] = strtr($value['path'], $this->getPathReplacements());

            return $value;
        });

        return new ConfigArray($prConfig);
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string name of database adapter
     */
    public function getDataBaseAdapter() {

        return $this->path('database.adapter');
    }

    /**
     * 
     * @return string database class name
     */
    public function getDataBaseClass() {

        return 'Phalcon\Db\Adapter\Pdo\\' . $this->getDataBaseAdapter();
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string
     */
    public function getDataBaseHost() {

        return $this->path('database.host');
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string
     */
    public function getDataBaseUser() {

        return $this->path('database.username');
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string
     */
    public function getDataBasePassword() {

        return $this->path('database.password');
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string
     */
    public function getDataBaseName() {

        return $this->path('database.dbname');
    }

    /**
     * Resolve database parameter from configuration
     * 
     * @return string
     */
    public function getDataBaseCharset() {

        return $this->path('database.charset');
    }

    /**
     * Checks whenever database adapter is Postgresql
     * 
     * @return bool
     */
    public function getDataBasePostgre() {

        return $this->path('database.adapter') == 'Postgresql';
    }

    /**
     * Checks whenever database logger is enabled
     * 
     * @return bool
     */
    public function getDataBaseLogsEnabled() {

        return $this->path('logs.db.enable');
    }

    /**
     * Data base credentials and settings
     * 
     * @return \Phalcon\Config 
     */
    public function getDataBaseParams() {

        $params = $this->getDatabaseConfig();

        $params->offsetSet('options', new ConfigArray([
                    \PDO::ATTR_CASE => \PDO::CASE_LOWER,
                    \PDO::ATTR_PERSISTENT => TRUE,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        ]));

        if ($this->getDataBasePostgre() && $params->offsetExists('charset')) {

            $params->offsetUnset('charset');
        }

        return $params;
    }

    /**
     * Data base credentials
     * 
     * @return \Phalcon\Config 
     */
    public function getDatabaseConfig() {

        if (!empty($this->getDataBaseAdapter())) {

            return new ConfigArray([
                'adapter' => $this->getDataBaseAdapter(),
                'host' => $this->getDataBaseHost(),
                'username' => $this->getDataBaseUser(),
                'password' => $this->getDataBasePassword(),
                'dbname' => $this->getDataBaseName(),
                'charset' => $this->getDataBaseCharset()
            ]);
        }

        $envFile = $this->getSystemConfigurableEnvFile();

        if (!file_exists($envFile)) {
            die($this->getEnv() . ' database configuration json is missing');
        }

        $db = new ConfigJson($envFile);

        $params = $db->path('database')->toArray();

        return new ConfigArray($params);
    }

    /**
     * @return \Phalcon\Config Translation configuration
     */
    public function getTranslationConfig() {

        if (!empty($this->path('translation'))) {

            return $this->path('translation');
        }

        $conf = new ConfigJson($this->getConfigResourcesDir() . '/translation.json');

        $toArr = $conf->toArray();

        array_walk($toArr, function(&$value) {

            $value = strtr($value, $this->getPathReplacements());

            return $value;
        });

        return new ConfigArray($toArr);
    }

    /**
     * localhost server path
     * 
     * @return \Phalcon\Config
     */
    public function getSystemPathsConfig() {

        //gets from config if exists
        if (!empty($this->path('systemPaths'))) {

            return $this->path('systemPaths');
        }

        return new ConfigJson($this->getConfigResourcesDir() . '/systemPaths.json');
    }

    /**
     * @return \Phalcon\Config Logs Configuration
     */
    public function getApplicationConfig() {

        if (!empty($this->path('application'))) {

            return $this->path('application');
        }

        $getappConfig = new ConfigJson($this->getConfigResourcesDir() . '/application.json');

        $appConfig = $getappConfig->toArray();

        array_walk($appConfig, function(&$value) {

            $value = strtr($value, $this->getPathReplacements());

            return $value;
        });

        return $appConfig;
    }

    /**
     * @return \Phalcon\Config Global configuration
     */
    public function getGlobalConfig() {

        if (!empty($this->path('global'))) {

            return $this->path('global');
        }

        $global = new ConfigJson($this->getConfigResourcesDir() . '/global.json');

        $toArray = $global->toArray();

        return new ConfigArray($toArray);
    }

    /**
     * @return \Phalcon\Config Logs Configuration
     */
    public function getLogsConfig() {

        if (!empty($this->logsConfig)) {

            return $this->logsConfig;
        }

        if (!empty($this->path('logs'))) {

            $this->logs = $this->path('logs');

            return $this->logs;
        }

        $getlogsConfig = new ConfigJson($this->getConfigResourcesDir() . '/logs.json');

        $logsConfig = $getlogsConfig->toArray();

        array_walk($logsConfig, function(&$value) {

            if (empty($value['path'])) {

                return false;
            }

            $value['path'] = strtr($value['path'], ['{baseDir}' => $this->getBaseDir()]);

            return $value;
        });

        $this->logsConfig = new ConfigArray($logsConfig);

        return $this->logsConfig;
    }

    /**
     * @return \Phalcon\Config System languages
     */
    public function getLanguagesConfig() {

        if (!empty($this->path('languages'))) {

            return $this->path('languages');
        }

        $langs = new ConfigJson($this->getConfigResourcesDir() . '/languages.json');

        return $langs;
    }

    /**
     * Gets Shared services from resources or from already generated System file
     * 
     * @return \Phalcon\Config Shared services managed from panel
     */
    public function getSharedServicesConfig() {

        $serv = new ConfigJson($this->getConfigResourcesDir() . '/sharedServices.json');

        return $serv;
    }

    /**
     * @return \Phalcon\Config Console module Configuration
     */
    public function consoleConfig() {

        $conConfig = new ConfigJson($this->getConfigResourcesDir() . '/console.json');

        //get core console config
        $coreConConfig = new ConfigJson($this->getPackageDir() . '/console.json');

        //merge any change from app console configuration file to core console configuration
        $coreConConfig->merge($conConfig);

        $coreConConfig->offsetSet('rootPath', strtr($coreConConfig->offsetGet('rootPath'), $this->getPathReplacements()));

        return $coreConConfig;
    }

    /**
     * Get the components from system config or from resources folder
     * 
     * @return \Phalcon\Config Components configuration
     */
    public function getComponentsConfig() {

        //gets from config if exists
        if (!empty($this->path('components'))) {

            return $this->path('components');
        }

        return new ConfigJson($this->getConfigResourcesDir() . '/components.json');
    }

    /**
     * @return \Phalcon\Config\Adapter\Json Cache configuration
     */
    public function getCacheConfig() {

        if (!empty($this->path('cache'))) {

            return $this->path('cache');
        }

        $cacheConfig = new ConfigJson($this->getConfigResourcesDir() . '/cache.json');

        $cacheDir = FileSystem::makeDir($this->getBaseDir() . '/' . self::STORAGE_FOLDER . '/cache/models/');

        $cacheConfig->offsetSet('cacheDir', $cacheDir);

        $this->cacheConfig = $cacheConfig;

        return $this->cacheConfig;
    }

    /**
     * @return \Phalcon\Config\Adapter\Json ACL configuration
     */
    public function getAclConfig() {

        if (!empty($this->aclConfig)) {

            return $this->aclConfig;
        }

        if (!empty($this->path('acl'))) {

            $this->aclConfig = $this->path('acl');

            return $this->aclConfig;
        }

        $aclConfig = new ConfigJson($this->getConfigResourcesDir() . '/acl.json');

        $this->aclConfig = $aclConfig;

        return $this->aclConfig;
    }

    /**
     * @return \Phalcon\Config Messages style
     */
    public function getFlashConfig() {

        if (!empty($this->path('flash'))) {

            return $this->path('flash');
        }

        $flash = new ConfigJson($this->getConfigResourcesDir() . '/flash.json');

        $toArray = $flash->toArray();

        return new ConfigArray($toArray);
    }

    /**
     * @return \Phalcon\Config Session configuration
     */
    public function getSessionConfig() {

        if (!empty($this->path('session'))) {

            return $this->path('session');
        }

        $config = new ConfigJson($this->getConfigResourcesDir() . '/session.json');

        $s = $config->toArray();

        return new ConfigArray($s);
    }

    /**
     * @return \Phalcon\Config Named Routes
     */
    public function getRoutesConfig() {

        if ($this->path('routes') instanceof \Phalcon\Config && $this->path('routes')->count() > 0) {

            return $this->path('routes');
        }

        $config = new ConfigJson($this->getConfigResourcesDir() . '/routes.json');

        $r = $config->toArray();

        return new ConfigArray($r);
    }

    /**
     * @return \Phalcon\Config Response headers configuration
     */
    public function getResponseConfig() {

        if (!empty($this->path('response'))) {

            return $this->path('response');
        }

        $config = new ConfigJson($this->getConfigResourcesDir() . '/response.json');

        $r = $config->toArray();

        return new ConfigArray($r);
    }

    /**
     * @param bool $fresh True Gets from resources DIR not from cache
     * 
     * @return \Phalcon\Config HTML Elements class and styles
     */
    public function getElementsConfig($fresh = false) {

        if (!empty($this->path('elements')) && $fresh === false) {

            return $this->path('elements');
        }

        $config = new ConfigJson($this->getConfigResourcesDir() . '/elements.json');

        $r = $config->toArray();

        return new ConfigArray($r);
    }

    /**
     * @return string OS - win or LINUX
     */
    public function getOs() {

        return preg_match('/windows|win/', strtolower(php_uname())) ? 'win' : 'linux';
    }

    /**
     * Gets system configuration from cache.
     * Merge with 
     * - any exist env.json file eg. development.json, staging.json
     * - namespaces
     * 
     */
    public function getFromCache() {

        $sysconfig = new ConfigJson($this->getSystemConfigurableFile());

        //get fresh assets, commented, can be amended from panel
        //$sysconfig->offsetSet('assets', $this->getAssetsConfig());

        $envFile = $this->getSystemConfigurableEnvFile();

        file_exists($envFile) ?
                        $sysconfig->merge(new ConfigJson($envFile)) :
                        '';

        $this->setSystemConfig($sysconfig);
    }

    /**
     * Generate system config file from resourses directory files
     * 
     * @param \Phalcon\Config $config
     */
    public function generateSystemConfig(\Phalcon\Config $config) {

        foreach (glob($this->getConfigResourcesDir() . '/*.json') as $file) {

            if (!is_readable($file)) {
                continue;
            }

            $file = pathinfo($file, PATHINFO_FILENAME);

            $method = 'get' . ucfirst($file) . 'Config';

            if (!method_exists(get_class(), $method)) {
                continue;
            }

            $config->offsetSet($file, $this->{$method}());
        }

        $envFile = $this->getSystemConfigurableEnvFile();

        file_exists($envFile) ?
                        $config->merge(new ConfigJson($envFile)) :
                        '';

        $this->setSystemConfig($config);

        FileSystem::writeFile($this->getSystemConfigurableFile(), json_encode($config->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        return true;
    }

    /**
     * Merge existing system configuration and generate new system file
     * 
     * @param \Phalcon\Config $resourse
     */
    public function updateSystemConfig(\Phalcon\Config $resourse) {

        $merged = $this->getSystemConfig()->merge($resourse);

        FileSystem::writeFile($this->getSystemConfigurableFile(), json_encode($merged->toArray(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));

        return $merged;
    }

}
