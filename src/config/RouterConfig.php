<?php

/**
 * 
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 *  
 */

namespace PMP\Core\Config;

use Phalcon\Config;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\Router\Group AS RouterGroup;

class RouterConfig extends Router {

    /**
     * @var string 
     */
    private $default_module = 'home';

    /**
     * @var string 
     */
    private $default_controller = 'index';

    /**
     * @var string 
     */
    private $default_action = 'index';

    /**
     * @var \PMP\Core\Config\SystemConfig
     */
    private $systemConfig;

    /**
     * @param \Phalcon\Di\FactoryDefaul $di
     * @param bool $defaultRoutes
     */
    public function __construct(\Phalcon\DiInterface $di, $defaultRoutes = false) {

        parent::__construct($defaultRoutes);

        $this->removeExtraSlashes(true);

        $this->setUriSource(Router::URI_SOURCE_SERVER_REQUEST_URI);

        //set Dependency injector
        $this->setDI($di);

        $this->systemConfig = $this->getDI()->getShared('systemConfig');
    }

    /**
     * Sets the default Module, Controller, Action
     * 
     * @param \Phalcon\Config $params
     */
    public function setConfig($params = false) {

        $defaults = $params instanceof \Phalcon\Config ? $params : new Config();

        $this->setDefaultModule($defaults->path('module', $this->default_module));

        $this->setDefaultController($defaults->path('controller', $this->default_controller));

        $this->setDefaultAction($defaults->path('action', $this->default_action));
    }

    /**
     * @return string Default module where has been set or FALSE
     */
    public function getDefaultModule() {

        $defaults = new Config($this->getDefaults());

        return $defaults->path('module', false);
    }

    /**
     * @return string Default Controller where has been set or FALSE
     */
    public function getDefaultController() {

        $defaults = new Config($this->getDefaults());

        return $defaults->path('controller', false);
    }

    /**
     * @return string Default Action where has been set or FALSE
     */
    public function getDefaultAction() {

        $defaults = new Config($this->getDefaults());

        return $defaults->path('action', false);
    }

    public function route($uri = null) {

        $route = new RouterGroup();

        $route->add('/:module', [
            'module' => 1
        ]);

        $route->add('/:module/:controller', [
            'module' => 1,
            'controller' => 2
        ]);

        $route->add('/:module/:controller/:action', [
            'module' => 1,
            'controller' => 2,
            'action' => 3
        ]);

        $route->add('/:module/:controller/:action/:params', [
            'module' => 1,
            'controller' => 2,
            'action' => 3,
            'params' => 4
        ]);
        
        
        $namedRoutes = $this->systemConfig->getRoutesConfig();

        foreach ($namedRoutes as $pattern => $options) {

            if (empty($options) || empty($options->path('path')) || empty($options->path('name'))) {
                continue;
            }
            $params = [
                '/' . $pattern,
                array_filter($options->path('path')->toArray()),
                !empty($options->path('via')) ? array_filter($options->path('via')->toArray()) : false
            ];
                       
            $route->add(...array_filter($params))
                    ->setName($options->path('name'));
        }
        
        $this->mount($route);

        $this->handle($uri);
    }

}
