<?php

namespace PMP\Core\Controllers;

class ErrorController extends CommonBase {

    public function onConstruct() {

        $this->flashSession->clear();
    }

    public function indexAction($message = null) {

        $response = [
            'error' => true,
            'message' => $message,
            'notifytitle' => $this->message('REQUEST_ACCESS_DENIED')
        ];

        $this->viewToAjax->setLayout('error/page-not-found');

        $this->viewToAjax->setVariables($response);
        
        $this->viewToAjax->addToParams($response);

        return $this->viewToAjax->returnToView();
    }

    public function exceptionAction(\Exception $exception = null) {

        $message = (!empty($exception)) ? $exception->getMessage() : $this->message('REQUEST_PAGE_NOT_FOUND');

        $response = [
            'error' => true,
            'message' => $message,
            'notifytitle' => $this->message('REQUEST_ACCESS_DENIED')
        ];

        $this->viewToAjax->setLayout('error/page-not-found');

        $this->viewToAjax->setVariables($response);
        
        $this->viewToAjax->addToParams($response);

        return $this->viewToAjax->returnToView();
    }

}
