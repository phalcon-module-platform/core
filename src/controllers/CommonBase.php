<?php

namespace PMP\Core\Controllers;

use Phalcon\Mvc\Controller;
use PMP\Core\Plugins\Translate;

/**
 * @property \Phalcon\Config $config
 * @property \Phalcon\Registry $registry
 * @property \Phalcon\Mvc\Dispatcher $dispatcher
 * @property \Phalcon\Tag $tag
 * @property \PMP\Core\Config\SystemConfig $systemConfig
 * @property \PMP\Core\Config\RouterConfig $router
 * @property \PMP\Core\Plugins\Profile\ProfileDetails $profile
 * @property \PMP\Core\Plugins\AccessList $acl
 * @property \PMP\Core\Plugins\Assets $assets
 * @property \PMP\Core\Plugins\Translate $translate
 * @property \PMP\Core\Plugins\Cache $cache
 * @property \PMP\Core\Plugins\ResponsePlugin $response
 * @property \PMP\Core\Plugins\ViewToAjax $viewToAjax
 * @property \PMP\Core\Plugins\FiltersPlugin $filters
 * @property \PMP\Core\Plugins\ConsolePlugin $console
 * @property \PMP\Core\Plugins\Session $session
 * @property \PMP\Core\Plugins\Profile\SessionProfile $sessionProfile
 * @property \PMP\Core\Plugins\SystemMessages $systemMessage
 * @property \PMP\Core\Plugins\Elements\ElementsPlugin $elements
 * @property \PMP\Core\Plugins\UrlPlugin $url
 * @property \PMP\Core\Plugins\IpData $ipdata
 * @property \PMP\Core\Models\User $user
 * @property \PMP\Core\Providers\Mail\MailManagerInterface $mailManager
 * 
 * @property \PMP\Core\Plugins\DataTable $dataTable
 */
class CommonBase extends Controller {

    public function onConstruct() {
        
    }

    /**
     * Direct user to error page
     * 
     * @param string $message Message to display
     * @param array $route Routing parameters.
     */
    public function setError($message, $route = []) {

        $this->flashSession->clear();

        $this->flashSession->warning($message);


        $this->dispatcher->forward([
            'namespace' => !empty($route[0]) ? $route[0] : '\PMP\Core\Controllers',
            'controller' => !empty($route[1]) ? $route[1] : 'error',
            'action' => !empty($route[2]) ? $route[2] : 'index',
            'params' => [$message]
        ]);
    }

    /**
     * @param string $var String to translate
     */
    public function translate($var = '') {

        return Translate::t($var);
    }

    /**
     * Gets the message from System Messages PLUGIN and translate it
     * 
     * @param string $variable_name variable name
     */
    public function message($variable_name) {

        $message = $this->systemMessage->get($variable_name);

        return Translate::t($message);
    }

    public function dictionary($var = '') {
        
    }

}
