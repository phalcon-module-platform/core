<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\AccessList;

/**
 * class Acl
 */
class Acl implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('acl', function () {

            $accessList = new AccessList();

            $accessList->set();

            return $accessList;
        });
    }

}
