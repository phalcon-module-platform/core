<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\ViewToAjax AS VA;

/**
 * class ViewToAjax
 */
class ViewToAjax implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('viewToAjax', function () {

            /* @var $router \PMP\Core\Config\RouterConfig */
            $router = $this->getRouter();

            /* @var $view \Phalcon\Mvc\View */
            $view = $this->getView();

            $parts = [
                $router->getModuleName() . '/' . $router->getControllerName() . '/' . $router->getActionName(),
                $router->getModuleName() . '/' . $router->getControllerName(),
                $router->getModuleName()
            ];

            $layout = false;

            foreach ($parts as $layouts) {
                
                if ($view->exists($layouts)) {
                    
                    $layout = $layouts;
                    
                    break;
                }
            }

            /* @var $va \PMP\Core\Services\ViewToAjax  */
            $va = new VA();

            $va->setLayout($layout);

            return $va;
        });
    }

}
