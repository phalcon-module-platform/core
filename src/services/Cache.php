<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Cache AS CachePlugin;

/**
 * class Cache
 */
class Cache implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        $config = $systemConfig->getCacheConfig();

        $di->setShared('cache', function () use ($config) {

            $cache = new CachePlugin();

            $cache->setConfig($config);

            return $cache;
        });
    }

}
