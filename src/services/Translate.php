<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Translate AS TranslatePlugin;

/**
 * class Translate
 */
class Translate implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $config \Phalcon\Config */
        $config = $systemConfig->getSystemConfig()->path('translation');

        $di->setShared('translate', function () use ($config) {

            $translate = new TranslatePlugin();

            $translate->setConfig($config);

            return $translate;
        });
    }

}
