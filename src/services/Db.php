<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Listeners\DatabaseListener;

/**
 * class Db
 */
class Db implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('db', function () {

            /* @var $this \Phalcon\Di\FactoryDefault */

            /* @var $config \Core\Config\SystemConfig */
            $config = $this->getSystemConfig();

            $class = $config->getDataBaseClass();

            $params = $config->getDataBaseParams()->toArray();

            //
            try {

                /* @var $connection \Phalcon\Db\Adapter\Pdo\Mysql  */
                $connection = new $class($params);
                //
            } catch (\PDOException $exc) {

                $connection = false;

                die($exc->getMessage() . ':' . $exc->getFile());
            }

            if (empty($connection)) {

                return false;
            }

            if ($config->getDataBaseLogsEnabled()) {
                
                $listener = new DatabaseListener($config->getLogsConfig()->path('db'));
                
                //Create an EventManager
                $eventsManager = $this->getShared('eventsManager');

                $eventsManager->attach('db', $listener);

                $connection->setEventsManager($eventsManager);
            }

            return $connection;
        });
    }

}
