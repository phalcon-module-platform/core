<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\User\UserPlugin;

/**
 * Shared services User
 */
class User implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('user', function () {

            $user = new UserPlugin();

            return $user->load();
            
        });
    }

}
