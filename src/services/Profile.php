<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Profile\ProfileDetails;

/**
 * class Profile
 */
class Profile implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('profile', function () {

            $profile = new ProfileDetails();

            return $profile;
        });
    }

}
