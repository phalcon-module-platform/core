<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;

/**
 * class Registry
 */
class Registry implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('registry', function () {

            return new \Phalcon\Registry();
        });
    }

}
