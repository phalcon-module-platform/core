<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\FlashSessionPlugin;

/**
 * class FlashSession
 */
class FlashSession implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $config \Phalcon\Config */
        $config = $systemConfig->getSystemConfig()->path('flash.html');

        $di->setShared('flashSession', function () use ($config) {

            $flash = new FlashSessionPlugin();

            $flash->setConfig($config->toArray());

            return $flash;
        });
    }

}
