<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Session AS SessionPlugin;

/**
 * class Session
 */
class Session implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $config \Phalcon\Config */
        $config = $systemConfig->getSessionConfig();

        $savePath = $systemConfig->getSessionSavePath();

        $di->setShared('session', function () use ($config, $savePath) {

            $session = new SessionPlugin();

            $session->setSessionSavePath($savePath);

            $session->setOptions($config->toArray());

            $session->start();

            return $session;
        });
    }

}
