<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\ResponsePlugin;

/**
 * class Response
 */
class Response implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $config \Phalcon\Config */
        $config = $systemConfig->getResponseConfig();

        $di->setShared('response', function () use ($config) {

            $response = new ResponsePlugin();

            $response->setConfig($config);

            return $response;
        });
    }

}
