<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\ConsolePlugin;

/**
 * class Console
 */
class Console implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('console', function () {

            return new ConsolePlugin();
        });
    }

}
