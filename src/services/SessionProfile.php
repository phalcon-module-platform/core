<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use Phalcon\Config;
use PMP\Core\Plugins\Profile\SessionProfile AS SP;

/**
 * class SessionProfile
 */
class SessionProfile implements ServiceProviderInterface {

    public function register(DiInterface $di) {
        
        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $adapter \Phalcon\Session\Adapter\Files */
        $adapter = $di->getSession();

        /* @var $usersession  array*/
        $usersession = $adapter->has($systemConfig->getSessionName()) ?
                $adapter->get($systemConfig->getSessionName()) :
                false;

        $di->setShared('sessionProfile', function () use($usersession) {

            $profile = !empty($usersession) ?
                    new Config($usersession) :
                    new Config([]);

            $sp = new SP($profile);

            return $sp;
        });
    }

}
