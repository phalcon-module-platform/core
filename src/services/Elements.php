<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Elements\ElementsPlugin;

/**
 * class HtmlElements
 */
class Elements implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        /* @var $config \Phalcon\Config */
        $elementsConfig = $systemConfig->getElementsConfig();

        $di->setShared('elements', function () use ($elementsConfig) {

            $manager = new ElementsPlugin();
            
            $manager->setConfig($elementsConfig);
            
            return $manager;
            
        });
    }

}
