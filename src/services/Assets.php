<?php

namespace PMP\Core\Services;

use Phalcon\Config;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\Assets AS AssetsPlugin;

/**
 * class Assets
 */
class Assets implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemConfig = $di->getSystemConfig();

        $config = $systemConfig->getAssetsConfig();

        $load = new Config();

        $load->offsetSet('module', $di->getRouter()->getModuleName());

        $load->offsetSet('controller', $di->getDispatcher()->getControllerName());

        $load->offsetSet('action', $di->getDispatcher()->getActionName());

        $di->setShared('assets', function() use ($config, $load) {

            $assets = new AssetsPlugin();

            $assets->setConfig($config, $load);

            $assets->loadCollections();

            return $assets;
        });
    }

}
