<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;

/**
 * class MailManagerService Shared service for Core\Providers\Mail\MailManagerInterface
 */
class MailManagerService implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        /* @var $systemConfig \PMP\Core\Config\SystemConfig */
        $systemconfig = $di->getShared('systemConfig');

        /* @var $provider string email provider name */
        $provider = $systemconfig->getEmailProvider();

        $config = $systemconfig->getEmailProviderConfig($provider);

        $di->setShared('mailManager', function () use($provider, $config) {

            $class = 'PMP\\Core\\Providers\Mail\\' . $provider;

            /* @var $instance Core\Providers\Mail\MailManagerInterface */
            $instance = new $class();

            $instance->setConfig($config);

            return $instance;
        });
    }

}
