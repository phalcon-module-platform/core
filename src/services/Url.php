<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\UrlPlugin;

/**
 * class Registry
 */
class Url implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('url', function () {
            
            $url = new UrlPlugin();
            
            $url->setBaseUri('/');

           
            return $url;
        });
    }

}
