<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\SystemMessages AS SM;

/**
 * class Acl
 */
class SystemMessages implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('systemMessage', function () {

            return new SM();
            
        });
    }

}
