<?php

namespace PMP\Core\Services;

use Phalcon\Di\ServiceProviderInterface;
use Phalcon\DiInterface;
use PMP\Core\Plugins\FiltersPlugin;

/**
 * class Filters
 */
class Filters implements ServiceProviderInterface {

    public function register(DiInterface $di) {

        $di->setShared('filters', function () {

            return new FiltersPlugin();
        });
    }

}
