<?php

namespace PMP\Core\Cli;

use Phalcon\Cli\Console AS PhalconConsole;
use Phalcon\Cli\Router;
use Phalcon\Di\FactoryDefault\Cli;
use Phalcon\Config;
use PMP\Core\Config\SystemConfig;

class Console extends Cli {

    private $baseDir;

    /**
     * Sets project base dir
     * 
     * @param string $baseDir Description
     */
    public function setBaseDir($baseDir) {

        $this->baseDir = $baseDir;
    }

    /**
     * Share main services [SystemConfig, Router, Console]
     */
    public function shareServices() {

        $this->setShared('systemConfig', function () {

            $sc = new SystemConfig();

            $sc->setBaseDir($this->baseDir);

            if ($sc->configCacheExists()) {

                $sc->getFromCache();

                return $sc;
            }

            $config = new Config([]);

            $sc->generateSystemConfig($config);

            return $sc;
        });

        $this->setShared('router', function() {

            $route = new Router();

            $route->handle();

            return $route;
        });


        $this->setShared('console', function() {

            /* @var $config \PMP\Core\Config\SystemConfig */
            $config = $this->getSystemConfig();

            $modules = $config->consoleConfig()->toArray();

            $console = new PhalconConsole();

            $console->registerModules(['cli' => $modules]);

            $console->setDefaultModule('cli');

            return $console;
        });
    }

}
