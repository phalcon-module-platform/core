<?php

namespace PMP\Core\Cli\Plugins;

use PMP\Core\Plugins\PluginInterface;
use Phalcon\Text;

class DispatcherPlugin extends PluginInterface {

    /**
     * @var string Task to execute
     */
    protected $task;

    /**
     * @var string Action to execute
     */
    protected $action;

    /**
     * @var array Params
     */
    protected $params = [];

    /**
     * Triggered before entering in the dispatch loop.<br />
     * 
     * @param \Phalcon\Events\Event $event
     * @param \Phalcon\Cli\Dispatcher $dispatcher
     * @param \Phalcon\Cli\Dispatcher\Exception $exception
     */
    public function beforeDispatchLoop($event, $dispatcher, $exception) {

        $this->dispatchArguments();

        if ($this->task) {
            $dispatcher->setTaskName($this->task);
        }

        if ($this->action) {
            $dispatcher->setActionName(\lcfirst(Text::camelize($this->action, '-')));
        }

        //if class exists to Application console, change the namespace
        $ns = $this->systemConfig->isReusedConsole($dispatcher->getTaskName());

        if ($ns) {
            $dispatcher->setDefaultNamespace($ns);
        }

        $dispatcher->setParams($this->params);
    }

    /**
     * Trigger an action before exception.If controller and action are not found, forward to Index Controller<br />
     * 
     * @param \Phalcon\Events\Event $event
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @param \Phalcon\Mvc\Dispatcher\Exception $exception
     */
    public function beforeException($event, $dispatcher, $exception) {
        
    }

    public function beforeExecuteRoute() {
        
    }

    public function afterExecuteRoute() {
        
    }

    public function afterDispatchLoop() {
        
    }

    /**
     * Resolve the arguments and build array to handle the task and action
     * 
     * @return array
     */
    private function dispatchArguments() {

        //get params from router
        $params = $this->router->getParams();

        if (empty($params[1])) {
            return false;
        }

        // remove first key where the name of php file is
        unset($params[0]);

        $route = [
            'task' => $params[1]
        ];

        $this->task = $params[1];

        unset($params[1]);

        if (!empty($params[2])) {

            $route['action'] = $params[2];

            $this->action = $params[2];

            unset($params[2]);
        }

        if (count($params) > 0) {

            $route['params'] = array_values($params);

            $this->params = array_values($params);
        }

        return $route;
    }

}
