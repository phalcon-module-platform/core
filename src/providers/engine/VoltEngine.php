<?php

namespace PMP\Core\Providers\Engine;

use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\ViewBaseInterface;
use Phalcon\Mvc\View\Engine\Php;
use Phalcon\DiInterface;

/**
 * class VoltEngine
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class VoltEngine extends Volt {

    /**
     * @var \PMP\Core\Config\SystemConfig     
     */
    protected $systemConfig;

    public function __construct(ViewBaseInterface $view, DiInterface $di = null) {

        parent::__construct($view, $di);

        /* call the magic method */
        $this->systemConfig = $di->getSystemConfig();
    }

    /**
     * @return array view engine
     */
    public function getEngine() {


        return [
            '.volt' => $this,
            '.phtml' => Php::class
        ];
    }

    /**
     * Register volt functions
     * 
     */
    public function registerFunctions() {

        $compiler = $this->getCompiler();

        $compiler->addFilter('gettype', function ($resolvedArgs, $exprArgs) {
            return 'gettype(' . $resolvedArgs . ')';
        });
        
        $compiler->addFilter('print', function ($resolvedArgs, $exprArgs) {
            return 'print_r(' . $resolvedArgs . ')';
        });
        
        $compiler->addFilter('dump', function ($resolvedArgs, $exprArgs) {
            return 'var_dump(' . $resolvedArgs . ')';
        });

        $compiler->addFunction('t', function($resolvedArgs) {
            return "\\PMP\\Core\\Plugins\\Translate::t({$resolvedArgs})";
        });

        $compiler->addFunction('t', function($resolvedArgs) {
            return "\\PMP\Core\\Plugins\\Translate::t({$resolvedArgs})";
        });

        $compiler->addFunction('sysMessage', function($resolvedArgs) {
            return "\\PMP\Core\\Plugins\\SystemMessages::sysMessage({$resolvedArgs})";
        });

        $compiler->addFunction('currencyToSymbol', function($resolvedArgs) {
            return "\\PMP\Core\\Plugins\\Pricing\\PricingHelper::currencyToSymbol({$resolvedArgs})";
        });

        $compiler->addFunction('widget', function($resolvedArgs) {

            return "\\PMP\Core\\Library\\WidgetManager::load({$resolvedArgs})";
        });


        $compiler->addFunction('strtotime', 'strtotime');
    }

}
