<?php

namespace PMP\Core\Providers\Mail;

/**
 * MailManagerConfig
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class MailManagerConfig {

    /**
     * @var \Phalcon\Config Object
     */
    private $managerConfig;

    /**
     * @var \Phalcon\Config Object
     */
    private $recipient;

    /**
     * @var array
     */
    private $errors;

    /**
     * Sets provider configuration
     * 
     * @param \Phalcon\Config $config Mail provider configuration
     */
    public function setConfig(\Phalcon\Config $config) {

        $this->managerConfig = $config;
    }

    /**
     *
     * @return \Phalcon\Config Provider configuration 
     */
    public function getConfig() {

        return $this->managerConfig;
    }

    /**
     * @return \Phalcon\Config Recipient details 
     */
    public function getRecipient() {
        return $this->recipient;
    }

    /**
     * Sets Recipient configuration
     * 
     * @param \Phalcon\Config $recipient [mail, subject, template]
     */
    public function setRecipient(\Phalcon\Config $recipient) {
        $this->recipient = $recipient;
    }

    /**
     * @return string
     */
    public function getRecipientEmail() {
        return $this->getRecipient()->path('email');
    }

    /**
     * @return string
     */
    public function getRecipientSubject() {
        return $this->getRecipient()->path('subject');
    }

    /**
     * @return string
     */
    public function getRecipientTemplate() {
        return $this->getRecipient()->path('template');
    }

    /**
     * @return array List with error messages
     */
    public function getErrors() {

        return $this->errors;
    }

    /**
     *
     * @return string
     */
    public function getFirstError() {

        return (!empty($this->errors[0])) ? $this->errors[0] : false;
    }

    /**
     *
     * @return string
     */
    public function getHost() {

        return $this->getConfig()->path('host');
    }

    /**
     *
     * @return string
     */
    public function getPort() {

        return $this->getConfig()->path('port');
    }

    /**
     * @return string
     */
    protected function getFromName() {

        return $this->getConfig()->path('from.name');
    }

    /**
     *
     * @return string
     */
    protected function getFromEmail() {

        return $this->getConfig()->path('from.email');
    }

    /**
     *
     * @return string
     */
    protected function getEncryption() {

        return $this->getConfig()->path('encryption');
    }

    /**
     *
     * @return string
     */
    protected function getUsername() {

        return $this->getConfig()->path('username');
    }

    /**
     *
     * @return string
     */
    protected function getPassword() {

        return $this->getConfig()->path('password');
    }

    /**
     * @param string $error Error message
     */
    protected function setError($error) {

        if (empty($error)) {

            return false;
        }

        $this->errors[] = trim($error);
    }

}
