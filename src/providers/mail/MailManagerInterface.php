<?php

namespace PMP\Core\Providers\Mail;

/**
 * MailManagerInterface
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
interface MailManagerInterface {

    /**
     * Sets provider configuration
     * 
     * @param \Phalcon\Config $config Mail provider configuration
     */
    public function setConfig(\Phalcon\Config $config);

    /**
     * Sets Recipient configuration
     * 
     * @param \Phalcon\Config $recipient Recipient options [mail, subject, template]
     */
    public function setRecipient(\Phalcon\Config $recipient);

    /**
     * @return \Phalcon\Config Provider configuration 
     */
    public function getConfig();

    /**
     * @return string
     */
    public function getHost();

    /**
     * @return string
     */
    public function getPort();

    /**
     * @return \Phalcon\Config Provider configuration 
     */
    public function getRecipient();

    /**
     * @return array List with error messages
     */
    public function getErrors();

    /**
     *
     * @return string
     */
    public function getFirstError();

    /**
     * Send email
     * 
     * @return mixed
     */
    public function send();







    //
}
