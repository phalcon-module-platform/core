<?php

namespace PMP\Core\Providers\Mail;

use PMP\Core\Providers\Mail\MailManagerConfig;
use Swift_Message;
use Swift_SmtpTransport;
use Swift_Mailer;
use Swift_TransportException;

class Swift extends MailManagerConfig implements MailManagerInterface {

    /**
     * Send email
     * 
     * @return mixed
     */
    public function send() {

        //prepare Swift message
        $message = new Swift_Message();

        $message->setSubject($this->getRecipientSubject());

        $message->setTo($this->getRecipientEmail());

        $message->setFrom([$this->getFromEmail() => $this->getFromName()]);

        $message->setBody($this->getRecipientTemplate(), 'text/html', 'utf-8');

        //prepare the transport message
        $transport = new Swift_SmtpTransport($this->getHost(), (int) $this->getPort(), $this->getEncryption());

        $transport->setTimeout(3);

        $transport->setUsername($this->getUsername());

        $transport->setPassword($this->getPassword());

        $mailer = new Swift_Mailer($transport);


        try {

            $mailer->send($message);

            //
        } catch (Swift_TransportException $exc) {

            $this->setError($exc->getMessage());

            return false;
        }
        
        return true;
    }

}
