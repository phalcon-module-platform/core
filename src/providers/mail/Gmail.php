<?php

namespace PMP\Core\Providers\Mail;

use PMP\Core\Providers\Mail\MailManagerConfig;
use League\OAuth2\Client\Provider\Google;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\OAuth;

class Gmail extends MailManagerConfig implements MailManagerInterface {

    /**
     * Send email
     * 
     * @return mixed
     */
    public function send() {

        $mail = new PHPMailer(true);

        $mail->XMailer = $this->getConfig()->path('xMailer', false);

        $mail->setFrom($this->getFromEmail(), $this->getFromName());

        $mail->addAddress($this->getRecipientEmail());

        $mail->Subject = $this->getRecipientSubject();

        $mail->isHTML();

        $mail->CharSet = 'UTF-8';

        $mail->Encoding = 'base64';

        $mail->Body = $this->getRecipientTemplate();

        $mail->isSMTP();

        $mail->Port = (int) $this->getPort();

        $mail->SMTPAuth = true;

        $mail->SMTPSecure = $this->getEncryption();

        /* Google's SMTP */
        $mail->Host = $this->getHost();

        /* Set AuthType to XOAUTH2. */
        $mail->AuthType = 'XOAUTH2';

        /* Create a new OAuth2 provider instance. */
        $provider = new Google([
            'clientId' => $this->getConfig()->path('clientId'),
            'clientSecret' => $this->getConfig()->path('clientSecret')
        ]);

        $mail->setOAuth(new OAuth([
            'provider' => $provider,
            'clientId' => $this->getConfig()->path('clientId'),
            'clientSecret' => $this->getConfig()->path('clientSecret'),
            'refreshToken' => $this->getConfig()->path('refreshToken'),
            'userName' => $this->getConfig()->path('username')
        ]));


        try {

            $mail->send();

            //
        } catch (\Exception $exc) {

            $this->setError($exc->getMessage());

            return false;
        }

        return true;
    }

}
