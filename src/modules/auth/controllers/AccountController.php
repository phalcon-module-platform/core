<?php

namespace PMP\Core\Modules\Auth\Controllers;

use PMP\Core\Models\User;
use PMP\Core\Plugins\Forms\RegisterForm;
use PMP\Core\Plugins\Forms\PasswordChangeForm;
use PMP\Core\Plugins\User\LoginHistory;
use PMP\Core\Plugins\User\AccountNotifications;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Plugins\User\UserSecurity;
use PMP\Core\Plugins\DynamicModal;
use PMP\Core\Library\Constants\Redirect;

class AccountController extends ControllerBase {

    /**
     * 
     */
    public function indexAction() {
        
    }

    /**
     * 
     */
    public function logoutAction() {

        $loginId = $this->session->hasLoginId($this->systemConfig->getSessionName());

        if ($loginId) {

            $loginHistory = new LoginHistory();

            $loginHistory->saveLogout($loginId);
        }

        $this->profile->clearProfileCache();

        $this->session->destroy();

        if ($this->cookies->has('restoreId')) {
            $this->cookies->delete('restoreId');
        }

        $message = $this->message('LOGOUT_SUCCESS');

        if ($this->request->isAjax()) {

            $this->response->setRedirectUrl('/');

            return [
                'message' => $message
            ];
        }

        $this->flashSession->success($this->message('LOGOUT_SUCCESS'));

        $this->response->redirect('/')->send();

        die();
    }

    /**
     * 
     */
    public function createAccountAction() {

        $form = new RegisterForm();

        $messages = $form->validate($this->request->getPost());

        $this->response->setError();

        if ($messages->count() > 0) {
            return ['message' => $this->message($messages->current()->getMessage())];
        }

        /* @var $existsuser \PMP\Core\Models\User */
        $existsuser = User::findFirstByEmail($this->request->getPost('email'));

        if ($existsuser) {

            return [
                'message' => $this->message('REGISTER_ERROR_ACCOUNT_EXISTS')
            ];
        }

        $user = new User();

        $user->setFullName($this->request->getPost('full_name'));

        $user->setEmail($this->request->getPost('email'));

        //TODO: set hash for email confirmation
        $tokenEmail = $this->security->getToken();

        $user->setRecoverHash($tokenEmail);

        $user->setStatus(UserConstants::status_register);

        //TODO: set role registered user, not by id
        $user->setRole(UserConstants::role_user);

        $user->setPassword($this->security->hash($this->request->getPost('password')));

        $user->setDateCreated(date('Y-m-d H:i:s'));

        $user->save();

        $errors = $user->getMessages();

        if (count($errors)) {
            return [
                'message' => $this->message('REGISTER_ERROR_EMAIL'),
            ];
        }

        $alert = new AccountNotifications();

        $alert->setUser($user)
                ->setSubject($this->message('EMAIL_SUBJECT_NEW_REGISTRATION'))
                ->setToken($tokenEmail)
                ->setLayout($this->systemMessage->get('EMAIL_LAYOUT_NEW_REGISTRATION'));//email/new-registration

        $issend = $alert->send();

        if (empty($issend) || !empty($issend->getErrors())) {
            return [
                'message' => $this->message('REGISTER_ERROR_SEND_EMAIL'),
                'form' => 'reset'
            ];
        }

        $this->response->setResetForm();
        return [
            'success' => true,
            'message' => $this->message('REGISTER_TRIAL_SUCCESS')
        ];
    }

    /**
     * Register User
     */
    public function registerAction() {

        return $this->viewToAjax->returnToView();
    }

    /**
     * User account confirmation after register
     */
    public function confirmAction() {

        if (!$this->request->isAjax()) {
            return;
        }

        $this->response->setError();

        if (empty($this->request->getPost('recover_hash'))) {

            $this->viewToAjax->setLayout('user/modals/confirm-account');

            $dynamicModal = new DynamicModal($this->request->getPost('dynamicModal'));

            $dynamicModal->setModalContent($this->viewToAjax->_getView());

            $this->response->setOpenModal($dynamicModal->getModalParams());

            return ['message' => $this->message('CONFIRM_ACCOUNT_ERROR_MISSING_HASH')];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstByRecoverHash($this->request->getPost('recover_hash'));

        if (empty($user)) {

            $this->viewToAjax->setLayout('user/modals/confirm-account');

            $dynamicModal = new DynamicModal($this->request->getPost('dynamicModal'));

            $dynamicModal->setModalContent($this->viewToAjax->_getView());

            $this->response->setOpenModal($dynamicModal->getModalParams());

            return ['message' => $this->message('CONFIRM_ACCOUNT_TOKEN_NOT_FOUND')];
        }

        $valid = $this->security->checkHash($this->request->getPost('password'), $user->getPassword());

        if (empty($valid)) {

            $this->viewToAjax->setLayout('user/modals/confirm-account');

            $dynamicModal = new DynamicModal($this->request->getPost('dynamicModal'));

            $dynamicModal->setModalContent($this->viewToAjax->_getView());

            $this->response->setOpenModal($dynamicModal->getModalParams());

            return ['message' => $this->message('CONFIRM_ACCOUNT_TOKEN_PASS_NOT_MATCH')];
        }


        $user->setRecoverHash(NULL);

        $user->setStatus(UserConstants::status_active);

        $user->save();

        $security = new UserSecurity($user);

        $security->setSession();

        $this->response->setRedirectUrl(Redirect::AFTER_LOGIN);

        return [
            'success' => true,
            'message' => $this->message('LOGIN_SUCCESS')
        ];
    }

    /**
     * Password recover
     */
    public function recoverAction() {

        if (!$this->request->isAjax()) {
            return;
        }

        if (empty($this->request->getPost('recover_hash'))) {

            $this->response->setError();

            return ['message' => $this->message('RECOVER_ERROR_MISSING_TOKEN')];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstByRecoverHash($this->request->getPost('recover_hash'));

        if (empty($user)) {

            $this->response->setError();

            return ['message' => $this->message('RECOVER_ERROR_WRONG_TOKEN')];
        }

        if ($user->getRecoverExpire() < date('Y-m-d H:i:s')) {

            $this->response->setError();

            return [
                'message' => $this->message('RECOVER_ERROR_TOKEN_EXPIRE'),
                'form' => 'reset'
            ];
        }

        $form = new PasswordChangeForm();

        $messages = $form->validate($this->request->getPost());

        if ($messages->count() > 0) {

            $this->response->setError();

            return [
                'message' => $this->message($messages->current()->getMessage())
            ];
        }

        $user->setRecoverHash(NULL);
        $user->setPassword($this->security->hash($this->request->getPost('password')));
        $user->setDateUpdated(date('Y-m-d H:i:s'));
        $user->setStatus(UserConstants::status_active);
        $user->save();

        return [
            'success' => true,
            'message' => $this->message('RECOVER_ACCOUNT_SUCCESS'),
            'form' => 'reset',
            'hash' => $this->security->checkHash($user->getEmail(), $this->request->getPost('recover_hash'))
        ];
    }

    /**
     * Password change
     */
    public function changePasswordAction() {

        if (!$this->request->isAjax()) {
            return;
        }


        $form = new PasswordChangeForm();

        $messages = $form->validate($this->request->getPost());

        if ($messages->count() > 0) {

            $this->response->setError();

            return [
                'message' => $this->message($messages->current()->getMessage())
            ];
        }

        $this->profile->clearUserCache();

        $this->user->setPassword($this->security->hash($this->request->getPost('password')));

        $this->user->setDateUpdated(date('Y-m-d H:i:s'));

        $this->user->save();

        return [
            'success' => true,
            'message' => $this->message('RECOVER_PASSWORD_SUCCESS'),
            'form' => 'reset'
        ];
    }

    /**
     * Password reset from link auth/account/reset/
     * - generate token (16 char) and keep to dB
     * - crypt the token and send crypted value to the user email address
     * - set set token expire time 
     */
    public function resetAction() {

        if (empty($this->request->getPost('email'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_EMAIL')];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstByEmail($this->request->getPost('email'));

        if ($user) {

            $tokenEmail = $this->security->getToken();

            $date = date('Y-m-d H:i:s');

            $user->setRecoverHash($tokenEmail);

            $user->setDateUpdated($date);

            $user->setRecoverExpire(date('Y-m-d H:i:s', strtotime($date . ' +12 hours')));

            $user->save();

            $alert = new AccountNotifications();

            $alert->setUser($user)
                    ->setSubject($this->message('EMAIL_SUBJECT_RESET_PASSWORD'))
                    ->setLayout($this->systemMessage->get('EMAIL_LAYOUT_RESET_PASSWORD'))//email/reset-password
                    ->setToken($tokenEmail)
                    ->setAdditionalParams(['token_expire_after' => '12 hours']);

            $alert->send();

            return [
                'success' => true,
                'message' => $this->message('RESET_ACCOUNT_CONFIRM_SUCCESS_SEND_MAIL'),
                'form' => 'reset'
            ];
        }

        return ['message' => $this->message('RESET_ACCOUNT_EMAIL_NOT_EXISTS')];
    }

    /**
     * 
     */
    public function forgotPasswordAction() {

        return $this->viewToAjax->returnToView();
    }

}
