<?php

namespace PMP\Core\Modules\Auth\Controllers;

use PMP\Core\Plugins\Forms\LoginForm;
use PMP\Core\Plugins\User\UserSecurity;
use PMP\Core\Models\User;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Library\Constants\Redirect;
use PMP\Core\Plugins\DynamicModal;

class LoginController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function authorizeAction() {

        $findUser = $this->checkUser();

        if ($findUser instanceof User) {

            $this->response->setRedirectUrl(Redirect::AFTER_LOGIN);

            return [
                'success' => true,
                'message' => $this->message('LOGIN_SUCCESS')
            ];
        }

        return $findUser;
    }

    /**
     * Display login page
     */
    public function indexAction() {


        $this->viewToAjax->addToParams(['title' => 'Login']);

        return $this->viewToAjax->returnToView();
    }

    /**
     * 
     */
    public function loginAsAction() {

        if (empty($this->request->getPost('id'))) {
            return [
                'message' => $this->message('ACCESS_DENIED'),
            ];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstById($this->request->getPost('id'));

        //read user security settings
        $security = new UserSecurity($user);

        if ($security->getUser()) {

            $security->setSession();

            return [
                'success' => true,
                'message' => $this->message('LOGIN_AS') . ' ' . $user->getFullName(),
                'redirect' => Redirect::AFTER_LOGIN
            ];
        }
    }

    /**
     * Check for an exist user and set session
     * @todo Apply additional options based on user account. 
     */
    private function checkUser() {

        $form = new LoginForm();

        $messages = $form->validate($this->request->getPost());

        if ($messages->count() > 0) {
            return [
                'message' => $this->message($messages->current()->getMessage()),
                'session-token' => $this->security->getSessionToken()
            ];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstByEmail($form->getValue('email'));

        if (empty($user)) {
            return [
                'message' => $this->message('LOGIN_ERROR_CREDENTIALS')
            ];
        }

        $security = new UserSecurity($user);

        if ($security->checkPassword($form->getValue('password')) === false) {

            return ['message' => $this->message('LOGIN_ERROR_PASSWORD')];
        }

        switch ($user->getStatus()) {
            case UserConstants::status_active:

                $security->setSession();

                return $security->getUser();

            case UserConstants::status_register:

                return $this->confirmAccountModal(UserConstants::status_register);

            case UserConstants::status_disabled:
                return [
                    'message' => $this->message(UserConstants::getAccountStatus($user->getStatus()))
                ];

            case UserConstants::status_reset:

                return [
                    'message' => $this->message(UserConstants::getAccountStatus($user->getStatus()))
                ];

            default:

                return [
                    'message' => $this->message(UserConstants::getAccountStatus())
                ];
        }
        
    }

    private function confirmAccountModal($status) {

        $this->viewToAjax->setLayout('user/modals/confirm-account');

        $this->viewToAjax->setVariables([]);

        $dynamicModal = new DynamicModal($this->request->getPost('dynamicModal'));

        $dynamicModal->setModalContent($this->viewToAjax->_getView());

        $this->response->setOpenModal($dynamicModal->getModalParams());

        $this->response->setFormUrl([
            'form' => $this->request->getPost('dynamicFormUrl'),
            'url' => 'auth/account/confirm/'
        ]);

        return true;
    }

}
