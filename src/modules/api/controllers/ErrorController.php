<?php

namespace PMP\Core\Modules\Api\Controllers;

class ErrorController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function initialize() {
        
    }

    /**
     * @param \Exception $exception
     */
    public function indexAction($exception) {

        return [
            'message' => $exception->getMessage()
        ];
    }

}
