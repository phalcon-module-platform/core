<?php

namespace PMP\Core\Modules\Api\Controllers;

use Phalcon\Mvc\Controller;

/**
 * 
 * @property \PMP\Core\Config\SystemConfig $systemConfig
 * @property \PMP\Core\Plugins\FiltersPlugin $filters
 * @property \PMP\Core\Plugins\ConsolePlugin $console
 * @property \PMP\Core\Plugins\ResponsePlugin $response
 */
class ControllerBase extends Controller {
    
  
    
}
