<?php

namespace PMP\Core\Modules\Api\Plugins;

use PMP\Core\Plugins\PluginInterface;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;


class DispatcherPlugin extends PluginInterface {

    /**
     * Triggered before entering in the dispatch loop. 
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher) {
        
        $actionName = lcfirst(\Phalcon\Text::camelize($dispatcher->getActionName(), "-"));

        $controllerName = lcfirst(\Phalcon\Text::camelize($dispatcher->getControllerName(), "-"));

        $moduleName = lcfirst(\Phalcon\Text::camelize($dispatcher->getModuleName(), "-"));

        //if module, controller and action exists in App , then change the namespace
        $ns = $this->systemConfig->isReusedModule($moduleName, $controllerName);

        if (!empty($ns)) {

            $dispatcher->setDefaultNamespace($ns);
        }
        
        //not working on most ENV
        //$dispatcher->setControllerName($controllerName);

        $dispatcher->setModuleName($moduleName);

        $dispatcher->setActionName($actionName);

        $translation = $this->translate;

        $translation->load();
    }

    /**
     * Trigger an action before exception.If controller and action are not found, forward to Index Controller<br />
     * 
     * @param \Phalcon\Events\Event $event
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @param \Phalcon\Mvc\Dispatcher\Exception $exception
     */
    public function beforeException($event, $dispatcher, $exception) {

        $this->dispatcher->forward([
            'namespace' => '\\PMP\\Core\\Modules\\Api\\Controllers',
            'controller' => 'error',
            'action' => 'index',
            'params' => [$exception]
        ]);
        
        return false;
    }

    public function beforeExecuteRoute() {
        
        $this->response->set(true);
    }

    public function afterExecuteRoute() {

        $data = $this->dispatcher->getReturnedValue();

        $this->response->setData(!empty($data) ? $data : []);

        $this->response->sendResponse();
    }

    public function afterDispatchLoop() {
        
    }

}
