<?php

namespace PMP\Core\Modules\Profile\Controllers;

use Phalcon\Mvc\Model\Query\Builder;
use PMP\Core\Models\UserLoginsHistory;

class LoginHistoryController extends ControllerBase {

    /**
     * 
     */
    public function onConstruct() {
        
    }

    /**
     * 
     */
    public function initialize() {
        
    }

    /**
     * 
     */
    public function indexAction() {

        $this->dataTable->setColumns($this->dataTableColumns());

        $options = ['dtOptions' => $this->dataTable->loadDataTableData()];
        
        $this->viewToAjax->setVariables($options);

        return $this->viewToAjax->returnToView();
    }

    /**
     * 
     */
    public function loadAction() {

        $history = $this->getHistory();

        return [
            'draw' => $this->request->getPost('draw'),
            'recordsFiltered' => $this->getHistory(true),
            'recordsTotal' => count($history),
            'data' => $history
        ];
    }

    /**
     * 
     */
    public function clearAction() {

        if (empty($this->request->getPost('selected'))) {

            return[
                'message' => $this->message('VALIDATE_ERROR_EMPTY_SELECTION'),
            ];
        }

        $ids = implode(',', $this->request->getPost('selected'));

        $records = UserLoginsHistory::find([
                    'conditions' => 'id IN(' . $ids . ') AND user_id = :user_id:',
                    'bind' => [
                        'user_id' => $this->user->getId()
                    ]
        ]);

        if (empty($records)) {
            return[
                'message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS'),
            ];
        }

        $records->delete();
        
        $this->response->setRefreshTable();

        return[
            'success' => true,
            'message' => $this->message('FORM_DELETE_RECORD_SUCCESS'),
            'records' => $records->toArray()
        ];
    }

    /**
     * @param bool $count Only count with filters if are applied
     * @return array query records
     */
    private function getHistory($count = false) {

        $dt = $this->dataTable;

        $dt->setColumns($this->dataTableColumns());

        $dt->setPostData($this->request->getPost());

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->addFrom('\PMP\Core\Models\UserLoginsHistory', 'UL');

        $dataSource->columns($dt->getQueryColumns());

        $dataSource->where('UL.user_id = :user_id:', ['user_id' => $this->user->getId()]);

        !empty($dt->getSearchedValue()) ?
                        $dataSource->andWhere($dt->getSearchQueryColumns(), $dt->getSearchedValue()) :
                        '';

        if ($count === true) {

            $sql = $dataSource->getQuery()->execute();

            return $sql->count();
        }

        $dt->getOrderColumn() ?
                        $dataSource->orderBy($dt->getOrderColumn() . ' ' . $dt->getOrderBy()) :
                        $dataSource->orderBy('UL.login_date ' . $dt->getOrderDirection());

        $dataSource->limit($this->request->getPost('length'), $this->request->getPost('start'));

        $sql = $dataSource->getQuery()->execute();

        $records = $sql->toArray();

        array_walk($records, function(&$value) {

            $icon = '<i class="far fa-share-square"></i>';

            $value['ip_address'] = !empty($value['ip_address']) ? $icon . $this->tag->linkTo([
                        'action' => 'https://geoiptool.com/en/?ip=' . $value['ip_address'],
                        'text' => $value['ip_address'],
                        'local' => false,
                        'target' => '_new']
                    ) : NULL;
        });

        return $records;
    }

    /**
     * 
     */
    private function dataTableColumns() {

        $columns = [
            [
                'visible' => 1,
                'data' => 'id',
                'searchable' => false,
                'orderable' => true,
                'width' => '3%',
                'title' => 'id',
                'name' => 'UL.id'
            ],
            [
                'visible' => '1',
                'data' => 'country',
                'searchable' => true,
                'orderable' => true,
                'width' => '7%',
                'title' => 'Country',
                'name' => 'UL.country'
            ],
            [
                'visible' => '1',
                'data' => 'city',
                'width' => '7%',
                'title' => 'City',
                'searchable' => true,
                'orderable' => true,
                'name' => 'UL.city'
            ],
            [
                'visible' => '1',
                'data' => 'ip_address',
                'width' => '5%',
                'title' => 'IP address',
                'searchable' => false,
                'orderable' => true,
                'name' => 'UL.ip_address'
            ],
            [
                'visible' => '1',
                'data' => 'user_agent',
                'searchable' => true,
                'orderable' => true,
                'width' => '20%',
                'title' => 'Browser',
                'name' => 'UL.user_agent'
            ],
            [
                'visible' => '1',
                'data' => 'login_date',
                'searchable' => 1,
                'orderable' => '1',
                'width' => '7%',
                'title' => 'Login date',
                'name' => 'UL.login_date',
                'defaultOrder' => true
            ]
        ];


        return $columns;
    }

}
