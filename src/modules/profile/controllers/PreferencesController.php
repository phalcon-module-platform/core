<?php

namespace PMP\Core\Modules\Profile\Controllers;

class PreferencesController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function initialize() {
        
    }

    public function indexAction() {
        
    }

    /**
     * Manage cookie for small left sidebar
     */
    public function smallBarAction() {

        

        $this->cookies->set('small-bar', $this->request->getPost('small-bar'), 0, '/', true);

        return [
            'data' => $this->request->getPost()
        ];
    }

}
