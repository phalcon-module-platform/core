<?php

namespace PMP\Core\Modules\Profile\Controllers;

use PMP\Core\Models\User;
use PMP\Core\Library\FileSystem;

class PersonalController extends ControllerBase {

    public function indexAction() {

        //get profile from cache
        $profileDetails = ['profileDetails' => $this->profile->details()];

        $this->viewToAjax->setVariables($profileDetails);

        return $this->viewToAjax->returnToView();
    }

    public function updateAction() {

        if (empty($this->request->getPost())) {

            return [
                'message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')
            ];
        }

        //if user change his email
        if ($this->user->getEmail() != $this->request->getPost('email') && User::findFirstByEmail($this->request->getPost('email'))) {

            return ['message' => $this->message('UPDATE_ERROR_RECORD_DUPLICATE')];
        }

        if ($this->user->getEmail() != $this->request->getPost('email')) {

            $this->user->save(['email' => $this->request->getPost('email')]);

            $this->profile->clearUserCache();
        }

        $profileDetails = $this->user->getProfileDetails();

        $profileDetails->assign($this->request->getPost(), null, ['first_name', 'last_name', 'company', 'position', 'website_address', 'about_me']);

        $profileDetails->save();

        $message = $this->message('UPDATE_SUCCESS');

        $this->profile->clearProfileCache();

        return [
            'success' => true,
            'message' => $message
        ];
    }

    public function uploadPhotoAction() {

        $files = $this->request->getUploadedFiles();

        /* @var $file \Phalcon\Http\Request\File */
        $file = $files[0];

        //relative path
        $savePath = FileSystem::makeDir($this->systemConfig->getRealUploadPath() . '/profiles/' . $this->user->getId() . '/');

        //base image path 
        $imagePath = $this->systemConfig->getUploadPath() . '/profiles/' . $this->user->getId() . '/';

        //new image name
        $newName = $this->security->getRandom()->uuid() . '.' . $file->getExtension();

        $image = new \Gumlet\ImageResize($file->getTempName());

        $image->resizeToBestFit(150, 150);

        //save to relative path
        $image->save($savePath . $newName, null, 100);

        $profile = $this->user->getProfileDetails();

        FileSystem::deleteFile($profile->getPhoto());

        $profile->setPhoto($imagePath . $newName);

        $profile->save();

        //must clear profile cache
        $this->profile->clearProfileCache();

        return [
            'success' => true,
            'file' => $file->getTempName(),
            'image' => $profile->getPhoto()
        ];
    }

    public function deletePhotoAction() {

        $profile = $this->user->getProfileDetails();

        FileSystem::deleteFile($profile->getPhoto());

        //profile-image
        $profile->setPhoto(NULL);

        $profile->save();

        //must clear profile cache
        $this->profile->clearProfileCache();

        $funcParams = [
            'element' => $this->request->getPost('dataImageElement'),
            'src' => 'images/default/profile-image.png'
        ];
        
        $this->response->setCallback('replaceImage', $funcParams);

        return [
            'success' => true
        ];
    }

}
