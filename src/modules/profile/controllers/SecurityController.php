<?php

namespace PMP\Core\Modules\Profile\Controllers;

use PMP\Core\Plugins\ViewToAjax;

class SecurityController extends ControllerBase {

    public function initialize() {

        //clear cached profile when update
        if ($this->request->isAjax() && !empty($this->request->getPost())) {

            /* @var $cache \PMP\Core\Plugins\Cache */
            $cache = $this->di->get('cache');

            $profilekey = '-profile' . '-user-' . $this->user->getId();

            $userkey = '-user-' . $this->user->getId();

            $cache->clearCache('profile', $profilekey);

            $cache->clearCache('user', $userkey);
        }
    }

    public function credentialsAction() {

        return $this->viewToAjax->returnToView();
    }

    public function updateSessionNotificationsAction() {



        if (empty($this->request->getPost())) {
            return [
                'success' => true,
                'message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')
            ];
        }

        /* @var $profile \PMP\Core\Models\UserSecurity */
        $profile = $this->user->getRelated('UserSecurity');

        $profile->assign($this->request->getPost(), null, ['session_time', 'login_alert', 'login_unathorized_alert', 'login_history']);

        $profile->setDateUpdated(date('Y-m-d H:i:s'));

        $profile->save();

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function sessionNotificationsAction() {

        $options = ['userSecurity' => $this->user->getRelated('UserSecurity')];

        $this->viewToAjax->setVariables($options);

        return $this->viewToAjax->returnToView();
    }

}
