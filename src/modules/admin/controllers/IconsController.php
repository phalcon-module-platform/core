<?php

namespace PMP\Core\Modules\Admin\Controllers;

class IconsController extends ControllerBase {

    public function faIconsAction() {
        
        
        return $this->viewToAjax->returnToView();
    }

    public function materialDesignIconsAction() {
        
        
        return $this->viewToAjax->returnToView();
    }
}
