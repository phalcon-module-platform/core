<?php

namespace PMP\Core\Modules\Admin\Controllers;

use PMP\Core\Models\SystemLanguages;
use PMP\Core\Models\SystemTranslation;
use Phalcon\Mvc\Model\Query\Builder;
use PMP\Core\Modules\Admin\Plugins\FormAddLanguage;

class LanguageController extends ControllerBase {

    public function initialize() {
        
    }

    public function indexAction() {
        
    }

    public function translationsAction() {

        $langId = (int) $this->dispatcher->getParam(0);

        if (empty($langId) || !is_numeric($langId)) {

            $this->setError($this->message('VALIDATE_ERROR_EMPTY_ID'));

            return false;
        }

        $this->dataTable->setColumns($this->dataTableColumns());

        $options = $this->dataTable->loadDataTableData();

        $this->viewToAjax->setVar('dtOptions', $options);

        $translated = SystemTranslation::count('language_id = ' . $langId);

        $this->viewToAjax->setVar('langId', $langId);

        $this->viewToAjax->setVar('translated', $translated);
        
        return $this->viewToAjax->returnToView();
        
        
    }

    public function getTranslationsAction() {

        $data = $this->getRecords();

        return [
            'draw' => $this->request->getPost('draw'),
            'recordsFiltered' => $this->getRecords(true),
            'recordsTotal' => count($data),
            'data' => $data
        ];
    }

    public function manageAction() {


        $systemLangs = SystemLanguages::find();

        $this->viewToAjax->setVar('systemLanguages', $systemLangs->toArray());
        
        return $this->viewToAjax->returnToView();
    }

    public function updateAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            if (empty($this->request->isPost('id'))) {

                return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
            }

            /* @var $lang \PMP\Core\Models\SystemLanguages */
            $lang = SystemLanguages::findFirstById($this->request->getPost('id'));

            $lang->assign($this->request->getPost(), null, ['name', 'code', 'short_name', 'class', 'flag', 'encoding']);

            $lang->save();




            return [
                'success' => true,
                'message' => $this->message('UPDATE_SUCCESS')
            ];
        }
    }

    public function deleteAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            if (empty($this->request->isPost('id'))) {

                return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
            }

            /* @var $lang \PMP\Core\Models\SystemLanguages */
            $lang = SystemLanguages::findFirstById($this->request->getPost('id'));

            $lang->delete();

            return [
                'success' => true,
                'message' => $this->message('FORM_DELETE_RECORD_SUCCESS'),
                'reload' => true
            ];
        }
    }

    public function addAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            $langForm = new FormAddLanguage();

            $messages = $langForm->validate($this->request->getPost());

            if ($messages->count() > 0) {
                return ['message' => $this->message($messages->current()->getMessage())];
            }

            /* @var $lang \PMP\Core\Models\SystemLanguages */
            $lang = new SystemLanguages();

            $lang->assign($this->request->getPost(), null, ['name', 'code', 'short_name', 'class', 'flag', 'encoding']);

            $lang->save();

            return [
                'success' => true,
                'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS'),
                'reload' => true
            ];
        }
    }

    public function updateConfigAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            $systemLangs = SystemLanguages::find();

            if (empty($systemLangs)) {
                return [
                    'message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')
                ];
            }

            $langsToArray = [];

            foreach ($systemLangs as $value) {
                $langsToArray[$value->getCode()] = $value->toArray();
            }

            $config = new \Phalcon\Config(['languages' => $langsToArray]);

            $this->systemConfig->updateSystemConfig($config);

            return [
                'success' => true,
                'message' => $this->message('UPDATE_SUCCESS')
            ];
        }
    }

    public function updateTranslationAction() {

        if (empty($this->request->getPost('id'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        /* @var $word \PMP\Core\Models\SystemTranslation */
        $word = SystemTranslation::findFirstById($this->request->getPost('id'));


        if (empty($word)) {

            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        $word->assign($this->request->getPost(), null, ['word', 'value']);

        $word->save();

        if (!empty($word->getMessages())) {

            return ['message' => $this->message($word->getFirstMessage())];
        }


        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function addTranslationAction() {

        if (empty($this->request->getPost())) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')];
        }

        $word = new SystemTranslation();

        $word->assign($this->request->getPost(), null, ['word', 'value', 'language_id']);

        $word->save();

        if (!empty($word->getMessages())) {

            return ['message' => $this->message($word->getFirstMessage())];
        }
        
        $this->response->setRefreshTable();

        return [
            'success' => true,
            'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS'),
            'form' => 'reset'
        ];
    }

    public function deleteTranslationAction() {

        if (empty($this->request->getPost())) {

            return ['message' => $this->message('ACCESS_DENIED')];
        }

        $transl = SystemTranslation::findFirstById($this->request->getPost('id'));

        if (empty($transl)) {

            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        $transl->delete();

        return [
            'success' => true,
            'message' => $this->message('FORM_DELETE_RECORD_SUCCESS'),
            'tableRefresh' => true
        ];
    }

    private function dataTableColumns() {

        $columns = [
            [
                'visible' => 0,
                'data' => 'id',
                'searchable' => false,
                'orderable' => true,
                'width' => '5%',
                'title' => 'id',
                'name' => 'S.id',
                'defaultOrder' => true,
                'orderDirection' => 'DESC'
            ],
            [
                'visible' => '0',
                'data' => 'language_id',
                'width' => '60px',
                'title' => 'lang id',
                'searchable' => false,
                'orderable' => true,
                'name' => 'S.language_id'
            ],
            [
                'visible' => '1',
                'data' => 'word',
                'width' => '60px',
                'title' => 'word',
                'searchable' => true,
                'orderable' => true,
                'name' => 'S.word'
            ],
            [
                'visible' => '1',
                'data' => 'value',
                'width' => '60px',
                'title' => 'value',
                'searchable' => true,
                'orderable' => true,
                'name' => 'S.value'
            ],
            [
                'visible' => '1',
                'data' => 'actions',
                'width' => '6%',
                'searchable' => false,
                'orderable' => false,
                'title' => ''
            ]
        ];


        return $columns;
    }

    private function getRecords($count = false) {

        $dt = $this->dataTable;

        $dt->setColumns($this->dataTableColumns());

        $dt->setPostData($this->request->getPost());

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->addFrom('PMP\Core\Models\SystemTranslation', 'S');

        $dataSource->columns($dt->getQueryColumns());

        //6. Where clause for user
        $dataSource->where('S.language_id = :language_id:', ['language_id' => $this->request->getPost('id')]);

        !empty($dt->getSearchedValue()) ?
                        $dataSource->andWhere($dt->getSearchQueryColumns(), $dt->getSearchedValue()) :
                        '';

        if ($count === true) {

            $sql = $dataSource->getQuery()->execute();

            return $sql->count();
        }

        $dt->getOrderColumn() ?
                        $dataSource->orderBy($dt->getOrderColumn() . ' ' . $dt->getOrderBy()) :
                        $dataSource->orderBy('S.id DESC');

        $dataSource->limit($this->request->getPost('length'), $this->request->getPost('start'));

        $sql = $dataSource->getQuery()->execute();

        $rec = $sql->toArray();

        array_walk($rec, function(&$value) {

            $buttons = '';
            $buttons .= $this->elements->button('loading',[
                        'style' => 'danger',
                        'class' => 'width-4 form-button',
                        'data' => [
                            'text' => '<i class="fa fa-trash fa-1x"></i>',
                            'post' => [
                                'action' => 'delete-translation',
                                'id' => $value['id']
                            ],
                            'url' => 'admin/language/delete-translation/'
                        ]
            ]);

            $inputValue = $this->elements->input('material',[
                        'data' => [
                            'url' => 'admin/language/update-translation/', 'id' => $value['id']
                        ],
                        'attr' => [
                            'class' => 'onchange-update',
                            'id' => 'value' . $value['id'],
                            'type' => 'text',
                            'name' => 'value',
                            'value' => $value['value']
                        ]
            ]);

            $inputWord = $this->elements->input('material',[
                        'data' => [
                            'url' => 'admin/language/update-translation/', 'id' => $value['id']
                        ],
                        'attr' => [
                            'class' => 'onchange-update',
                            'id' => 'word' . $value['id'],
                            'type' => 'text',
                            'name' => 'word',
                            'value' => $value['word']
                        ]
            ]);

            $value['value'] = $inputValue;
            $value['word'] = $inputWord;
            $value['actions'] = $buttons;
        });

        return $rec;
    }

}
