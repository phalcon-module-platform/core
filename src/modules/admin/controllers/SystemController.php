<?php

namespace PMP\Core\Modules\Admin\Controllers;

use PMP\Core\Plugins\User\AccountNotifications;
use PMP\Core\Library\FileSystem;
use League\OAuth2\Client\Provider\Google;

class SystemController extends ControllerBase {

    public function indexAction() {
        
    }

    public function clearCacheAction() {

        $params = $this->request->getPost('id');

        if (empty($params)) {
            return true;
        }

        /* @var $cache \PMP\Core\Plugins\Cache */
        $cache = $this->cache;

        $path = $this->systemConfig->getModelsCachelDir();

        foreach (glob($path . "*" . $cache->getCacheFileExt()) as $file) {
            if (preg_match('/' . $params . '/', $file)) {
                FileSystem::deleteFile($file);
            }
        }

        return [
            'success' => true,
            'message' => $this->message('ACTION_DONE'),
            'post' => $this->request->getPost(),
            'params' => $params,
            'path' => $path
        ];
    }

    public function clearAclAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            $file = $this->systemConfig->getAclFile();

            FileSystem::deleteFile($file);

            return [
                'success' => true,
                'message' => $this->message('ACTION_DONE'),
                'file' => $file,
            ];
        }
    }

    public function clearVoltCacheAction() {

        if ($this->request->isAjax() || $this->request->isPost()) {

            $path = $this->systemConfig->getLayoutsCachelDir();

            $dir = FileSystem::deleteDirFiles($path);

            return [
                'success' => true,
                'deleted' => $dir,
                'message' => $this->message('ACTION_DONE'),
                'post' => $this->request->getPost()
            ];
        }
    }

    /**
     * 
     * URL after authorization
     * ?state=b7a7da3597380cedb776d294a0e92988&code=4/qwBRNR-Nitmu_ETlXWCddlmbR2xUFSxJSwwupA5jJGILV-9_rfkybpGzRg7ogiX_Ypfv_ablvipcOkRm6ckjT4U&scope=https://mail.google.com/
     */
    public function gmailApiAction() {

        // it executed after google confirmation screen redirect back to this Action
        if (!empty($this->request->getQuery('code')) && !empty($this->request->getQuery('state'))) {

            if (empty($this->session->get('google-api-auth'))) {

                $this->flashSession->error($this->message('SESSION_HAS_EXPIRED'));

                return $this->viewToAjax->returnToView();
            }

            $authparams = $this->session->get('google-api-auth');

            $provider = new Google($authparams);

            $token = $provider->getAccessToken('authorization_code', [
                'code' => $this->request->getQuery('code')
            ]);

            $credentials = [
                'accessToken' => $token->getToken(),
                'clientId' => $authparams['clientId'],
                'clientSecret' => $authparams['clientSecret'],
                'refreshToken' => $token->getRefreshToken()
            ];

            if (empty($credentials['refreshToken'])) {

                $this->flashSession->error($this->message('GOOGLE_API_REFRESH_TOKEN_ERROR'));

                return $this->viewToAjax->returnToView();
            }


            $this->session->set('google-api-auth', []);

            $this->flashSession->success($this->message('GOOGLE_API_REFRESH_TOKEN_SUCCESS'));

            return $this->viewToAjax
                            ->setVar('googleSession', $credentials)
                            ->returnToView();
        }


        if ($this->request->isAjax() || $this->request->isPost()) {

            $this->session->get('google-api-auth') ?
                            $this->session->set('google-api-auth', []) :
                            '';

            $authparams = [
                'clientId' => $this->request->getPost('clientId'),
                'clientSecret' => $this->request->getPost('clientSecret'),
                'redirectUri' => 'https://' . $this->request->getHttpHost() . '/admin/system/gmail-api/', //$this->request->getHttpHost(),
                'accessType' => 'offline'
            ];

            $provider = new Google($authparams);

            $options = [
                'scope' => [
                    'https://mail.google.com/'
                ]
            ];

            $authUrl = $provider->getAuthorizationUrl($options);

            //save to session for later usage
            $this->session->set('google-api-auth', $authparams);

            return [
                'success' => true,
                'redirect' => $authUrl,
                'message' => $this->message('GOOGLE_API_AUTHORIZE')
            ];
        }
    }

    public function testMailServerAction() {

        $alerts = new AccountNotifications();

        $options = $this->systemConfig->getEmailProviderConfig('Swift');

        $result = $alerts->testMailServer($options);

        if ($result !== true) {

            return [
                'message' => implode(PHP_EOL, $result) . PHP_EOL
            ];
        } else {
            return [
                'success' => true,
                'message' => $this->message('MAIL_SERVER_TEST_SUCCESS')
            ];
        }
    }

}
