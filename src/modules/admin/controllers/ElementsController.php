<?php

namespace PMP\Core\Modules\Admin\Controllers;

use Phalcon\Config;

class ElementsController extends ControllerBase {

    public function faIconsAction() {
        
    }

    public function materialDesignIconsAction() {
        
    }

    public function buttonsAction() {

        /* @var $resourceConfig \Phalcon\Config */
        $resourceConfig = $this->systemConfig->getElementsConfig(true)->path('buttons', new Config([]));

        /* @var $fromcache \Phalcon\Config */
        $fromcache = $this->systemConfig->getElementsConfig()->path('buttons', new Config([]));

//        if ($resourceConfig->path('types')->count() !== $fromcache->path('types')->count()) {
//
//            $merged = $resourceConfig->path('types')->merge($fromcache->path('types'));
//
//            $fromcache->offsetSet('types', $merged);
//        }
//
//        if ($resourceConfig->path('styles')->count() !== $fromcache->path('styles')->count()) {
//
//            $merged = $resourceConfig->path('styles')->merge($fromcache->path('styles'));
//
//            $fromcache->offsetSet('styles', $merged);
//        }


        $resourceConfig->merge($fromcache);

        $this->viewToAjax->setVar('buttons', $resourceConfig);

        return $this->viewToAjax->returnToView();
    }

    public function updateButtonsAction() {


        $resourse = new Config(['elements' => $this->request->getPost('elements')]);

        $this->systemConfig->updateSystemConfig($resourse);



        return true;
    }

}
