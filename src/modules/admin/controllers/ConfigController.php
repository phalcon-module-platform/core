<?php

namespace PMP\Core\Modules\Admin\Controllers;

use Phalcon\Config;

class ConfigController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function indexAction() {
        
    }

    public function updateConfigAction() {

        //load configuration from post and merge with existing one
        $newConfig = new Config($this->request->getPost('config'));

        $this->systemConfig->updateSystemConfig($newConfig);

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    /**
     * 
     * @deprecated
     */
    public function updateAssetsAction() {

        //load configuration from post and merge with existing one
        $newConfig = new Config($this->request->getPost('config'));

        $this->systemConfig->getSystemConfig()->path('assets')->path('files')->offsetSet('css', false);

        $this->systemConfig->getSystemConfig()->path('assets')->path('files')->offsetSet('js', false);

        $this->systemConfig->updateSystemConfig($newConfig);

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function globalAction() {

        $config = $this->systemConfig->getGlobalConfig();


        $this->viewToAjax->setVar('name', 'Global');

        $this->viewToAjax->setVar('conf', $config);

        return $this->viewToAjax->returnToView();
    }

    public function assetsAction() {

        $config = $this->systemConfig->path('assets');


        $this->viewToAjax->setVar('name', 'Assets');

        $this->viewToAjax->setVar('conf', $config);

        return $this->viewToAjax->returnToView();
    }

    public function cacheAction() {

        $config = $this->systemConfig->path('cache');

        $this->viewToAjax->setVar('name', 'Cache');

        $this->viewToAjax->setVar('conf', $config);

        return $this->viewToAjax->returnToView();
    }

    public function emailAction() {

        $this->viewToAjax->setVar('name', 'Email');

        $this->viewToAjax->setVar('emailProviders', $this->systemConfig->getEmailProvidersConfig());

        return $this->viewToAjax->returnToView();
    }

    public function flashAction() {

        $this->viewToAjax->setVar('name', 'Flash messages');

        $this->viewToAjax->setVar('conf', $this->systemConfig->getFlashConfig());

        return $this->viewToAjax->returnToView();
    }

    public function logsAction() {

        $this->viewToAjax->setVar('name', 'Logs');

        $this->viewToAjax->setVar('conf', $this->systemConfig->getLogsConfig());

        return $this->viewToAjax->returnToView();
    }

    public function routesAction() {

        $this->viewToAjax->setVar('name', 'Routes');

        $this->viewToAjax->setVar('conf', $this->systemConfig->getRoutesConfig());

        return $this->viewToAjax->returnToView();
    }

    public function updateRoutesAction() {

        $post = new Config($this->request->getPost());

        if (empty($post->path('name'))) {
            return [
                'success' => false,
                'message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')
            ];
        }

        $path = array_filter(explode('/', str_replace(' ', '', $post->path('path'))));

        $paths = [
            'module' => !empty($path[0]) ? $path[0] : false,
            'controller' => !empty($path[1]) ? $path[1] : false,
            'action' => !empty($path[2]) ? $path[2] : false,
            'params' => !empty($path[3]) ? $path[3] : false
        ];

        $route = new Config([
            'path' => array_filter($paths),
            'name' => str_replace(' ', '', $post->path('name')),
            'via' => array_filter(explode(',', str_replace(' ', '', $post->path('via'))))
        ]);

        if (empty($this->systemConfig->getSystemConfig()->path('routes'))) {

            $this->systemConfig->getSystemConfig()->offsetSet('routes', []);
        }
        $this->systemConfig->getSystemConfig()->path('routes')->offsetSet($post->path('pattern'), $route);

        $this->systemConfig->updateSystemConfig(new Config([]));

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function removeRouteAction() {

        $post = new Config($this->request->getPost());

        $pattern = $post->path('pattern');

        $routes = $this->systemConfig->getSystemConfig()->path('routes')->toArray();

        unset($routes[$pattern]);

        $this->systemConfig->getSystemConfig()->offsetSet('routes', $routes);

        $this->systemConfig->updateSystemConfig(new Config([]));

        return [
            'success' => true,
            'routes' => $routes,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function responseAction() {

        $this->viewToAjax->setVar('name', 'Response');

        $this->viewToAjax->setVar('conf', $this->systemConfig->getResponseConfig());

        return $this->viewToAjax->returnToView();
    }

    public function pathsAction() {

        $this->viewToAjax->setVar('name', 'Paths');

        $this->viewToAjax->setVar('conf', $this->systemConfig->getSystemPathsConfig());

        return $this->viewToAjax->returnToView();
    }

    public function consoleAction() {

        $this->viewToAjax->setVar('name', 'Console');

        return $this->viewToAjax->returnToView();
    }

    public function elementsAction() {

        $this->viewToAjax->setVar('name', 'Elements');

        /* @var $resourceConfig \Phalcon\Config */
        $resourceConfig = $this->systemConfig->getElementsConfig(true);

        /* @var $fromcache \Phalcon\Config */
        $fromcache = $this->systemConfig->getElementsConfig();

        $resourceConfig->merge($fromcache);

        $this->viewToAjax->setVar('conf', $resourceConfig);

        return $this->viewToAjax->returnToView();
    }

}
