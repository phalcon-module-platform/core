<?php

namespace PMP\Core\Modules\Admin\Controllers;

use PMP\Core\Library\FileSystem;
use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config;

class MenuBuilderController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function indexAction() {

        $menuFile = $this->systemConfig->getConfigResourcesDir() . '/menu.json';

        $menu = file_exists($menuFile) ?
                new ConfigJson($menuFile) :
                new Config([]);

        $params = [
            'menu' => json_encode($menu->toArray())
        ];

        $this->viewToAjax->setVariables($params);

        return $this->viewToAjax->returnToView();
    }

    public function saveAction() {

        $decode = json_decode($this->request->getPost('menu'), true);

        $encode = json_encode($decode, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

        FileSystem::writeFile($this->systemConfig->getConfigResourcesDir() . '/menu.json', $encode);

        return [
            'success' => true,
            'menu' => $decode
        ];
    }

}
