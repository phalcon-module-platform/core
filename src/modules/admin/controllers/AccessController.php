<?php

namespace PMP\Core\Modules\Admin\Controllers;

use PMP\Core\Models\SystemRoles;
use PMP\Core\Models\SystemModules;
use PMP\Core\Models\SystemControllers;
use PMP\Core\Models\SystemActions;
use PMP\Core\Models\SystemAccess;
use PMP\Core\Models\User;
use PMP\Core\Modules\Admin\Plugins\FormAddAccess;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Plugins\DynamicModal;

class AccessController extends ControllerBase {

    public function indexAction() {
        
    }

    public function groupsAction() {

        $options = [
            'systemRoles' => SystemRoles::findAccessibleRoles()
        ];

        $this->viewToAjax->setVariables($options);

        return $this->viewToAjax->returnToView();
    }

    /**
     * Display all system roles to view
     */
    public function rolesAction() {

        $roles = SystemRoles::findAccessibleRoles();

        $this->viewToAjax->setVar('systemRoles', $roles->toArray());
    }

    public function roleUpdateAction() {

        $this->response->set();

        if (empty($this->request->getPost('id'))) {
            return [
                'message' => $this->message('VALIDATE_ERROR_EMPTY_ID')
            ];
        }

        /* @var $role \PMP\Core\Models\SystemRoles */
        $role = SystemRoles::findFirstById($this->request->getPost('id'));

        if (empty($role)) {
            return [
                'message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')
            ];
        }

        $role->assign($this->request->getPost(), null, ['name', 'description']);

        $role->save();

        if (!empty($role->getMessages())) {
            return [
                'message' => $role->getFirstMessage()
            ];
        }

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function roleAddAction() {

        $role = new SystemRoles();

        $role->assign($this->request->getPost(), null, ['name', 'description']);

        $role->save();

        if (!empty($role->getMessages())) {
            return [
                'message' => $role->getFirstMessage()
            ];
        }

        return [
            'success' => true,
            'reload' => true,
            'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS')
        ];
    }

    public function roleDeleteAction() {

        /* @var $users \PMP\Core\Models\User */
        $users = User::count('role = ' . $this->request->getPost('id'));

        if ($users > 0) {
            return [
                'message' => $this->message('ROLE_DELETE_ERROR')
            ];
        }

        /* @var $role \PMP\Core\Models\SystemRoles */
        $role = SystemRoles::findFirstById($this->request->getPost('id'));

        if ($role->getProtected() == '1') {
            return [
                'message' => $this->message('ROLE_DELETE_PROTECTED_ERROR')
            ];
        }

        $role->delete();

        if (!empty($role->getMessages())) {
            return [
                'message' => $role->getFirstMessage()
            ];
        }

        return [
            'success' => true,
            'reload' => true,
            'message' => $this->message('FORM_DELETE_RECORD_CHILDS_SUCCESS')
        ];
    }

    public function permisionsAction() {

        $roleId = (int) $this->request->getPost('id');

        if ($roleId == UserConstants::role_admin) {

            return ['message' => $this->message('ACCESS_DENIED')];
        }

        if (empty($roleId) || !is_numeric($roleId)) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        $where = ' SR.id = ' . $roleId;
        $query = 'SELECT 
                     SA.id,
                     SR.id AS roleid,
                     SR.name AS role,
                     SC.name AS controller,
                     SC.title AS controllerTitle,
                     SC.id AS controllerId,
                     A.title AS actionTitle,
                     A.name AS `action`,
                     A.description AS `actiondescription`,
                     SA.access
                     FROM _system_access SA 
                     INNER JOIN _system_roles SR ON SR.id = SA.fkirole
                     INNER JOIN _system_actions A ON A.id = SA.fkiaction
                     INNER JOIN _system_controllers SC on SC.id = SA.fkicontroller
                     WHERE ' . $where . '
                     ORDER BY SC.name';

        $permisions = $this->db->query($query)->fetchAll();

        $systemControllers = SystemControllers::find();

        //get actions if controller is selected
        $actionModels = SystemActions::find();

        $actions = [];

        foreach ($actionModels->toArray() as $value) {
            $actions[$value['fkicontroller']][] = [
                'value' => $value['id'],
                'title' => $value['title']
            ];
        }

        $params = [
            'systemPermisions' => $permisions,
            'roleId' => $roleId,
            'roleName' => !empty($permisions[0]) ? $permisions[0]['role'] : '',
            'systemActions' => json_encode($actions),
            'systemControllers' => $systemControllers->toArray()
        ];

        $this->viewToAjax->setLayout('admin/modals/group-permisions');

        $this->viewToAjax->setVariables($params);

        $dynamicModal = new DynamicModal($this->request->getPost('target'));

        $dynamicModal->setModalContent($this->viewToAjax->_getView());

        $this->response->setOpenModal($dynamicModal->getModalParams());

        return $params;
    }

    public function permisionsUpdateAction() {


        if (empty($this->request->getPost('id'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        /* @var $access \PMP\Core\Models\SystemAccess */
        $access = SystemAccess::findFirstById($this->request->getPost('id'));

        if (empty($access)) {
            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }


        $access->assign($this->request->getPost(), null, ['access']);


        $access->save();


        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    public function permisionsAddAction() {

        if (empty($this->request->getPost('fkirole'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        $accessForm = new FormAddAccess();

        $messages = $accessForm->validate($this->request->getPost());

        if ($messages->count() > 0) {
            return ['message' => $messages->current()->getMessage()];
        }

        /* @var $controller \PMP\Core\Models\SystemControllers */
        $controller = SystemControllers::findFirstById($this->request->getPost('fkicontroller'));

        /* @var $module \PMP\Core\Models\SystemModules */
        $module = SystemModules::findFirstById($controller->getFkimodule());

        $newAccess = new SystemAccess();

        $newAccess->assign($this->request->getPost(), null, ['fkicontroller', 'fkiaction', 'access', 'fkirole']);

        $newAccess->setCreated(date('Y-m-d H:i:s'));

        $newAccess->setFkimodule($module->getId());

        $newAccess->save();

        if (!empty($newAccess->getFirstMessage())) {
            return ['message' => $newAccess->getFirstMessage()];
        }

        return [
            'success' => true,
            'record' => $newAccess->toArray(),
            'reload' => true,
            'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS')
        ];
    }

    public function permisionDeleteAction() {

        if (empty($this->request->getPost('id'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        /* @var $permision \PMP\Core\Models\SystemAccess */
        $permision = SystemAccess::findFirstById($this->request->getPost('id'));

        if (empty($permision)) {
            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        $permision->delete();

        return [
            'success' => true,
            'reload' => true,
            'message' => $this->message('FORM_DELETE_RECORD_SUCCESS')
        ];
    }

}
