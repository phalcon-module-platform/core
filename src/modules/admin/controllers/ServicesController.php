<?php

namespace PMP\Core\Modules\Admin\Controllers;

use Phalcon\Config;

class ServicesController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function indexAction() {
        
    }

    public function sharedServicesAction() {

        //get fresh services from initial config file and merge with saved
        $freshSharedServices = $this->systemConfig->getSharedServicesConfig();

        //already saved/edited
        $savedSharedServices = $this->systemConfig->path('sharedServices');

        $sharedServices = ['sharedServices' => $freshSharedServices->merge($savedSharedServices)];

        $this->viewToAjax->setVariables($sharedServices);

        return $this->viewToAjax->returnToView();
    }

    public function updateServicesAction() {

        if (empty($this->request->getPost('sharedServices'))) {
            return [
                'success' => false,
                'message' => $this->message('REQUEST_RESOURCE_NOT_FOUND')
            ];
        }

        $dataSharedServices = new Config($this->request->getPost());

        //merge  existing components configuration
        $this->systemConfig->updateSystemConfig($dataSharedServices);

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

}
