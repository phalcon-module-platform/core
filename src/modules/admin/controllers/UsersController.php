<?php

namespace PMP\Core\Modules\Admin\Controllers;

use PMP\Core\Models\User;
use PMP\Core\Library\DateTimeCalculations;
use PMP\Core\Plugins\DynamicModal;
use Phalcon\Mvc\Model\Query\Builder;
use PMP\Core\Models\SystemRoles;

class UsersController extends ControllerBase {

    public function onConstruct() {
        
    }

    public function indexAction() {

        $this->dataTable->setColumns($this->dataTableColumns());

        $options = ['dtOptions' => $this->dataTable->loadDataTableData()];

        $this->viewToAjax->setVariables($options);

        return $this->viewToAjax->returnToView();
    }

    public function getUsersAction() {

        return $this->getUsers();
    }

    public function updateInfoAction() {

        if (!$this->request->getPost() || empty($this->request->getPost('user_id'))) {
            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        /* @var $user \PMP\Core\Models\User */
        $user = User::findFirstById($this->request->getPost('user_id'));

        if (!$user) {
            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        $user->assign($this->request->getPost(), null, ['role', 'email', 'full_name']);

        $user->setDateUpdated(date('Y-m-d H:i:s'));

        $user->save();

        if ($user->getMessages()) {
            return ['message' => $this->message($user->getFirstMessage())];
        }

        $this->response->setCloseModal();

        $this->response->setRefreshTable();

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    /**
     * Get user details and show into modal.<br />
     * Dynamic modal is used . Keys needed into post
     * - target : target modal id attribute without hash
     * 
     */
    public function userDetailsAction() {



        if (!$this->request->isAjax() || empty($this->request->getPost('id'))) {

            return ['message' => $this->message('ACCESS_DENIED')];
        }

        $params = [
            'userData' => User::findFirstById($this->request->getPost('id')),
            'systemRoles' => SystemRoles::find()
        ];

        $this->viewToAjax->setLayout('admin/users/user-details');
        
        $this->viewToAjax->setVariables($params);

        $dynamicModal = new DynamicModal($this->request->getPost('target'));

        $dynamicModal->setModalContent($this->viewToAjax->_getView());
        
        $this->response->setOpenModal($dynamicModal->getModalParams());
        
        return $params;
    }

    private function getUsers() {

        $dt = $this->dataTable;

        $dt->setColumns($this->dataTableColumns());

        $dt->setPostData($this->request->getPost());

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->addFrom('\PMP\Core\Models\User', 'U');

        $dataSource->columns($dt->getQueryColumns());

        $dataSource->innerJoin('\PMP\Core\Models\SystemRoles', 'U.role = SR.id', 'SR');

        //6. Where clause for user
        $dataSource->where('U.id != :id:', ['id' => $this->user->getId()]);

        !empty($dt->getSearchedValue()) ?
                        $dataSource->andWhere($dt->getSearchQueryColumns(), $dt->getSearchedValue()) :
                        '';

        $dt->getOrderColumn() ?
                        $dataSource->orderBy($dt->getOrderColumn() . ' ' . $dt->getOrderBy()) :
                        $dataSource->orderBy('U.last_visit DESC');

        $dataSource->limit($this->request->getPost('length'), $this->request->getPost('start'));

        $sql = $dataSource->getQuery()->execute();

        $users = $sql->toArray();
        
        //$this->elements->button();

        array_walk($users, function(&$value) {

            $buttons = '';
            $buttons .= $this->elements->button('loading',[
                        'style' => 'success', 'class' => 'width-4 active-button tooltipped',
                        'data' => [
                            'text' => '<i class="material-icons">perm_identity</i>',
                            'action' => 'login-as',
                            'position' => 'top',
                            'tooltip' => 'login as ' . $value['full_name'],
                            'id' => $value['id'],
                            'url' => 'auth/login/login-as/'
                        ]
            ]);
            $buttons .= $this->elements->button('loading',[
                        'style' => 'info',
                        'class' => 'width-4 ml-1 modal-trigger',
                        'data' => [
                            'text' => '<i class="material-icons">info</i>',
                            'toggle' => 'modal',
                            'target' => '#viewUserDetails',
                            'action' => 'user-details',
                            'id' => $value['id'],
                            'url' => 'admin/users/user-details/'
                        ]
            ]);

            $value['login_as'] = $buttons;

            $value['last_visit'] = DateTimeCalculations::timeAgo(strtotime($value['last_visit']));
        });

        return [
            'draw' => $this->request->getPost('draw'),
            'recordsFiltered' => $this->countUsers(),
            'recordsTotal' => count($users),
            'data' => $users
        ];
    }

    private function countUsers() {

        return User::count('id != ' . $this->user->getId());
    }

    private function dataTableColumns() {

        $columns = [
            [
                'visible' => 1,
                'data' => 'id',
                'searchable' => false,
                'orderable' => true,
                'width' => '5%',
                'title' => 'id',
                'name' => 'U.id',
                'defaultOrder' => true,
                'orderDirection' => 'DESC'
            ], [
                'visible' => 0,
                'data' => 'role_id',
                'name' => 'SR.id'
            ],
            [
                'visible' => '1',
                'data' => 'full_name',
                'searchable' => true,
                'orderable' => true,
                'width' => '10%',
                'title' => 'Full Name',
                'name' => 'U.full_name'
            ],
            [
                'visible' => '1',
                'data' => 'email',
                'searchable' => true,
                'orderable' => true,
                'width' => '10%',
                'title' => 'Email',
                'name' => 'U.email'
            ],
            [
                'visible' => '1',
                'data' => 'role',
                'width' => '10%',
                'title' => 'Role',
                'searchable' => true,
                'orderable' => true,
                'name' => 'SR.description'
            ],
            [
                'visible' => '1',
                'data' => 'last_visit',
                'width' => '10%',
                'title' => 'Last Visit',
                'searchable' => false,
                'orderable' => true,
                'name' => 'U.last_visit'
            ],
            [
                'visible' => '1',
                'data' => 'where_did',
                'searchable' => 1,
                'orderable' => '1',
                'width' => '12%',
                'title' => 'Location',
                'name' => 'U.where_did'
            ],
            [
                'visible' => '1',
                'data' => 'login_as',
                'searchable' => false,
                'orderable' => false,
                'width' => '10%',
                'title' => 'Actions'
            ]
        ];


        return $columns;
    }

}
