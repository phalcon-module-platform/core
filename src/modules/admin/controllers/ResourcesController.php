<?php

namespace PMP\Core\Modules\Admin\Controllers;

use Phalcon\Config;
use PMP\Core\Models\SystemModules;
use PMP\Core\Models\SystemControllers;
use PMP\Core\Models\SystemActions;
use PMP\Core\Plugins\DynamicModal;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Text;

class ResourcesController extends ControllerBase {

    /**
     * 
     * 
     */
    public function onConstruct() {
        
    }

    /**
     * 
     */
    public function indexAction() {

        $this->dataTable->setColumns($this->dataTableColumns());

        $this->dataTable->setPageLength(10);

        $systemModules = SystemModules::find();

        $options = [
            'dtOptions' => $this->dataTable->loadDataTableData(),
            'systemModules' => $systemModules
        ];

        $this->viewToAjax->setVariables($options);

        return $this->viewToAjax->returnToView();
    }

    /**
     * 
     */
    public function loadAction() {

        $controllers = $this->getControllers();

        return [
            'draw' => $this->request->getPost('draw'),
            'recordsFiltered' => $this->getControllers(true),
            'recordsTotal' => count($controllers),
            'data' => $controllers
        ];
    }

    /**
     */
    public function viewActionsAction() {

        /* @var $systemActions \PMP\Core\Models\SystemActions */
        $systemActions = SystemActions::findByFkicontroller($this->request->getPost('id'));

        $controller = SystemControllers::findFirstById($this->request->getPost('id'));

        $module = $controller->getRelated('SystemModules');

        $methods = $this->getControllerMethods($module->getName(), $controller->getName());

        $params = [
            'actions' => $systemActions,
            'controller' => $controller,
            'methods' => $methods
        ];

        $this->viewToAjax->setLayout('admin/modals/view-actions');

        $this->viewToAjax->setVariables($params);

        $dynamicModal = new DynamicModal($this->request->getPost('target'));

        $dynamicModal->setModalContent($this->viewToAjax->_getView());

        $this->response->setOpenModal($dynamicModal->getModalParams());

        return $params;
    }

    /**
     * Create resource : access action in Controller or access to all actions.
     * If access to all actions must allowed , the ones already added must be removed
     * If access to specific action / actions must be added , then full access to controller
     * must be removed if was added
     * 
     */
    public function createActionAction() {

        if (empty($this->request->getPost('name'))) {
            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_PARAMETER')];
        }

        /* @var $controllerModel \PMP\Core\Models\SystemControllers */
        $controllerModel = SystemControllers::findFirstById($this->request->getPost('fkicontroller'));

        if (empty($controllerModel)) {
            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        //checks ControllerClass
        $controllerClassExists = $this->controllerActionExists($controllerModel->getFkimodule(), $controllerModel->getName());

        if (empty($controllerClassExists)) {
            return ['message' => $this->message('RESOURCES_CONTROLLER_NOT_EXISTS')];
        }

        //Checks Action in Controller Class
        $actionExists = $this->controllerActionExists($controllerModel->getFkimodule(), $controllerModel->getName(), $this->request->getPost('name'));

        if (empty($actionExists)) {
            return ['message' => $this->message('RESOURCES_CONTROLLER_ACTION_NOT_EXISTS')];
        }

        //if full access to controller, eg. action is *, checks if there is already added actions and do not allow ,that will broke the access list

        if ($this->request->getPost('name') == '*' && $controllerModel->getRelated('SystemActions')->count() > 0) {
            return [
                'message' => 'To allow full access to all actions, first remove the ones you added'
            ];
        }

        $action = new SystemActions();

        $action->assign($this->request->getPost(), null, ['name', 'title', 'description', 'fkicontroller']);

        $action->save();

        if (!empty($action->getMessages())) {
            return ['message' => $action->getFirstMessage()];
        }

        $this->response->setCloseModal();

        return [
            'success' => true,
            'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS')
        ];
    }

    /**
     * 
     */
    public function updateActionAction() {

        /* @var $action \PMP\Core\Models\SystemActions */
        $action = SystemActions::findFirstById($this->request->getPost('id'));

        if (empty($action)) {
            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        /* @var $controllerModel \PMP\Core\Models\SystemControllers */
        $controllerModel = $action->getRelated('SystemControllers');

        $actionExists = $this->controllerActionExists($controllerModel->getFkimodule(), $controllerModel->getName(), $this->request->getPost('name'));

        if (empty($actionExists)) {
            return ['message' => $this->message('RESOURCES_CONTROLLER_ACTION_NOT_EXISTS')];
        }

        $action->assign($this->request->getPost(), null, ['name', 'title', 'description', 'inmenu']);

        $action->save();

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    /**
     * 
     */
    public function deleteActionAction() {

        if (empty($this->request->getPost('id'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        /* @var $action \PMP\Core\Models\SystemActions */
        $action = SystemActions::findFirstById($this->request->getPost('id'));

        if (empty($action)) {
            return ['message' => $this->message('UPDATE_ERROR_RECORD_NOT_EXISTS')];
        }

        $action->delete();

        $this->response->setCloseModal();

        return [
            'success' => true,
            'message' => $this->message('FORM_DELETE_RECORD_SUCCESS')
        ];
    }

    /**
     * 
     */
    public function viewResourceAction() {

        $systemModules = SystemModules::find();

        $resource = SystemControllers::findFirstById($this->request->getPost('id'));

        $params = [
            'systemModules' => $systemModules,
            'resource' => $resource
        ];

        $this->viewToAjax->setLayout('admin/modals/view-resource');

        $this->viewToAjax->setVariables($params);

        $dynamicModal = new DynamicModal($this->request->getPost('target'));

        $dynamicModal->setModalContent($this->viewToAjax->_getView());

        $this->response->setOpenModal($dynamicModal->getModalParams());

        return $params;
    }

    /**
     * 
     */
    public function deleteResourceAction() {

        $records = SystemControllers::find([
                    'conditions' => 'id IN(' . implode(',', $this->request->getPost('selected')) . ')'
        ]);

        if (!empty($records)) {

            $records->delete();
        }

        $this->response->setRefreshTable();

        return [
            'message' => $this->message('FORM_DELETE_RECORD_CHILDS_SUCCESS'),
            'success' => true,
            'records' => $records->toArray()
        ];
    }

    /**
     */
    public function updateResourceAction() {

        if (empty($this->request->getPost('id'))) {

            return ['message' => $this->message('VALIDATE_ERROR_EMPTY_ID')];
        }

        $class = $this->controllerActionExists(
                $this->request->getPost('fkimodule'), $this->request->getPost('name')
        );

        //if controller not exists
        if (empty($class)) {
            return ['message' => $this->message('RESOURCES_CONTROLLER_NOT_EXISTS')];
        }

        /* @var $controller \PMP\Core\Models\SystemControllers */
        $controller = SystemControllers::findFirstById($this->request->getPost('id'));

        $controller->assign($this->request->getPost(), null, ['name', 'title', 'description', 'inmenu', 'fkimodule']);

        $controller->save();

        if (!empty($controller->getMessages())) {
            return [
                'message' => $this->message($controller->getFirstMessage())
            ];
        }

        $this->response->setRefreshTable();

        $this->response->setCloseModal();

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS')
        ];
    }

    /**
     * 
     */
    public function createResourceAction() {

        if (!$this->request->isAjax()) {

            $this->setError($this->message('ACCESS_DENIED'));

            return false;
        }

        $class = $this->controllerActionExists(
                $this->request->getPost('fkimodule'), $this->request->getPost('name')
        );

        //if controller not exists
        if (empty($class)) {
            return ['message' => $this->message('RESOURCES_CONTROLLER_NOT_EXISTS')];
        }

        $controller = new SystemControllers();

        $controller->assign($this->request->getPost(), null, ['name', 'title', 'description', 'inmenu']);

        $controller->setFkimodule($this->request->getPost('fkimodule'));

        $controller->save();

        if (!empty($controller->getMessages())) {

            return ['message' => $this->message($controller->getFirstMessage())];
        }

        $this->response->setRefreshTable();

        return [
            'success' => true,
            'message' => $this->message('FORM_ADD_NEW_RECORD_SUCCESS')
        ];
    }

    /**
     * Scan module controllers directory and retrieve all the controller names 
     */
    public function getModuleControllersAction() {

        /* @var $module \PMP\Core\Models\SystemModules */
        $module = SystemModules::findFirstById($this->request->getPost('fkimodule'));

        if (empty($module)) {
            return false;
        }

        //scan core module controllers
        $appDir = $this->systemConfig->getAppModulesDir() . '' . $module->getName();

        $appControllers = glob($appDir . '/controllers/*Controller.php');

        $coreDir = $this->systemConfig->getCoreModulesDir() . '' . $module->getName();

        $coreControllers = glob($coreDir . '/controllers/*Controller.php');

        $controllers = array_merge($appControllers, $coreControllers);

        array_walk($controllers, function(&$value) {

            $filename = lcfirst(pathinfo($value, PATHINFO_FILENAME));

            $controller = str_replace('Controller', '', $filename);

            $value = [
                'value' => $controller,
                'title' => $controller
            ];
        });

        array_unshift($controllers, ['value' => '*', 'title' => 'Full Access']);

        return [
            'source' => $controllers
        ];
    }

    /**
     * @param int $module_id Module ID
     * @param string $controller_name Controller Name
     * @param string $action_name Action Name
     * 
     * @return bool FALSE if module or controller not exists, TRUE if Resource controller namespace exists
     */
    private function controllerActionExists($module_id, $controller_name, $action_name = false) {

        $uncamelized = Text::uncamelize($controller_name, '-');

        /* @var $module \PMP\Core\Models\SystemModules */
        $module = SystemModules::findFirstById($module_id);

        if (empty($module)) {
            return false;
        }

        //when resource is full access, set class to ControllerBase
        $contr = $controller_name == '*' ?
                'ControllerBase' :
                Text::camelize($uncamelized) . 'Controller';

        $uncamelizedM = Text::uncamelize($module->getName(), '-');

        $moduleNs = ucfirst(Text::camelize($uncamelizedM)) . '\\Controllers\\' . $contr;

        //checks the destinations
        $class = [
            'core' => class_exists('\\PMP\\Core\\Modules\\' . $moduleNs),
            'app' => class_exists('\\App\\Modules\\' . $moduleNs)
        ];

        //return false if Class does not exists
        if (empty(array_filter($class))) {
            return false;
        }

        //if action is not set, return true
        if (empty($action_name)) {
            return true;
        }

        //when resource is full access to module, return true
        if ($action_name === '*') {
            return true;
        }

        $method = $action_name . 'Action';

        //continue checking method
        $methods = [
            'core' => method_exists('\\PMP\\Core\\Modules\\' . $moduleNs, $method),
            'app' => method_exists('\\App\\Modules\\' . $moduleNs, $method)
        ];

        return !empty(array_filter($methods)) ? true : false;
    }

    /**
     * @param string $module_name
     * @param string $controller_name
     * 
     * @return \Phalcon\Config
     */
    private function getControllerMethods($module_name, $controller_name) {

        $uncamelized = Text::uncamelize($controller_name, '-');

        $uncamelizedM = Text::uncamelize($module_name, '-');

        //when resource is full access, set class to ControllerBase
        $contr = $controller_name == '*' ?
                'ControllerBase' :
                ucfirst(Text::camelize($uncamelized)) . 'Controller';

        $moduleNs = ucfirst(Text::camelize($uncamelizedM)) . '\\Controllers\\' . $contr;


        //checks the destinations
        $class = [
            'core' => '\\PMP\\Core\\Modules\\' . $moduleNs,
            'app' => '\\App\\Modules\\' . $moduleNs
        ];

        $path = class_exists($class['app']) ? $class['app'] : $class['core'];

        $methods = get_class_methods($path);

        $actions = [];

        $actions[] = [
            'value' => '*',
            'title' => 'Full Access'
        ];

        if (empty($methods)) {
            new Config($actions);
        }

        foreach ($methods as $value) {

            $matches = false;

            preg_match('/Action/', $value, $matches);

            if (empty($matches)) {
                continue;
            }

            $action = str_replace('Action', '', $value);

            $actions[] = [
                'value' => $action,
                'title' => $action
            ];
        }

        array_unshift($actions, ['value' => '*', 'title' => 'Full Access']);

        return new Config($actions);
    }

    /**
     * @param bool $count Only count with filters if are applied
     * @return array query records
     */
    private function getControllers($count = false) {

        $dt = $this->dataTable;

        $dt->setColumns($this->dataTableColumns());

        $dt->setPostData($this->request->getPost());

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->addFrom(SystemControllers::class, 'SC');

        $dataSource->innerJoin(SystemModules::class, 'M.id = SC.fkimodule', 'M');

        $dataSource->columns($dt->getQueryColumns());

        !empty($dt->getSearchedValue()) ?
                        $dataSource->where($dt->getSearchQueryColumns(), $dt->getSearchedValue()) :
                        '';

        if ($count === true) {

            $sql = $dataSource->getQuery()->execute();

            return $sql->count();
        }

        $dt->getOrderColumn() ?
                        $dataSource->orderBy($dt->getOrderColumn() . ' ' . $dt->getOrderBy()) :
                        $dataSource->orderBy('M.name ' . $dt->getOrderDirection());

        $dataSource->limit($this->request->getPost('length'), $this->request->getPost('start'));

        $sql = $dataSource->getQuery()->execute();

        $records = $sql->toArray();

        array_walk($records, function(&$value) {

            $buttons = '';
            $buttons .= $this->elements->button('loading', [
                'style' => 'info',
                'class' => 'width-8 ml-1 modal-trigger btn-sm',
                'data' => [
                    'text' => 'actions',
                    'toggle' => 'modal',
                    'target' => '#viewActions',
                    'action' => 'view-actions',
                    'id' => $value['id'],
                    'url' => 'admin/resources/view-actions/'
                ]
            ]);
            $buttons .= $this->elements->button(null, [
                'style' => 'success',
                'class' => 'width-4 btn-sm ml-1 modal-trigger ',
                'data' => [
                    'text' => '<i class="material-icons">edit</i>',
                    'toggle' => 'modal',
                    'target' => '#viewResource',
                    'action' => 'view-resource',
                    'id' => $value['id'],
                    'url' => 'admin/resources/view-resource/'
                ]
            ]);
            $value['actions'] = $buttons;
        });

        return $records;
    }

    /**
     */
    private function dataTableColumns() {

        $columns = [
            [
                'visible' => 1,
                'data' => 'id',
                'searchable' => false,
                'orderable' => true,
                'width' => '5%',
                'title' => 'id',
                'name' => 'SC.id'
            ],
            [
                'visible' => '0',
                'data' => 'fkimodule',
                'searchable' => false,
                'orderable' => false,
                'width' => '0%',
                'title' => 'Module id',
                'name' => 'SC.fkimodule'
            ],
            [
                'visible' => '1',
                'data' => 'module_name',
                'searchable' => 1,
                'orderable' => '1',
                'width' => '6%',
                'title' => 'Modul',
                'name' => 'M.name',
                'defaultOrder' => true,
                'orderDirection' => 'ASC'
            ],
            [
                'visible' => '1',
                'data' => 'title',
                'width' => '10%',
                'title' => 'title',
                'searchable' => false,
                'orderable' => true,
                'name' => 'SC.title'
            ],
            [
                'visible' => '1',
                'data' => 'description',
                'width' => '10%',
                'title' => 'Description',
                'searchable' => true,
                'orderable' => true,
                'name' => 'SC.description'
            ],
            [
                'visible' => '1',
                'data' => 'controller',
                'searchable' => true,
                'orderable' => true,
                'width' => '6%',
                'title' => 'Controller',
                'name' => 'SC.name'
            ],
            [
                'visible' => '1',
                'data' => 'actions',
                'width' => '12%',
                'searchable' => false,
                'orderable' => false,
                'title' => 'actions'
            ]
        ];

        return $columns;
    }

}
