<?php

namespace PMP\Core\Modules\Admin\Controllers;

use Phalcon\Config;
use PMP\Core\Library\FileSystem;

class ComponentsController extends ControllerBase {

    public function onConstruct() {
        
    }

    
    public function indexAction() {

        //scan components folder
        $components = glob($this->systemConfig->getAppFullDir() . '/components/*', GLOB_ONLYDIR);

        //get component name from path
        array_walk($components, function(&$path) {

            $name = basename($path);

            $config = !empty($this->systemConfig->path('components.' . $name)) ?
                    $this->systemConfig->path('components.' . $name) :
                    FileSystem::readJsonFile($this->systemConfig->getConfigResourcesDir() . '/' . $name . '.json');

            $path = [
                'name' => $name,
                'config' => $config->toArray()
            ];
        });

        $params = [
            'components' => new Config($components)
        ];

        $this->viewToAjax->setVariables($params);

        return $this->viewToAjax->returnToView();
    }

    public function updateAction() {

        //get components from system configuration
        $componentsConfig = $this->systemConfig->getComponentsConfig();

        //get new component settings from post
        $newConfig = new Config($this->request->getPost('components'));

        //merge with existing ones
        $componentsConfig->merge($newConfig);

        $components = new Config(['components' => $componentsConfig]);

        $this->systemConfig->updateSystemConfig($components);

        return [
            'success' => true,
            'message' => $this->message('UPDATE_SUCCESS'),
            'newConfig' => $this->systemConfig->getSystemConfig()
        ];
    }

}
