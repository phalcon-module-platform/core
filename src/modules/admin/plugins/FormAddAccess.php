<?php

namespace PMP\Core\Modules\Admin\Plugins;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use PMP\Core\Plugins\Translate;

class FormAddAccess extends Validation {

   public function initialize() {

      $this->add('fkicontroller', new PresenceOf([
          'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
      ]));

      $this->add('fkiaction', new PresenceOf([
          'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
      ]));

   }

}
