<?php

namespace PMP\Core\Modules\Admin\Plugins;

use Phalcon\Validation;



class MultiarrayValidation extends Validation {


    /**
     * Gets the a value to validate in the array/object data source
     *
     * @param string $field
     * @return mixed
     */
    public function getValue($field) {

        if (strstr($field, "[")) {
            $name_array = explode("[", str_replace("]", "", $field));            
            $value = $this->request->get($name_array[0]);
            unset($name_array[0]);
            foreach($name_array as $name) {
                if (isset($value[$name])) {
                    $value = $value[$name];
                }
            }
        } else {
            $value = $this->request->get($field);
        }

        return $value;

    }


}