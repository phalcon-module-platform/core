<?php

namespace PMP\Core\Modules\Admin\Plugins;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class FormAddLanguage extends Validation {

   public function initialize() {

      $this->add('name', new PresenceOf([
          'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
      ]));

      $this->add('code', new PresenceOf([
          'message' => 'VALIDATE_ERROR_EMPTY_PARAMETER'
      ]));

      $this->setFilters('name', 'trim');
      $this->setFilters('code', 'trim');
      $this->setFilters('short_name', 'trim');
      $this->setFilters('class', 'trim');
      $this->setFilters('flag', 'trim');
      $this->setFilters('encoding', 'trim');

   }

}
