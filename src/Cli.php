<?php

namespace PMP\Core;

use Phalcon\Cli\Console AS PhalconConsole;
use Phalcon\Cli\Dispatcher;
use Phalcon\Loader;
use Phalcon\DiInterface;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Events\Manager;
use PMP\Core\Cli\Plugins\DispatcherPlugin;

/**
 * class Cli
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 * 
 * @property \PMP\Core\Config\SystemConfig $systemConfig
 * @property \Phalcon\Cli\Console $console
 */
class Cli extends PhalconConsole implements ModuleDefinitionInterface {

    /**
     * @var \Phalcon\Config
     */
    protected $modulConfig;

    /**
     * Registers the module auto-loader
     *
     * @param DiInterface $di
     */
    public function registerAutoloaders(DiInterface $di = null) {

        $this->modulConfig = $this->systemConfig->consoleConfig();

        $dirs = [];

        foreach ($this->modulConfig->path('namespaces', []) as $namespace) {

            $dir = [$this->modulConfig->path('rootPath'), strtolower($namespace)];

            $path = chop(implode('/', $dir), '/') . '/';

            if (!file_exists($path)) {
                continue;
            }

            $nsd = [$this->modulConfig->path('namespacesRoot'), $namespace];

            $dirs[implode('\\', $nsd)] = $path;
        }

        //register additional directories / services
        foreach ($this->modulConfig->path('registerNamespaces', []) as $ns) {

            //
            $paths = explode('\\', strtolower($ns));

            $dir = $this->systemConfig->getAppFullDir() . '/' . implode('/', $paths) . '/';

            if (!file_exists($dir) && !is_dir($dir)) {
                continue;
            }

            $use = 'App\\' . $ns;

            $dirs[$use] = $dir;
        }

        $loader = new Loader();

        $loader->registerNamespaces($dirs);

        $loader->registerClasses($dirs);

        $loader->registerDirs(array_values($dirs));

        $loader->register();

        return true;
    }

    /**
     * Registers services related to the module
     *
     * @param DiInterface $di
     */
    public function registerServices(DiInterface $di) {

        $moduleConfig = $this->modulConfig;

        $services = [
            \PMP\Core\Services\Db::class,
            \PMP\Core\Services\Cache::class,
            \PMP\Core\Services\Registry::class
        ];

        foreach ($services as $service) {

            $this->di->register(new $service());
        }

        //share specific module services
        foreach ($moduleConfig->path('sharedServices', []) as $specificService) {
            $this->di->register(new $specificService());
        }

        $di->setShared('dispatcher', function () use($moduleConfig) {

            //Create/Get an EventManager
            $eventsManager = new Manager();

            $eventsManager->attach("dispatch", new DispatcherPlugin());

            $dispatcher = new Dispatcher();

            $dispatcher->setDefaultNamespace($moduleConfig->path('metadata.controllersNamespace'));

            //Bind the EventsManager to the dispatcher
            $dispatcher->setEventsManager($eventsManager);

            return $dispatcher;
        });
    }

}
