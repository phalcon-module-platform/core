<?php

namespace PMP\Core\Plugins;

/**
 * class DynamicBlock 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class DynamicBlock {

    /**
     * @var string block ID
     */
    private $dynamicBlock = 'block';

    /**
     * @var string block HTML content
     */
    private $content = 'html';

    /**
     * @var array
     */
    private $params = [];

    /**
     * @param string $dynamicBlock HTML block ID
     */
    public function __construct($dynamicBlock = false) {

        $this->params[$this->dynamicBlock] = $dynamicBlock;

        $this->params[$this->content] = false;
    }

    /**
     * @param string $blockId
     */
    public function setBlockId($blockId = false) {

        $this->params[$this->dynamicBlock] = $blockId;
    }

    /**
     * @param string $blockContent
     */
    public function setBlockContent($blockContent = false) {

        $this->params[$this->content] = $blockContent;
    }

    /**
     * @return mixed content or false
     */
    public function getBlockContent() {

        return !empty($this->params[$this->content]) ? $this->params[$this->content] : false;
    }

    /**
     * @return mixed content or false
     */
    public function getBlockParams() {

        return $this->params;
    }

}
