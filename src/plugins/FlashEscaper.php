<?php

namespace PMP\Core\Plugins;


class FlashEscaper extends \Phalcon\Escaper {

    public function escapeHtml($text) {
        
        $prefix = '<button type="button" class="close" data-dismiss="alert">&times;</button>';

        return $prefix . parent::escapeHtml($text);
    }

}
