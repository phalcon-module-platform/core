<?php

namespace PMP\Core\Plugins;

use Phalcon\Mvc\User\Plugin;

/**
 * @property \Phalcon\Config $config
 * @property \Phalcon\Registry $registry
 * @property \Phalcon\Mvc\Dispatcher $dispatcher
 * @property \Phalcon\Tag $tag
 * @property \PMP\Core\Config\SystemConfig $systemConfig
 * @property \PMP\Core\Config\RouterConfig $router
 * @property \PMP\Core\Plugins\Profile\ProfileDetails $profile
 * @property \PMP\Core\Plugins\AccessList $acl
 * @property \PMP\Core\Plugins\Assets $assets
 * @property \PMP\Core\Plugins\Translate $translate
 * @property \PMP\Core\Plugins\Cache $cache
 * @property \PMP\Core\Plugins\ResponsePlugin $response
 * @property \PMP\Core\Plugins\ViewToAjax $viewToAjax
 * @property \PMP\Core\Plugins\FiltersPlugin $filters
 * @property \PMP\Core\Plugins\ConsolePlugin $console
 * @property \PMP\Core\Plugins\Session $session
 * @property \PMP\Core\Plugins\Profile\SessionProfile $sessionProfile
 * @property \PMP\Core\Plugins\SystemMessages $systemMessage
 * @property \PMP\Core\Plugins\IpData $ipdata
 * @property \PMP\Core\Plugins\DataTable $dataTable
 * @property \PMP\Core\Plugins\Elements\ElementsPlugin $elements
 * @property \PMP\Core\Models\User $user
 * @property \PMP\Core\Providers\Mail\MailManagerInterface $mailManager
 * 
 * configure your properties below 
 * 
 */
class PluginInterface extends Plugin {
    
    

}
