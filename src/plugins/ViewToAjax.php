<?php

namespace PMP\Core\Plugins;

use PMP\Core\Plugins\PluginInterface;

/**
 * Pickup partial template and return the contents
 * 
 */
class ViewToAjax extends PluginInterface {

    /**
     * @var string Path to the partial layout
     */
    private $layout;

    /**
     * @var array Parameters which the layout expect
     */
    private $variables = [];

    /**
     * @var array Parameters to return in view
     */
    private $params = [];

    /**
     * @param string $layout Path to the partial layout
     * 
     * @return \PMP\Core\Plugins\ViewToAjax
     */
    public function setLayout($layout) {

        $this->layout = chop($layout, '/');

        return $this;
    }

    /**
     * @return string Layout name used
     */
    public function getLayout() {
        return $this->layout;
    }

    /**
     * @return array Variables used
     */
    public function getVariables() {
        return $this->variables;
    }

    /**
     * @param array $variables Parameters which the layout expect
     * 
     * @return \PMP\Core\Plugins\ViewToAjax
     */
    public function setVariables($variables = []) {

        $this->variables = $variables;

        return $this;
    }

    /**
     * Set a single parameter
     *
     * @param string $key
     * @param mixed $value
     * 
     * @return \PMP\Core\Plugins\ViewToAjax
     */
    public function setVar($key, $value) {

        $this->variables[$key] = $value;

        return $this;
    }

    /**
     * Load volt view and return HTML contents
     */
    public function _getView() {

        ob_start();

        $this->view->partial($this->getLayout(), $this->getVariables());

        $contents = ob_get_clean();

        return $contents;
    }

    /**
     * Add extra parameters to Ajax response
     * 
     * @param array $param
     * 
     * @return \PMP\Core\Plugins\ViewToAjax 
     */
    public function addToParams($param = []) {

        if (empty($param) || !is_array($param)) {

            return $this;
        }

        $this->params = array_merge($this->params, $param);

        return $this;
    }

    /**
     * @return array
     */
    public function getParams() {

        $page = [
            'title' => $this->tag->getTitle(false),
            'docType' => $this->tag->getDocType()
        ];

        $this->params['page'] = $page;

        return $this->params;
    }

    /**
     * Return response to AJAX or Render partial view layout based
     * 
     * @param array $extra_data Extra data to append to response
     * 
     * @return array    response to AJAX
     * @return bool     TRUE when render view
     */
    public function returnToView($extra_data = []) {

        !empty($extra_data) ?
                        $this->addToParams($extra_data) :
                        false;

        if (!$this->request->isPost()) {

            $this->view->partial($this->getLayout(), $this->getVariables());

            return true;
        }

        $this->response->setCallback('dynamicPage', $this->_getView());

        if ($this->request->getPost('callback')) {
            $this->response->setCallback($this->request->getPost('callback'), $this->request->getPost('callbackParams'));
        }


        return $this->getParams();
    }

}
