<?php

namespace PMP\Core\Plugins\Profile;

use Phalcon\Config;

/**
 * class SessionProfile Mapping of Profile session
 * 
 * 
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class SessionProfile {

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $login_id;

    /**
     * @var string
     */
    private $last_visit;

    /**
     * @var string
     */
    private $fullname;

    /**
     * @var string
     */
    private $email;

    /**
     * @var \Phalcon\Config
     */
    private $role;

    /**
     * @var \Phalcon\Config
     */
    private $look_and_feel;

    /**
     * @var int
     */
    private $session_time;

    /**
     * @var string Session token key
     */
    private $token;

    /**
     * @param \Phalcon\Config $profile_session
     */
    public function __construct(\Phalcon\Config $profile_session = null) {

        $params = !empty($profile_session) ? $profile_session : new Config();

        foreach ($params as $property => $prop_value) {

            if (!property_exists(get_class(), $property)) {

                continue;
            }

            $this->{$property} = $prop_value;
        }
    }

    /**
     * @return int user ID
     */
    public function getUserId() {

        return $this->id;
    }

    /**
     * @return int user login ID
     */
    public function getLoginId() {

        return $this->login_id;
    }

    /**
     * @param string $datetime
     */
    public function setLastVisit($datetime) {

        $this->last_visit = $datetime;
    }

    /**
     * @return string user last visit date time
     */
    public function getLastVisit() {

        return !empty($this->last_visit) ? $this->last_visit : date('Y-m-d H:i:s');
    }

    /**
     * @return string user full name
     */
    public function getFullName() {

        return $this->fullname;
    }

    /**
     * @return string user email address
     */
    public function getEmail() {

        return $this->email;
    }

    /**
     * @return int session time in seconds
     */
    public function getSessionTime() {

        return $this->session_time;
    }

    /**
     * @return string session token key generated after login
     */
    public function getToken() {

        return $this->token;
    }

    /**
     * @return string date-time when session will expire
     */
    public function expiredAt() {

        //calculate the time when session need to expire
        return \date('Y-m-d H:i:s', \strtotime($this->getLastVisit() . ' +' . $this->getSessionTime() . ' seconds'));
    }

    /**
     * @return int role ID
     */
    public function getRoleId() {

        return $this->role instanceof Config ? $this->role->path('id') : false;
    }

    /**
     * @return int Role name
     */
    public function getRoleName() {

        return $this->role instanceof Config ? $this->role->path('name') : false;
    }

    /**
     * @return string Role description
     */
    public function getRoleDescription() {

        return $this->role instanceof Config ? $this->role->path('description') : false;
    }

    /**
     * @return bool
     */
    public function getIsProtected() {

        return $this->role instanceof Config ? $this->role->path('protected') : false;
    }

    /**
     * Sets new option into session profile look and feel
     * 
     * @param string $option
     * @param mixed $parameter
     */
    public function setLookAndFeel($option, $parameter) {

        if (empty($option)) {
            return false;
        }

        $this->look_and_feel->offsetSet($option, $parameter);

        return $this;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getLookAndFeel() {

        return $this->look_and_feel;
    }

    /**
     * @return string
     */
    public function isDarkMode() {

        return $this->look_and_feel instanceof Config ? $this->look_and_feel->path('dark-mode', false) : false;
    }

    /**
     * Convert class properties to array
     * 
     * @return array
     */
    public function toArray() {

        $props = get_class_vars(get_class());

        array_walk($props, function(&$value, $prop) {

            $v = $this->{$prop};

            $value = $v instanceof Config ? $v->toArray() : $v;
        });

        return $props;
    }

}
