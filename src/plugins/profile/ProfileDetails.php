<?php

namespace PMP\Core\Plugins\Profile;

use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Plugins\User\LoginHistory;

class ProfileDetails extends PluginInterface {

    /**
     * @var \PMP\Core\Models\UserProfileDetails User profile model
     */
    protected $profileDetails;

    /**
     * Gets the user profile from cache or cache the profile if it's not cached
     * 
     * @return \PMP\Core\Models\UserProfileDetails User profile model or false
     */
    public function details() {

        if (empty($this->isLogged())) {

            return false;
        }

        if (!empty($this->profileDetails)) {

            return $this->profileDetails;
        }

        $cache = $this->cache;

        if ($cache->isCacheable('UserProfileDetails', $this->getCacheKey())) {

            $this->profileDetails = $cache->getCache($this->getCacheKey());

            return $this->profileDetails;
        }

        $profile = $this->user->getProfileDetails();

        $cache->setCache('UserProfileDetails', $profile, $this->getCacheKey());

        $this->profileDetails = $profile;

        return $this->profileDetails;
    }

    /**
     * Check if user session is expired 
     * 
     * @return bool true if it's expired
     */
    public function isExiredSession() {

        if (empty($this->isLogged())) {

            return true;
        }

        if (date('Y-m-d H:i:s') > $this->sessionProfile->expiredAt()) {

            if (!empty($this->sessionProfile->getLoginId())) {

                $loginHistory = new LoginHistory();

                $loginHistory->saveLogout($this->sessionProfile->getLoginId(), $this->sessionProfile->getSessionTime());
            }

            return true;
        }

        return false;
    }

    /**
     * Check whenever user is logged
     */
    public function isLogged() {

        return !empty($this->sessionProfile->getUserId()) ? true : false;
    }

    /**
     * Clear the profile cache
     */
    public function clearProfileCache() {

        $cache = $this->cache;

        $cache->clearCache('UserProfileDetails', $this->getCacheKey());
    }

    /**
     * Clear User cache
     */
    public function clearUserCache() {

        if (empty($this->sessionProfile->getUserId())) {

            return false;
        }

        $cache = $this->cache;

        $cache->clearCache('user', $this->getUserCacheKey());
    }

    /**
     * Update session with new parameters
     */
    public function updateUserSession() {

        $this->sessionProfile->setLastVisit(date('Y-m-d H:i:s'));

        $this->session->set($this->systemConfig->getSessionName(), $this->sessionProfile->toArray());
    }
    /**
     * Update session with new parameters
     */
    public function updateSessionProfile() {

        $this->session->set($this->systemConfig->getSessionName(), $this->sessionProfile->toArray());
    }

    /**
     * @return string profile cache key
     */
    public function getCacheKey() {

        $id = $this->sessionProfile->getUserId();

        if (empty($id)) {

            return false;
        }
        return '-profile' . '-user-' . $id;
    }

    /**
     * @return string profile cache key
     */
    public function getUserCacheKey() {

        $id = $this->sessionProfile->getUserId();

        if (empty($id)) {

            return false;
        }

        return '-user-' . $id;
    }

}
