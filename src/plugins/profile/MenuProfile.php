<?php

namespace PMP\Core\Plugins\Profile;

/**
 * Class generating the menus
 */
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Plugins\Translate;

class MenuProfile extends PluginInterface {

   public function __construct() {
      
   }

   /**
    * Generate User Profile menu
    */
   public function generateMenu() {
      
      $action = $this->dispatcher->getActionName();

      $controller = $this->dispatcher->getControllerName();

      $menu = [
          'personal' => [
              'controller' => 'profile',
              'icon' => 'icon-profile',
              'class' => '',
              'active' => false,
              'title' => Translate::t('Details')
          ],
          'settings' => [
              'controller' => 'profile',
              'icon' => 'icon-cog3',
              'class' => '',
              'active' => false,
              'title' => Translate::t('Settings'),
              'divider' => false,
              'target' => false
          ]
      ];

      if (!empty($menu[$action]['controller']) && $menu[$action]['controller'] == $controller) {

         $menu[$action]['active'] = true;
         
      }

      return $menu;

   }

}
