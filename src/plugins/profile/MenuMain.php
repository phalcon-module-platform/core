<?php

namespace PMP\Core\Plugins\Profile;

use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Plugins\Translate;

class MenuMain extends PluginInterface {

    protected $role;

    /**
     * Generate main menu
     * @param int $role User role id
     */
    public function __construct() {
        
    }

    /**
     * Generate User Profile menu
     */
    public function generateMenu() {

        $action = lcfirst(\Phalcon\Text::uncamelize($this->dispatcher->getActionName(), "-"));

        //$module = $this->dispatcher->getModuleName();

        $controller = $this->dispatcher->getControllerName();

        $menu = [
            'config' => [
                'controller' => 'config',
                'icon' => '<i class="material-icons">settings</i>',
                'slim-icon' => '<i class="material-icons">settings</i>',
                'class' => '',
                'active' => false,
                'title' => Translate::t('Config'),
                'pageTitle' => Translate::t('Config'),
                'items' => [
                    'global' => [
                        'action' => 'global',
                        'icon' => '',
                        'slim-icon' => 'GL',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Global'),
                        'pageTitle' => Translate::t('Global')
                    ],
                    'assets' => [
                        'action' => 'assets',
                        'icon' => '',
                        'slim-icon' => 'AS',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Assets'),
                        'pageTitle' => Translate::t('Assets')
                    ],
                    'cache' => [
                        'action' => 'cache',
                        'icon' => '',
                        'slim-icon' => 'CH',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Cache'),
                        'pageTitle' => Translate::t('Cache')
                    ],
                    'console' => [
                        'action' => 'console',
                        'icon' => '',
                        'slim-icon' => 'CN',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Console'),
                        'pageTitle' => Translate::t('Console')
                    ],
                    'elements' => [
                        'action' => 'elements',
                        'icon' => '',
                        'slim-icon' => 'EL',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Elements'),
                        'pageTitle' => Translate::t('Elements')
                    ],
                    'email' => [
                        'action' => 'email',
                        'icon' => '',
                        'slim-icon' => 'EM',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Email'),
                        'pageTitle' => Translate::t('Email')
                    ],
                    'flash' => [
                        'action' => 'flash',
                        'icon' => '',
                        'slim-icon' => 'FL',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Flash'),
                        'pageTitle' => Translate::t('Flash')
                    ],
                    'logs' => [
                        'action' => 'logs',
                        'icon' => '',
                        'slim-icon' => 'LG',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Logs'),
                        'pageTitle' => Translate::t('Logs')
                    ],
                    'response' => [
                        'action' => 'response',
                        'icon' => '',
                        'slim-icon' => 'RS',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Response'),
                        'pageTitle' => Translate::t('Response')
                    ],
                    'routes' => [
                        'action' => 'routes',
                        'icon' => '',
                        'slim-icon' => 'RT',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Routes'),
                        'pageTitle' => Translate::t('Routes')
                    ],
                    'paths' => [
                        'action' => 'paths',
                        'icon' => '',
                        'slim-icon' => 'PT',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('System paths'),
                        'pageTitle' => Translate::t('System paths')
                    ],
                ]
            ],
            'services' => [
                'controller' => 'services',
                'icon' => '<i class="sv-normal material-icons">share</i>',
                'slim-icon' => '<i class="sv-slim material-icons">share</i>',
                'class' => '',
                'active' => false,
                'title' => Translate::t('Services'),
                'pageTitle' => Translate::t('Services'),
                'items' => [
                    'shared-services' => [
                        'action' => 'shared-services',
                        'icon' => '',
                        'slim-icon' => 'SS',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Shared services'),
                        'pageTitle' => Translate::t('Shared services')
                    ]
                ]
            ],
            'components' => [
                'controller' => 'components',
                'icon' => '<i class="sv-normal material-icons">apps</i>',
                'slim-icon' => '<i class="sv-slim material-icons">apps</i>',
                'class' => 'dynamic-page',
                'active' => false,
                'title' => Translate::t('Components'),
                'pageTitle' => Translate::t('Components')
            ],
            'access' => [
                'controller' => 'access',
                'icon' => '<i class="material-icons">security</i>',
                'class' => '',
                'active' => false,
                'title' => Translate::t('System access'),
                'pageTitle' => Translate::t('System access'),
                'items' => [
                    'groups' => [
                        'action' => 'groups',
                        'icon' => '',
                        'slim-icon' => 'GR',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Groups'),
                        'pageTitle' => Translate::t('Groups')
                    ]
                ]
            ],
            'resources' => [
                'controller' => 'resources',
                'icon' => '<i class="material-icons">settings_input_composite</i>',
                'class' => 'dynamic-page',
                'active' => false,
                'title' => Translate::t('Resources access'),
                'pageTitle' => Translate::t('Resources access')
            ],
            'menu-builder' => [
                'controller' => 'menu-builder',
                'icon' => '<i class="material-icons">format_align_left</i>',
                'class' => 'dynamic-page',
                'active' => false,
                'title' => Translate::t('Menu builder'),
                'pageTitle' => Translate::t('Menu builder')
            ],
            'users' => [
                'controller' => 'users',
                'icon' => '<i class="material-icons">people_outline</i>',
                'sim-icon' => '<i class="material-icons">people_outline</i>',
                'class' => 'dynamic-page',
                'active' => false,
                'title' => Translate::t('Users'),
                'pageTitle' => Translate::t('Users')
            ],
            'language' => [
                'controller' => 'language',
                'icon' => '<i class="material-icons">flag</i>',
                'slim-icon' => '<i class="material-icons">flag</i>',
                'class' => '',
                'active' => false,
                'title' => Translate::t('Language'),
                'pageTitle' => Translate::t('Language'),
                'items' => [
                    'manage' => [
                        'action' => 'manage',
                        'icon' => '',
                        'slim-icon' => 'ML',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Manage Languages'),
                        'pageTitle' => Translate::t('Manage Languages')
                    ]
                ]
            ],
            'icons' => [
                'controller' => 'icons',
                'icon' => '<i class="material-icons">apps</i>',
                'slim-icon' => '<i class="material-icons">apps</i>',
                'class' => '',
                'active' => false,
                'title' => Translate::t('Icons'),
                'pageTitle' => Translate::t('Icons'),
                'items' => [
                    'fa-icons' => [
                        'action' => 'fa-icons',
                        'icon' => '',
                        'slim-icon' => 'IC',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Fa-icons'),
                        'pageTitle' => Translate::t('Fa-icons')
                    ],
                    'material-design-icons' => [
                        'action' => 'icons',
                        'icon' => '',
                        'slim-icon' => 'MD',
                        'class' => 'dynamic-page',
                        'active' => false,
                        'title' => Translate::t('Material design icons'),
                        'pageTitle' => Translate::t('Material design icons')
                    ]
                ]
            ]
        ];

        if (!empty($menu[$controller])) {

            $menu[$controller]['active'] = 'active';

            $this->tag->setTitle($menu[$controller]['pageTitle']);
        }

        if (!empty($menu[$controller]['items'][$action])) {

            $menu[$controller]['items'][$action]['active'] = 'active';

            $this->tag->appendTitle(' - ' . $menu[$controller]['items'][$action]['pageTitle']);
        }

        return $menu;
    }

}
