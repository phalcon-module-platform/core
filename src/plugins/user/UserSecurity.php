<?php

namespace PMP\Core\Plugins\User;

use Phalcon\Config;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Plugins\User\AccountNotifications;
use PMP\Core\Plugins\User\LoginHistory;
use PMP\Core\Plugins\Profile\SessionProfile;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Plugins\Translate;

class UserSecurity extends PluginInterface {

    /**
     * @var \PMP\Core\Models\User 
     */
    protected $_user;

    /**
     * @var PMP\Core\Models\UserSecurity 
     */
    protected $user_security;

    /**
     * @param \PMP\Core\Models\User $user
     */
    public function __construct(\PMP\Core\Models\User $user) {

        $this->_user = $user;

        //
    }

    /**
     * Check if password is correct.
     * @param string $password Password from web-form
     */
    public function checkPassword($password) {

        $valid = $this->security->checkHash($password, $this->getUser()->getPassword());

        if ($valid === true) {
            return true;
        }
        
        //alert user when wrong password has been typed 
        if ($this->userSecurity()->getLoginUnathorizedAlert() && $this->getUser()->getStatus() == UserConstants::status_active) {

            $alert = new AccountNotifications();

            $alert->setUser($this->getUser())
                    ->setLayout($this->systemMessage->get('EMAIL_LAYOUT_WRONG_LOGIN_ATTEMPT'))
                    ->setSubject(Translate::t($this->systemMessage->get('EMAIL_SUBJECT_WRONG_LOGIN_ATTEMPT')));


            $alert->send();
        }

        return false;
    }

    /**
     * Save the new login details into login history and set the session
     * Set user session.
     */
    public function setSession() {

        $lh = new LoginHistory();

        $lh->saveLogin($this->getUser()->getId());

        $address = [$lh->getLoginHistory()->getCity(), $lh->getLoginHistory()->getCountry()];

        /* @var \PMP\Core\Models\SystemRoles */
        $system_role = $this->getUser()->getRelated('SystemRoles', ['role' => 'id']);

        $params = new Config([
            'id' => $this->getUser()->getId(),
            'login_id' => $lh->getLoginHistory()->getId(),
            'last_visit' => date('Y-m-d H:i:s'),
            'fullname' => $this->getUser()->getFullName(),
            'email' => $this->getUser()->getEmail(),
            'role' => $system_role->toArray(),
            'session_time' => $this->userSecurity()->getSessionTime(),
            'token' => $this->security->getToken(),
            'look_and_feel' => [
                'dark-mode' => false
            ]
        ]);

        $sessionProfile = new SessionProfile($params);

        //isolate user session 
        $this->session->set($this->systemConfig->getSessionName(), $sessionProfile->toArray());

        if ($this->userSecurity()->getLoginAlert()) {

            $alert = new AccountNotifications();

            $alert->setUser($this->getUser())
                    ->setLayout($this->systemMessage->get('EMAIL_LAYOUT_SUCCESS_LOGIN'))
                    ->setSubject(Translate::t($this->systemMessage->get('EMAIL_SUBJECT_SUCCESS_LOGIN')));

            $alert->send();
        }

        return $this->getUser()
                        ->setLastVisit(date('Y-m-d H:i:s'))
                        ->setWhereDid(implode(', ', array_filter($address)))
                        ->save();
    }

    /**
     * @return \PMP\Core\Models\User
     */
    public function getUser() {

        return $this->_user;
    }

    /**
     * @return \PMP\Core\Models\UserSecurity
     */
    public function userSecurity() {

        if (!empty($this->user_security)) {

            return $this->user_security;
        }

        $security = $this->_user->getSecurity();

        $this->user_security = $security;

        return $this->user_security;
    }

}
