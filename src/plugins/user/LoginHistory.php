<?php

namespace PMP\Core\Plugins\User;

use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Models\UserLoginsHistory;

class LoginHistory extends PluginInterface {

    /**
     * @var \PMP\Core\Models\UserLoginsHistory
     */
    protected $login_history;

    public function __construct() {
        
    }

    /**
     * Update user logout time
     * 
     * @param int $loginId Last login ID
     * @param int $session_time Session time in seconds
     */
    public function saveLogout($loginId, $session_time = false) {

        $history = UserLoginsHistory::findFirstById($loginId);

        if (empty($history)) {

            return false;
        }

        //calculate time in seconds
        $expiry_time = new \DateTime($history->getLoginDate());

        $current_date = new \DateTime();

        $diff = !empty($session_time) ? $session_time : $current_date->getTimestamp() - $expiry_time->getTimestamp();

        $history->setTimeSpent($diff);

        $history->setLogoutDate(date('Y-m-d H:i:s'));

        $history->save();
    }

    /**
     * 
     * @param int $userId User ID
     * 
     * @return int Last insert ID
     */
    public function saveLogin($userId) {

        $loginHistory = new UserLoginsHistory();

        $loginHistory->setUserId($userId);

        /*@var $ipdata \PMP\Core\Plugins\IpData */
        $ipdata = $this->ipdata;

        $ipdata->locate($this->request->getClientAddress(true));

        $loginHistory->setIpAddress($ipdata->getIpAddress());

        $loginHistory->setUserAgent($this->request->getUserAgent());

        $loginHistory->setCity($ipdata->getCity());

        $loginHistory->setLatitude($ipdata->getLatitude());

        $loginHistory->setLongtitude($ipdata->getLongitude());

        $loginHistory->setCountry($ipdata->getCountry());

        $loginHistory->setCountryCode($ipdata->getCountryCode());

        $loginHistory->setLoginDate(date('Y-m-d H:i:s'));

        $loginHistory->save();

        $this->login_history = $loginHistory;

        return $this->db->lastInsertId();
    }

    /**
     * @return \PMP\Core\Models\UserLoginsHistory
     */
    public function getLoginHistory() {

        return $this->login_history;
    }

}
