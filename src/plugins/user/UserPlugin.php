<?php

namespace PMP\Core\Plugins\User;

use PMP\Core\Models\User;
use PMP\Core\Plugins\PluginInterface;

/**
 * class of UserPlugin
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class UserPlugin extends PluginInterface {

    public function load() {

        $id = $this->sessionProfile->getUserId();

        if (empty($id)) {
            return false;
        }

        /* @var $cache \PMP\Core\Plugins\Cache */
        $cache = $this->cache ? $this->cache : false;

        $key = $this->profile->getUserCacheKey();

        if ($key && $cache && $cache->isCacheable('user', $key)) {

            return $cache->getCache($key);
            
        } else {

            $user = User::findFirstById($id);

            $cache ? $cache->setCache('user', $user, $key) : false;
        }

        return $user;
    }

}
