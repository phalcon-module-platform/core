<?php

/**
 * Account Notifications
 * 
 * Working with User Account and User Profile 
 */

namespace PMP\Core\Plugins\User;

use Phalcon\Config;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Library\FileSystem;
use PMP\Core\Library\Validator;

class AccountNotifications extends PluginInterface {

    /**
     * @var \PMP\Core\Models\User
     */
    private $user;

    /**
     * @var string Token
     */
    private $token;

    /**
     * @var string Subject
     */
    private $subject;

    /**
     * @var string Layout
     */
    private $layout;

    /**
     * @var \Phalcon\Config
     */
    private $additional_params;

    /**
     * @var \Phalcon\Config
     */
    private $full_params;

    /**
     * @var \Phalcon\Config
     */
    private $default_params;

    /**
     * @var bool
     */
    private $use_locator = true;

    /**
     * By default IP locator is ON, call this function to turn it OFF
     */
    public function ignoreIpLocator() {

        $this->use_locator = false;

        return $this;
    }

    /**
     * @return string User email address
     */
    public function getEmail() {

        return !empty($this->user) ? $this->user->getEmail() : false;
    }

    /**
     * @return string User Full Name
     */
    public function getFullName() {

        return !empty($this->user) ? $this->user->getFullName() : false;
    }

    /**
     * @return string Token where used
     */
    public function getToken() {

        return $this->token;
    }

    /**
     * @return string Subject where used
     */
    public function getSubject() {

        return $this->subject;
    }

    /**
     * @return string Layout name
     */
    public function getLayout() {

        return $this->layout;
    }

    /**
     * @param \PMP\Core\Models\User $user
     */
    public function setUser(\PMP\Core\Models\User $user) {

        $this->user = $user;

        return $this;
    }

    /**
     * @param string $token Token where can be used into email templates
     */
    public function setToken($token = false) {

        $this->token = $token;

        return $this;
    }

    /**
     * @param string $subject Email subject
     */
    public function setSubject($subject = false) {

        $this->subject = $subject;

        return $this;
    }

    /**
     * @param string $layout Layout name located in partial folder
     */
    public function setLayout($layout) {

        $this->layout = $layout;

        return $this;
    }

    /**
     * @return string Render Layout
     */
    public function renderLayout() {

        $params = $this->getAllParams();

        $this->viewToAjax->setLayout($this->getLayout());

        $this->viewToAjax->setVariables($params->toArray());

        return $this->viewToAjax->_getView();
    }

    /**
     * Set new or override default parameters
     * 
     * @return \Phalcon\Config Default parameters used in email template
     */
    public function setAdditionalParams($params = []) {

        $this->additional_params = new Config($params);

        return $this;
    }

    /**
     * @return \Phalcon\Config Additional parameters
     */
    public function getAdditionalParams() {

        return !empty($this->additional_params) ? $this->additional_params : new Config([]);
    }

    /**
     * @return \Phalcon\Config Default parameters used in email template
     */
    public function getDefaultParams() {

        if (!empty($this->default_params)) {

            return $this->default_params;
        }

        $ip = $this->ipdata;

        if ($this->use_locator === true) {

            $ip->locate($this->request->getClientAddress(true));
        }

        $location = [$ip->getRegion(), $ip->getCountry(), $ip->getCity(), $ip->getPostalCode()];

        $lat_long = !empty($ip->getLatitude()) && !empty($ip->getLongitude()) ?
                'https://maps.google.com/?q=' . $ip->getLatitude() . ',' . $ip->getLongitude() . '&z=18' :
                false;

        $params = new Config([
            'name' => $this->getFullName(),
            'platform_name' => $this->systemConfig->path('global.platform.title'),
            'platform_url' => $this->request->getScheme() . '://' . $this->request->getServerName(),
            'email_address' => $this->getEmail(),
            'support_url' => $this->systemConfig->path('global.platform.support_url'),
            'token' => $this->getToken(),
            'ip_address' => $ip->getIpAddress(),
            'location' => implode(', ', array_filter($location)),
            'lat_long' => $lat_long,
            'browser_name' => $this->request->getUserAgent(),
            'company_name' => $this->systemConfig->path('global.company.name'),
            'company_address' => $this->systemConfig->path('global.company.address'),
            'help_url' => $this->systemConfig->path('global.platform.help_url'),
            'live_chat_url' => $this->systemConfig->path('global.platform.live_chat_url'),
            'subscription_length' => '14 days',
            'subscription_start_date' => date('Y-m-d'),
            'subscription_end_date' => date('Y-m-d', strtotime('now +14 days')),
            'support_email' => $this->systemConfig->path('global.platform.support_email'),
        ]);

        $this->default_params = $params;

        return $this->default_params;
    }

    /**
     * @return \Phalcon\Config All parameters where used in template
     */
    public function getAllParams() {

        if (!empty($this->full_params)) {

            return $this->full_params;
        }

        $default_params = $this->getDefaultParams();

        $add_params = $this->getAdditionalParams();

        $this->full_params = $default_params->merge($add_params);

        return $this->full_params;
    }

    /**
     * Send the email
     */
    public function send() {

        if (empty($this->mailManager->getHost())) {
            return false;
        }
        
        $email = Validator::email($this->getEmail()) === true ? $this->getEmail() : false;

        if (empty($email) || empty($this->getSubject())) {

            return false;
        }

        $this->mailManager->setRecipient(new Config([
                    'subject' => $this->getSubject(),
                    'email' => $this->getEmail(),
                    'template' => $this->renderLayout()
        ]));

        $this->mailManager->send();

        return $this->mailManager;
    }

    /**
     * Test connection to mail server
     * 
     * @param \Phalcon\Config $params
     * 
     * @return mixed TRUE if connection success else Array with errors
     */
    public function testMailServer(\Phalcon\Config $params) {

        if (empty($params->path('username')) || empty($params->path('password'))) {

            return [];
        }

        error_reporting(0);

        ini_set('display_errors', 0);

        ini_set('log_errors', '0');

        imap_timeout(IMAP_OPENTIMEOUT, 1);

        imap_timeout(IMAP_READTIMEOUT, 1);

        imap_timeout(IMAP_WRITETIMEOUT, 1);

        imap_timeout(IMAP_CLOSETIMEOUT, 1);

        $connectionLine = "{";

        $connectionLine .= $params->path('host') . ':';

        $connectionLine .= $params->path('encryption') == 'ssl' ? 993 : 143;

        $connectionLine .= '/imap/' . $params->path('encryption');

        $connectionLine .= '/novalidate-cert';

        $connectionLine .= '}';

        $mbox = \imap_open($connectionLine, $params->path('username'), $params->path('password'));

        $errors = imap_errors();

        if (!empty($errors)) {

            $log = date('Y-m-d H:i:s') . PHP_EOL;

            $log .= implode(PHP_EOL, $errors) . PHP_EOL;

            FileSystem::writeFile($this->systemConfig->getEmailLogsPath() . '/error.log', $log, true);
        }

        imap_close($mbox);

        return empty($errors) ? true : $errors;
    }

}
