<?php

namespace PMP\Core\Plugins;

use Phalcon\Text;
use PMP\Core\Plugins\PluginInterface;

class ConsolePlugin extends PluginInterface {

    /**
     * @var string
     */
    private $task;

    /**
     * @var string
     */
    private $action;

    /**
     * @var []
     */
    private $options;

    /**
     * @var string
     */
    private $executed_command;

    /**
     * @var string
     */
    private $params_after;

    /**
     * @var string
     */
    private $params_before;

    /**
     * Execute command with shell_exec (for linux) and popen for Windows 
     * 
     * @return string The output from the executed command or <b>NULL</b> if an error occurred or the
     * command produces no output.
     * </p><p>
     * This function can return <b>NULL</b> both when an error occurs or the program
     * produces no output. It is not possible to detect execution failures using
     * this function. <b>exec</b> should be used when access to the
     * program exit code is required.
     */
    public function shellExec() {

        $command = $this->getShellCommand();

        if ($this->systemConfig->getOs() == 'win') {

            $exec = \pclose(\popen($command, 'w'));
        } else {

            $exec = \shell_exec($command);
        }

        return $exec;
    }

    /**
     * @param array $options [task, action, options]
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin
     */
    public function load($options = []) {

        foreach ($options as $prop => $value) {
            $method = 'set' . Text::camelize($prop);
            if (method_exists(get_class(), $method)) {
                $this->{$method}($value);
            }
        }

        return $this;
    }

    /**
     * @param string $task
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin
     */
    public function setTask($task) {
        $this->task = $task;

        return $this;
    }

    /**
     * @param string $action
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin 
     */
    public function setAction($action) {

        $this->action = $action;

        return $this;
    }

    /**
     * Encode the options
     * 
     * @param mixed $options
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin
     */
    public function setOptions($options = []) {

        $arguments = json_encode($options);

        $input = str_replace('"', '\"', $arguments);

        $this->options = '"' . $input . '"';

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions() {

        return $this->options;
    }

    /**
     * 
     * @param type $params additional parameters right after script bin
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin
     */
    public function setParamsBefore($params = []) {

        $this->params_before = implode(' ', $params);

        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getParamsBefore() {


        return $this->params_before;
    }

    /**
     * Additional parameters right after options. 
     * If options does not exists, parameters will be added right after executable command 
     * 
     * @param array $params 
     * 
     * @example cli.php main main "{\"options\":[1,2,3,4,5,6,7]}" param1 param2
     * @example cli.php main main param1 param2
     * 
     * @return \PMP\Core\Plugins\ConsolePlugin
     */
    public function setParamsAfter($params = []) {

        $this->params_after = implode(' ', $params);

        return $this;
    }

    /**
     * 
     * @return string
     */
    public function getParamsAfter() {

        return $this->params_after;
    }

    /**
     * @return string The command that has been executed
     */
    public function getExecutedCommand() {

        return $this->executed_command;
    }

    /**
     * @return string Shell command to execute
     */
    public function getShellCommand() {

        $command = [
            $this->systemConfig->getSystemPathsConfig()->path('php'),
            $this->systemConfig->getAppFullDir() . '/cli.php',
            $this->getParamsBefore(),
            $this->task,
            $this->action
        ];

        $command[] = $this->getOptions();
        $command[] = $this->getParamsAfter();

        $arguments = array_filter($command);

        if ($this->systemConfig->getOs() == 'win') {

            $arguments[] = ' 2>&1';

            $this->executed_command = 'start /B ' . implode(' ', $arguments);
        } else {

            $arguments[] = '> /dev/null &';

            $this->executed_command = implode(' ', $arguments);
        }

        return $this->executed_command;
    }

}
