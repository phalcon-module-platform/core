<?php

namespace PMP\Core\Plugins;

/**
 * class DynamicModal 
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class DynamicModal {

    /**
     * @var string Modal ID
     */
    private $targetModal = 'targetModal';

    /**
     * @var string Modal ID
     */
    private $message = 'message';

    /**
     * @var string Modal HTML content
     */
    private $modalContent = 'modalContent';

    /**
     * @var array
     */
    private $params = [];

    /**
     * @param string $targetModal
     */
    public function __construct($targetModal = false) {

        $this->params[$this->targetModal] = $targetModal;

        $this->params[$this->message] = false;

        $this->params[$this->modalContent] = false;
    }

    /**
     * @param string $modalContent
     */
    public function setModalContent($modalContent = false) {

        $this->params[$this->modalContent] = $modalContent;
    }

    /**
     * @return mixed Modal content or false
     */
    public function getModalContent() {

        return !empty($this->params[$this->modalContent]) ? $this->params[$this->modalContent] : false;
    }

    /**
     * Adding key value pair to response
     * 
     * @param array $param
     */
    public function addToParams($param = []) {

        if (empty($param) || !is_array($param)) {
            return false;
        }

        $this->params = array_merge($this->params, $param);
    }

    /**
     * @return array Parameters needed to render the modal
     */
    public function getModalParams() {

        return $this->params;
    }

}
