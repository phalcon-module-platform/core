<?php

/**
 * FiltersPlugin for Data normalization
 */

namespace PMP\Core\Plugins;

use PMP\Core\Plugins\PluginInterface;

class FiltersPlugin extends PluginInterface {

    /**
     * Number as Float and round Half UP
     * 
     * @param string|number $float
     * @param number $decimals
     * 
     * @return float
     */
    public function toFloat($float = '0.00', $decimals = 2) {

        //$round = round($float, $decimals, PHP_ROUND_HALF_UP);
        
        $value = number_format($float, (int) $decimals, '.', '');

        return (float) $value;
    }

}
