<?php

namespace PMP\Core\Plugins\Forms;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;

class LoginForm extends Validation {

  public function initialize() {

    $this->add('email', new PresenceOf([
        'message' => 'LOGIN_ERROR_EMAIL_EMPTY'
    ]));
    
    $this->add('password', new PresenceOf([
        'message' => 'LOGIN_ERROR_PASSWORD_EMPTY'
    ]));

    $this->setFilters('password', 'trim');
    
    $this->setFilters('email', 'trim');

  }

}
