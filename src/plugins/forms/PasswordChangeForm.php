<?php

namespace PMP\Core\Plugins\Forms;

use Phalcon\Validation;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\PasswordStrength;
use Phalcon\Validation\Validator\Identical;

class PasswordChangeForm extends Validation {

    public function initialize() {

        $this->add('password', new PresenceOf([
            'message' => 'RECOVER_ERROR_MISSING_PASSWORD'
        ]));

        $this->add('password', new StringLength([
            'max' => 32,
            'messageMaximum' => 'RECOVER_ERROR_PASSWORD_LENGHT_MAX',
            'min' => 6,
            'messageMinimum' => 'RECOVER_ERROR_PASSWORD_LENGHT_MIN',
        ]));

        $this->add('password', new PasswordStrength([
            'minScore' => 3,
            'message' => 'RECOVER_ERROR_PASSWORD_WEAK',
            'allowEmpty' => false
        ]));


        $this->add('re-password', new PresenceOf([
            'message' => 'RECOVER_ERROR_PASSWORD_REPEAT'
        ]));

        $this->add('re-password', new Identical([
            'value' => $this->request->getPost('password'),
            'message' => 'RECOVER_ERROR_PASSWORD_NOT_MATCH'
        ]));


        $this->setFilters('password', 'trim');

        $this->setFilters('re-password', 'trim');
        
    }

}
