<?php

namespace PMP\Core\Plugins;

use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Plugins\Translate;

/**
 * Ajax response styles
 * 
 */
class FlashStyle extends PluginInterface {

    /**
     * @var array Default response style if is not configured into config file
     */
    private $flashconfig = [
        'error' => [
            'notifytitle' => 'Error',
            'notifyclass' => 'bg-danger',
            'notifytype' => 'error',
            'notifyicon' => 'icon-blocked'
        ],
        'success' => [
            'notifytitle' => 'Success',
            'notifyclass' => 'bg-success',
            'notifytype' => 'success',
            'notifyicon' => 'icon-checkmark3'
        ],
        'warning' => [
            'notifytitle' => 'Warning',
            'notifyclass' => 'bg-warning',
            'notifytype' => 'warning',
            'notifyicon' => 'icon-checkmark'
        ],
        'info' => [
            'notifytitle' => 'Info',
            'notifyclass' => 'bg-info',
            'notifytype' => 'info',
            'notifyicon' => 'icon-checkmark'
        ]
    ];

    public function __construct() {

        if (!empty($this->systemConfig->getFlashConfig()->path('ajax'))) {

            $this->flashconfig = $this->systemConfig->getFlashConfig()->path('ajax')->toArray();
        }
    }

    /**
     * Translate response title 
     */
    public function translateFlash() {

        !empty($this->flashconfig['error']['notifytitle']) ?
                        $this->flashconfig['error']['notifytitle'] = Translate::t($this->flashconfig['error']['notifytitle']) :
                        '';
        !empty($this->flashconfig['success']['notifytitle']) ?
                        $this->flashconfig['success']['notifytitle'] = Translate::t($this->flashconfig['success']['notifytitle']) :
                        '';

        !empty($this->flashconfig['warning']['notifytitle']) ?
                        $this->flashconfig['warning']['notifytitle'] = Translate::t($this->flashconfig['warning']['notifytitle']) :
                        '';
        !empty($this->flashconfig['info']['notifytitle']) ?
                        $this->flashconfig['info']['notifytitle'] = Translate::t($this->flashconfig['info']['notifytitle']) :
                        '';
    }

    /**
     * Return the response style
     * 
     * @param string $type Response type : error,success,warning,info
     * @param array $params Response parameters : message, title, redirect, reload
     * 
     * @return array Merged array with the response style and parameters
     */
    public function responseStyle($type = null, $params = []) {

        $style = (!empty($type) && array_key_exists($type, $this->flashconfig)) ?
                $type :
                'error';

        return !empty($params) && is_array($params) ?
                array_merge($params, $this->flashconfig[$style]) :
                $this->flashconfig[$style];
    }

    /**
     * Return error messages and error styles
     * 
     */
    public function error($params = []) {

        return array_merge($params, $this->flashconfig['error']);
    }

    public function warning($params = []) {
        return array_merge($params, $this->flashconfig['warning']);
    }

    public function success($params = []) {
        return array_merge($params, $this->flashconfig['success']);
    }

    public function info($params = []) {
        return array_merge($params, $this->flashconfig['info']);
    }

    /**
     * @return array Default ajax messages style configuration
     */
    public function getConfig() {

        return $this->flashconfig;
    }

}
