<?php

namespace PMP\Core\Plugins;

use Phalcon\Text;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Mvc\Model\Query\Builder;
use Phalcon\Config\Adapter\Json AS JsonAdapter;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Library\FileSystem;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Models\SystemAccess;
use PMP\Core\Models\SystemRoles;
use PMP\Core\Models\SystemActions;
use PMP\Core\Models\SystemControllers;
use PMP\Core\Models\SystemModules;

/**
 * AccessList Plugin.
 * 
 */
class AccessList extends PluginInterface {

    /**
     * @var \Phalcon\Acl\Adapter\Memory
     */
    protected $acl;

    /**
     * @var int
     */
    protected $role;

    /**
     * Using Role from sessionProfile. If sessionProfile is empty, default Role is Guest
     * 
     */
    public function set() {

        $roleId = $this->sessionProfile->getRoleId();

        $this->role = !empty($roleId) ? $roleId : UserConstants::role_guest;
    }

    /**
     * @return int Role from session Profile
     */
    public function getRole() {

        return $this->role;
    }

    /**
     * Add Access list to session Bag
     * 
     * @param \Phalcon\Acl\Adapter\Memory $acl
     */
    public function setPersistent($acl) {

        $this->persistent->acl = $acl;
    }

    /**
     * Gets the Access list from session Bag
     * 
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getPersistent() {

        return $this->persistent->acl;
    }

    /**
     * Clear access list if exists into Session Bag
     */
    public function clearPersistent() {

        if ($this->persistent->has('acl')) {

            $this->persistent->destroy();
        }
    }

    /**
     * Load access list from configuration
     * 
     * @return \Phalcon\Acl\Adapter\Memory
     */
    public function getAccessList() {

        if (!empty($this->acl)) {

            return $this->acl;
        }

        //Load access list from cache if exists and if need to be used from 
        if ($this->persistent->has('acl') && $this->systemConfig->getAclUsePersistent()) {

            $this->acl = $this->getPersistent();

            return $this->acl;
        }

        $acls = ($this->systemConfig->getAclUsageFromFile()) ?
                $this->loadFromFile($this->systemConfig->getAclFile()) :
                $this->loadFromDb();

        $acl = new AclMemory();

        $acl->setDefaultAction(
                $acls['defaultAction']
        );

        foreach ($acls['resource'] as $resource => $options) {

            if ($resource == '*') {
                continue;
            }
            $acl->addResource(new Resource(
                            $resource, $options['description']
                    ), !empty($options['actions']) ? $options['actions'] : []
            );
        }

        foreach ($acls['role'] as $role => $access) {

            try {
                $acl->addRole(new Role((string) $access['id'], (string) $access['description']));
            } catch (\Exception $exc) {
                
            }
            if (!empty($access['allow']) && is_array($access['allow'])) {

                $this->addAllowed($acl, (string) $access['id'], $access['allow']);
            } else if (!empty($access['deny']) && is_array($access['deny'])) {

                $this->addDeny($acl, (string) $access['id'], $access['deny']);
            } else {

                $acl->allow((string) $access['id'], '*', '*');
            }
        }

        //if persistent must be used
        if (!empty($this->systemConfig->getAclUsePersistent())) {
            $this->setPersistent($acl);
        }

        $this->acl = $acl;

        return $acl;
    }

    /**
     * @return array Modules with System Controllers access
     */
    public function getSystemControllers() {

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->columns([
            'module_id' => 'M.id',
            'module_name' => 'M.name',
            'resource' => 'SC.name',
            'actions' => 'SA.name',
            'description' => 'SC.description'
        ]);

        $dataSource->addFrom(SystemControllers::class, 'SC');

        $dataSource->innerJoin(SystemActions::class, 'SA.fkicontroller = SC.id', 'SA');

        $dataSource->innerJoin(SystemModules::class, 'M.id = SC.fkimodule', 'M');

        /* @var $system_controllers  \Phalcon\Mvc\Model\Resultset\Simple */
        $system_controllers = $dataSource->getQuery()->execute();

        return $system_controllers->toArray();
    }

    /**
     * 
     * @return array Roles and System Access
     */
    public function getSystemAccess() {

        /* @var $dataSource  \Phalcon\Mvc\Model\Query\Builder */
        $dataSource = new Builder();

        $dataSource->columns([
            'module_id' => 'M.id',
            'module_name' => 'M.name',
            'module_description' => 'M.description',
            'id' => 'SA.id',
            'roleid' => 'SR.id',
            'role' => 'SR.name',
            'access' => 'SA.access',
            'resource' => 'SC.name',
            'action' => 'A.name',
            'role_description' => 'SR.description'
        ]);

        $dataSource->addFrom(SystemAccess::class, 'SA');

        $dataSource->innerJoin(SystemRoles::class, 'SR.id = SA.fkirole', 'SR');

        $dataSource->innerJoin(SystemActions::class, 'A.id = SA.fkiaction', 'A');

        $dataSource->innerJoin(SystemControllers::class, 'SC.id = SA.fkicontroller', 'SC');

        $dataSource->innerJoin(SystemModules::class, 'M.id = SC.fkimodule', 'M');

        $dataSource->orderBy('SR.name,SC.name ASC');

        /* @var $system_access  \Phalcon\Mvc\Model\Resultset\Simple */
        $system_access = $dataSource->getQuery()->execute();

        return $system_access->toArray();
    }

    /**
     * @return bool
     */
    public function hasAccess() {

        /* @var $acl \Phalcon\Acl\Adapter\Memory */
        $acl = $this->getAccessList();

        //return true if default module requested
        if ($this->dispatcher->getModuleName() === $this->router->getDefaultModule()) {
            return true;
        }

        // if full access granted
        if ($acl->isAllowed($this->getRole(), '*!*', '*')) {
            return true;
        }

        $fullModuleAccess = $this->dispatcher->getModuleName() . ':*';

        $moduleControllerAccess = lcfirst(Text::camelize($this->dispatcher->getModuleName(), "-")) . ':' . lcfirst(Text::camelize($this->dispatcher->getControllerName(), "-"));

        //if full module access return true
        if ($acl->isAllowed($this->getRole(), $fullModuleAccess, $this->dispatcher->getActionName())) {

            return true;
        }

        // check access based on controllers
        if (!$acl->isAllowed($this->getRole(), $moduleControllerAccess, $this->dispatcher->getActionName())) {

            return false;
        }

        return true;
    }

    /**
     * Redirect User to Error page if any error occur 
     * 
     * @param array $params [url, message]
     */
    public function redirect($params = []) {

        $url = (!empty($params['url'])) ? $params['url'] : '/';

        $mesage = (!empty($params['message'])) ? $params['message'] : '';

        //fix data table js error when session expire
        if (empty($params['data'])) {
            $params['data'] = [];
        }

        if ($this->request->isAjax()) {

            $this->response->setError();

            $this->response->setData($params);

            !empty($params['url']) ?
                            $this->response->setRedirectUrl($params['url']) :
                            false;

            $this->response->setCloseModal();

            $this->response->sendResponse();

            return false;
        }

        $this->flashSession->clear();

        $this->flashSession->warning($mesage);

        $this->response->redirect($url, true, 400)->send();

        //stop executing the router
        return false;
    }

    /**
     * Add allowed resources to access list
     * 
     * @param \Phalcon\Acl\Adapter\Memory $acl Memory Adapter
     * @param string $role Role name
     * @param array $accesses Accesses to allow
     */
    private function addAllowed(\Phalcon\Acl\Adapter\Memory $acl, $role, $accesses) {

        foreach ($accesses as $controller => $actions) {

            if (!$acl->isResource($controller)) {
                continue;
            }

            //2, profile:security, [action1, action2]
            $acl->allow($role, $controller, $actions['actions']);
        }
    }

    /**
     * Add deny resources to access list
     * 
     * @param \Phalcon\Acl\Adapter\Memory $acl Memory Adapter
     * @param string $role Role name
     * @param array $deny Accesses to allow
     */
    private function addDeny(\Phalcon\Acl\Adapter\Memory $acl, $role, $deny) {

        foreach ($deny as $controller => $actions) {

            if ($acl->isResource($controller)) {

                $acl->deny($role, $controller, $actions['actions']);
            }
        }
    }

    /**
     * Load Access List Configuration from ini file.If file not exists, load access list from dB and save to file
     * 
     * @param string $file location to ini file
     * @return array Access list configuration
     */
    private function loadFromFile($file) {

        //get acl from file if exists
        if (file_exists($file)) {

            $adapter = new JsonAdapter($file);

            return $adapter->toArray();
        }

        //get acl from database and write file
        $acls = $this->loadFromDb();

        FileSystem::writeFile($file, json_encode($acls, JSON_PRETTY_PRINT));

        return $acls;
    }

    /**
     * Load access list from db and save to cache <br /> 
     */
    private function loadFromDb() {

        $sc = $this->getSystemControllers();

        $sa = $this->getSystemAccess();

        $resources = [];
        $roles = [];
        $acl = [];

        //Add administrator to access list
        $roles[UserConstants::role_admin_name] = [
            'description' => $this->systemMessage->get(UserConstants::role_admin_title),
            'id' => UserConstants::role_admin,
            'allow' => '*'
        ];

        foreach ($sc as $r) {

            $resource = $r['module_name'] . ':' . $r['resource'];

            $resources[$resource]['description'] = $r['description'];
            ($r['actions'] == '*') ?
                            $resources[$resource]['actions'] = '' :
                            $resources[$resource]['actions'][] = $r['actions'];
        }

        foreach ($sa as $role) {

            $roleresource = $role['module_name'] . ':' . $role['resource'];

            $roles[$role['role']]['description'] = $role['role_description'];
            $roles[$role['role']]['id'] = $role['roleid'];

            ($role['action'] == '*') ?
                            $roles[$role['role']][$role['access']][$roleresource]['actions'] = '*' :
                            $roles[$role['role']][$role['access']][$roleresource]['actions'][] = $role['action'];
        }

        $acl['defaultAction'] = 0;
        $acl['resource'] = $resources;
        $acl['role'] = $roles;

        return $acl;
    }

}
