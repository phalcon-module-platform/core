<?php

namespace PMP\Core\Plugins;

use Phalcon\Session\Adapter\Files AS SessionAdapter;
use PMP\Core\Library\FileSystem;

/**
 * class SessionPlugin
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class Session extends SessionAdapter {

    /**
     * @param string $path
     */
    public function setSessionSavePath($path = false) {

        if (empty($path)) {
            return false;
        }

        $dir = FileSystem::makeDir($path);

        session_save_path($dir);
    }

    public function setOptions(array $options) {

        return parent::setOptions($options);
    }

    /**
     * @param string $session_name Session name
     * 
     * @return array|boll false if user session does not exists
     */
    public function getUserSession($session_name = false) {

        if (empty($session_name)) {
            return false;
        }

        return $this->has($session_name) ? $this->get($session_name) : false;
    }

    /**
     * Sets a session variable in an application context
     *
     * <code>
     * $session->set("auth", "yes");
     * </code>
     *
     * @param string $index
     * @param mixed $value
     */
    public function set($index, $value) {
        parent::set($index, $value);
    }

    /**
     * Resolve key from session 
     * 
     * @param string $sessionName
     * 
     * @return mixed login_id or false
     */
    public function hasLoginId($sessionName) {

        $session = parent::get($sessionName);

        return !empty($session['login_id']) ? $session['login_id'] : false;
    }

}
