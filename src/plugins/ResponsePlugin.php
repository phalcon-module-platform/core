<?php

namespace PMP\Core\Plugins;

use Phalcon\Config;
use Phalcon\Http\Response;
use PMP\Core\Library\Array2XML;
use PMP\Core\Library\FileSystem;

class ResponsePlugin extends Response {

    /**
     * @var string Response style
     */
    protected $style;

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @var \Phalcon\Config
     */
    protected $data;

    /**
     * @var string Default response type
     */
    protected $type = 'json';

    /**
     * @var bool flash style
     */
    protected $noflash;

    /**
     * @var bool
     */
    protected $is_response = false;

    /**
     * @var array Callback functions to run in FE
     */
    protected $_callbacks = [];

    public function setConfig(\Phalcon\Config $config = null) {

        $options = $config instanceof \Phalcon\Config ? $config : new Config([]);

        $this->setCache((int) $options->path('cache', 0));

        $this->setHeader('X-SAPI', php_sapi_name());

        $this->setHeader('X-Version', $options->path('xVersion', 'Phalcon Framework ' . \Phalcon\Version::get()));

        $this->setHeader('X-Runtime', zend_version());

        $this->setHeader('X-Revision', PHP_VERSION_ID);

        $this->setHeader('X-Robots-Tag', $options->path('robotsTag', 'noindex, noarchive, nosnippet, nofollow'));

        $this->data = new Config([]);
    }

    /**
     * Sets to disable view and return different response type. By default error is set to TRUE
     * 
     * @param bool $noflash set to true for non flash style
     * 
     * @return ResponsePlugin
     */
    public function set($noflash = false) {

        $this->is_response = true;

        $this->noflash = $noflash === false ? false : true;

        $this->setError();

        return $this;
    }

    /**
     * Checks whenever view is disabled
     */
    public function isResponseSet() {
        return $this->is_response;
    }

    /**
     * Sets into header status code and message
     */
    public function setHeaderStatusCode($status_code = false) {



        return $this;
    }

    /**
     * Set response style.
     * 
     * @param string $style success, error, warning, notice
     */
    private function setStyle($style = 'success') {
        $this->style = $style;

        return $this;
    }

    /**
     * @return string Response style, default is success
     */
    public function getStyle() {

        if (empty($this->style)) {
            $this->setSuccess();
        }

        return $this->style;
    }

    /**
     * Set response data.
     * 
     * @param string $data Data to add
     */
    public function setData($data) {

        $validate = is_array($data) ? $data : [];

        $newData = new Config($validate);

        $this->data->merge($newData);

        return $this->data;
    }

    /**
     * @return \Phalcon\Config
     */
    public function getData() {

        return $this->data;
    }

    public function setJson() {

        $this->type = 'json';

        return $this;
    }

    public function setXml() {
        $this->type = 'xml';

        return $this;
    }

    public function setHtml() {
        $this->type = 'html';

        return $this;
    }

    public function setFile() {
        $this->type = 'file';
    }

    /**
     * @return string Response type, default is json
     */
    public function getType() {

        return $this->type;
    }

    /**
     * Set flash style to response
     */
    private function setNoFlash() {

        if ($this->noflash) {
            return false;
        }

        $responseAjax = new FlashStyle();

        $responseAjax->translateFlash();

        $data = $responseAjax->responseStyle($this->getStyle(), $this->getData()->toArray());

        //adds flash style data
        $this->setData($data);
    }

    /**
     * Globally Close all modals on response
     */
    public function setCloseModal($modal = '') {

        $params = [
            'targetModal' => $modal,
            'modal' => 'close'
        ];

        $this->setData(['modal' => 'close']);

        $this->setCallback('closeModal', $params);

        return $this;
    }

    /**
     * Dynamically open modal 
     * 
     * @param array $params [targetModal, modalContent]
     */
    public function setOpenModal($params) {

        $this->setCallback('openModal', $params);

        return $this;
    }

    /**
     * Append Content to specific element with append-contents attribute
     * 
     * @param string $append_content_value append-contents attribute value
     * @param string $content
     */
    public function setAppendContent($append_content_value, $content) {

        $params = [
            'target' => $append_content_value,
            'content' => $content
        ];

        $this->setCallback('appendContent', $params);

        return $this;
    }

    /**
     * Dynamically remove DIV block
     * 
     * @param string $blockIdClass
     */
    public function setRemoveBlock($blockIdClass = '') {

        $this->setCallback('removeBlocks', $blockIdClass);

        return $this;
    }

    /**
     * Sets callback function name to Dynamically render html contents to specific html element with dynamic-block attribute
     */
    public function setDynamicBlock($params) {

        $this->setCallback('dynamicBlock', $params);

        return $this;
    }

    /**
     * Sets callback function name to Dynamically change the FORM POST url address
     * 
     * @param array $params [form => (data-dynamic-form-url) value, url => string]
     */
    public function setFormUrl($params) {

        $this->setCallback('dynamicFormUrl', $params);

        return $this;
    }

    /**
     * Sets Dynamically contents to render into html element with data-dynamic-content attribute
     */
    public function setDynamicContent($block, $content = '') {

        $this->setData([
            'dynamicContent' => [
                'block' => $block,
                'content' => $content
            ]
        ]);

        return $this;
    }

    /**
     * Refresh data table on response
     * 
     * @param string $table_class_id
     * 
     */
    public function setRefreshTable($table_class_id = false) {

        $this->setCallback('dataTableReload', [
            'table' => $table_class_id
        ]);

        $this->setData(['tableRefresh' => true]);

        return $this;
    }

    /**
     * Reset web-form on response
     */
    public function setResetForm() {

        $this->setData(['form' => 'reset']);

        return $this;
    }

    /**
     * Refresh page on response
     */
    public function setReloadPage() {

        $this->setData(['reload' => true]);

        return $this;
    }

    /**
     * Refresh redirect on response
     * @param string $url
     */
    public function setRedirectUrl($url = '') {

        if (empty($url)) {
            return $this;
        }

        $this->setData(['redirect' => $url]);
    }

    /**
     * 
     */
    public function setSuccess() {

        $this->setData(['success' => true]);

        $this->setData(['error' => false]);

        $this->setStyle('success');

        return $this;
    }

    /**
     * Sets error warning on response
     */
    public function setWarning() {

        $this->setData(['warning' => true]);

        $this->setData(['error' => false]);

        $this->setStyle('warning');

        return $this;
    }

    /**
     * Sets error warning on response
     */
    public function setInfo() {

        $this->setData(['info' => true]);

        $this->setData(['error' => false]);

        $this->setStyle('info');

        return $this;
    }

    /**
     * 
     * @param array $jsfunc JavaScript function to run
     * @param array $params JavaScript function parameters
     * 
     * @return \PMP\Core\Plugins\ViewToAjax
     */
    public function setCallback($jsfunc = false, $params = false) {

        $call = [
            !empty($jsfunc) ? $jsfunc : false,
            !empty($params) ? $params : false
        ];


        $this->_callbacks[] = array_filter($call);

        return $this;
    }

    /**
     * Gets the JS function and parameters where to execute in FE
     * 
     * @return array
     */
    public function getCallbacks() {

        return $this->_callbacks;
    }

    /**
     * Sets error style on response
     */
    public function setError() {

        $this->setData(['error' => true]);

        $this->setData(['success' => false]);

        $this->setStyle('error');

        return $this;
    }

    /**
     * Sets file deletion after response 
     */
    public function setDeleteFile() {

        $this->setData(['fileDelete' => true]);

        return $this;
    }

    /**
     * @return bool
     */
    public function isFlash() {
        return $this->flash;
    }

    /**
     * Enable JSON response.
     */
    private function setJsonResponse() {

        $this->setContentType('application/json', 'UTF-8');

        $this->setJsonContent($this->getData()->toArray(), JSON_PRETTY_PRINT|JSON_BIGINT_AS_STRING|JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);

        $this->setContentLength(strlen($this->getContent()));

        $this->send();
    }

    /**
     * Enable the XML response.
     */
    private function setXMLResponse() {

        $this->setContentType('text/xml', 'UTF-8');

        $generateXml = Array2XML::createXML('response', $this->getData()->toArray());

        $xml = $generateXml->saveXML();

        $this->setContent($xml);

        $this->setContentLength(strlen($this->getContent()));

        $this->send();
    }

    /**
     * Enable the HTML response.
     */
    private function setHTMLResponse() {

        $this->setContentType('text/html', 'UTF-8');

        $this->setContent($this->getData()->path('message'));

        $this->setContentLength(strlen($this->getContent()));

        $this->send();
    }

    /**
     * 
     */
    private function setFileToSendResponse() {

        $file = $this->getData()->path('file');

        if (empty($file) || !file_exists($file)) {

            die('file not exists');
        }

        $filesize = filesize($file);

        $filetype = mime_content_type($file);

        $filename = basename($file);

        $this->setContentType($filetype);

        $this->setContentLength($filesize);

        $this->setHeader('Content-Description', 'File Transfer');

        $this->setHeader('Content-Disposition', 'attachment;filename="' . $filename . '"');

        $this->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0, max-age=0');

        $this->setHeader('Pragma', 'public');

        $this->setHeader('Last-Modified', \gmdate('D, d M Y H:i:s') . ' GMT');

        $this->setFileToSend($file, $filename);

        $this->send();

        //delete file after download
        if (!empty($this->getData()->path('fileDelete'))) {
            FileSystem::deleteFile($file);
        }
    }

    /**
     * Send the response
     * 
     */
    public function sendResponse() {

        //disable view
        $this->getDI()->getShared('view')->disable();

        $returned = $this->getDI()->getShared('dispatcher')->getReturnedValue();

        $returnParams = is_array($returned) ? $returned : [];

        //because default response is error, checks if there is success true from function return 
        !empty($returnParams['success']) ? $this->setSuccess() : false;

        $this->setNoFlash();

        $returnParams['callbacks'] = $this->getCallbacks();

        //set data from dispatcher
        $this->setData($returnParams);

        switch ($this->getType()) {
            case 'xml':
                $this->setXMLResponse();
                break;
            case 'html':
                $this->setHTMLResponse();
                break;
            case 'file':
                $this->setFileToSendResponse();
                break;
            case 'json':
                $this->setJsonResponse();
                break;
            default:
                $this->setJsonResponse();
                break;
        }

        die();
    }

}
