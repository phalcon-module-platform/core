<?php

namespace PMP\Core\Plugins\Elements;

use PMP\Core\Plugins\Elements\Button;
use PMP\Core\Plugins\Elements\Input;
use PMP\Core\Plugins\Elements\Form;
use PMP\Core\Plugins\Elements\Accordion;

class ElementsPlugin {

    /**
     * @var \Phalcon\Config
     */
    private $elements_config;

    public function setConfig($config) {

        $this->elements_config = $config;
    }

    /**
     * Render specific element
     * 
     * @param array $type ['element':'name']
     * @param array $params Element parameters
     */
    public function render($type = [], $params = []) {

        $get_method = array_keys($type);

        $get_type = array_values($type);

        $method_name = !empty($get_method[0]) ? $get_method[0] : false;

        return !method_exists(get_class(), $method_name) ?
                false :
                $this->{$method_name}(!empty($get_type[0]) ? $get_type[0] : null, $params);
    }

    public function input($name = false, $params = [], $checked = false) {

        $input = Input::getInstance($this->elements_config->path('inputs'));

        return method_exists($input, $name) ?
                $input->{$name}($params, $checked) :
                $input->field($params);
    }

    /**
     * Generate Button
     * 
     * @param string $name available names : button, loading, autosave
     * @param type $params Button options : [ data = [], text = '', class = '', style = '', tooltip = [], popover = [] ]
     * 
     * @return string HTML
     */
    public function button($name = 'button', $params = []) {

        $button = Button::getInstance($this->elements_config->path('buttons'));

        return method_exists($button, $name) ?
                $button->{$name}($params) :
                $button->button($params);
    }

    public function form($name = false, $params = []) {

        $form = Form::getInstance($this->elements_config->path('forms'));

        return method_exists($form, $name) ?
                $form->{$name}($params) :
                $form->start($params);
    }

    public function accordion($name = false, $params = []) {

        $accord = Accordion::getInstance($this->elements_config->path('accordion'));

        return (method_exists($accord, $name)) ?
                $accord->{$name}($params) :
                $accord->start($params);
    }

    public function select($name = false, $params = []) {

        $select = Select::getInstance($this->elements_config->path('select'));

        return method_exists($select, $name) ?
                $select->{$name}($params) :
                $select->dropdown($params);
    }

}