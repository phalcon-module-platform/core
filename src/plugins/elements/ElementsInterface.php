<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace PMP\Core\Plugins\Elements;

interface ElementsInterface {

    public function setStyle();

    public function getStyle();
}
