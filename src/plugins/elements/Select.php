<?php

namespace PMP\Core\Plugins\Elements;

use Phalcon\Config;
use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Select {

    /**
     * @var $this
     */
    private static $instance = null;

    /**
     * @var \Phalcon\Config
     */
    public $styles;

    public function __construct($styles = false) {

        $this->styles = $styles instanceof \Phalcon\Config ? $styles : new Config([]);

        self::$instance = $this;
    }

    /**
     * Gets the instance of class
     * 
     * @return $this
     */
    public static function getInstance($styles) {

        if (self::$instance == null) {

            new self($styles);
        }

        return self::$instance;
    }

    /**
     * 
     */
    public function dropdown($options) {

        $records = [];

        if (isset($options['records'])) {

            $records = $options['records'];

            unset($options['records']);
        }

        switch (gettype($records)) {
            case 'string':
                $records = $this->fromJson($records, $options);
                break;
            case 'array':
                $records = $this->fromArray($records, $options);
                break;
            case 'object':
                $records = $this->fromObject($records, $options);
                break;
            default:
                $records = [];
                break;
        }

        $opt = new Config($options);

        $class = [
            $this->styles->path('class'),
            $opt->path('class', false),
        ];

        $html = '';

        if (!empty($opt->path('label'))) {

            $html .= Tag::tagHtml('label', [
                        'class' => 'active'
            ]);

            $html .= $opt->path('label');

            $html .= Tag::tagHtmlClose('label');
        }

        $attributes = [
            $opt->path('id', AttributesManager::randomIdName('id')),
            'name' => $opt->path('name', AttributesManager::randomIdName('name')),
            'class' => implode(' ', array_filter($class)),
            'value' => $opt->path('value', ''),
            'useEmpty' => true,
            'emptyText' => !empty($options['placeholder']) ? $options['placeholder'] : $this->styles->path('placeholder'),
            'emptyValue' => $opt->path('emptyValue', false)
        ];

        if (isset($options['multiple']) && $options['multiple'] == true) {
            $attributes['multiple'] = true;
        }

        if (isset($options['useEmpty']) && $options['useEmpty'] == false) {

            unset($attributes['useEmpty']);

            unset($attributes['emptyText']);

            unset($attributes['emptyValue']);
        }

        if (isset($options['searchable'])) {

            $attributes['searchable'] = !empty($options['searchable']) ?
                    $options['searchable'] :
                    $this->styles->path('searchable-text');
        }

        if (!empty($opt->path('using'))) {
            $attributes['using'] = $options['using'];
        }

        $resolveAttr = !empty($options['data']) ? $options['data'] : [];

        $attr = array_merge($attributes, AttributesManager::resolveDataAttributes($resolveAttr));

        $html .= is_array($records) ?
                Tag::selectStatic($attr, $records) :
                Tag::select($attr, $records);

        return $html;
    }

    private function fromArray($records, $options) {

        //checks if values are array
        $values = array_values($records);

        $keys = array_keys($records);

        $keysAskeysValues = !empty($values[0]) && is_string($keys[0]) ? true : false;

        //first check if associative array and keys must not be values
        if (!isset($records[0]) && $keysAskeysValues === false) {
            return $records;
        }

        $newRecords = [];

        //if values are array, build the array from keys only
        if ($keysAskeysValues) {

            foreach ($keys as $value) {
                $newRecords[$value] = $value;
            }

            return $newRecords;
        }

        $using = !empty($options['using']) ? $options['using'] : false;

        //get first two keys from first member of array
        if (empty($options['using'])) {

            $first = $records[0];

            $keys = array_keys($first);

            $using = [$keys[0], $keys[1]];
        }

        //extract exact records based on using and build associative array
        foreach ($records as $value) {

            $key = $using[0];

            $text = $using[1];

            //if useing options not match to keys in array member
            if (!array_key_exists($key, $value) || !array_key_exists($text, $value)) {
                continue;
            }

            $newRecords[$value[$key]] = $value[$text];
        }

        return $newRecords;
    }

    private function fromObject($records, $options) {

        return $records instanceof \Phalcon\Config ?
                $this->fromArray($records->toArray(), $options) :
                $records;
    }

    private function fromJson($records, $options) {

        $decode = json_decode($records, true);

        $rec = !empty($decode) && is_array($decode) ? $decode : [];

        return $this->fromArray($rec, $options);
    }

}
