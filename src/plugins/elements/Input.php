<?php

namespace PMP\Core\Plugins\Elements;

use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Input {

    /**
     * @var $this
     */
    private static $instance = null;

    /**
     * @var string Default form group class name
     */
    private $formGroupClass = 'input-field';

    /**
     * @var string Default form input class name
     */
    private $formInputClass = 'form-control';

    /**
     * @var string Default checkbox tooltip class name
     */
    private $iconToolTip = 'fa fa-info mr-2 text-info material-tooltip-main tooltipped';

    /**
     * @var \Phalcon\Config
     */
    public $styles;

    public function __construct($styles = false) {

        $this->styles = $styles instanceof \Phalcon\Config ? $styles : new Config([]);

        self::$instance = $this;
    }

    /**
     * Gets the instance of class
     * 
     * @return $this
     */
    public static function getInstance($styles) {

        if (self::$instance == null) {

            new self($styles);
        }

        return self::$instance;
    }

    public function material($options) {

        $inputAttributes = $this->inputAttributes($options);

        $html = '';

        $html .= Tag::tagHtml('div', ['class' => 'md-form']);

        switch ($inputAttributes['type']) {
            case 'text':
                $html .= Tag::textField($inputAttributes);
                break;
            case 'textarea':
                $html .= Tag::textArea($inputAttributes);
                break;
            case 'hidden':
                $html .= Tag::hiddenField($inputAttributes);
                break;
            case 'number':
                $html .= Tag::numericField($inputAttributes);
                break;
            case 'tel':
                $html .= Tag::telField($inputAttributes);
                break;
            case 'email':
                $html .= Tag::emailField($inputAttributes);
                break;
            case 'password':
                $html .= Tag::passwordField($inputAttributes);
                break;
            default:
                $html .= Tag::textField($inputAttributes);
                break;
        }

        $html .= !empty($options['title']) ? Tag::tagHtml('label') : '';

        if (!empty($options['tooltip'])) {

            $ioptions = [
                'class' => $this->iconToolTip
            ];

            $iopt = array_merge($ioptions, AttributesManager::resolveDataAttributes($options));

            $html .= Tag::tagHtml('i', $iopt);

            $html .= Tag::tagHtmlClose('i');
        }

        $html .= !empty($options['title']) ? $options['title'] : '';

        $html .= !empty($options['title']) ? Tag::tagHtmlClose('label') : '';

        $html .= Tag::tagHtmlClose('div');

        return $html;
    }

    /**
     * Generate feedback icon after the input element
     */
    public function inputFeedbackIcon($options) {

        $html = Tag::tagHtml('div', ['class' => 'form-control-feedback']);

        $color = !empty($options['color']) ? ' text-' . $options['color'] . '' : '';

        $html .= Tag::tagHtml('i', ['class' => $options['icon'] . $color]);

        $html .= Tag::tagHtmlClose('i');

        $html .= Tag::tagHtmlClose('div');

        return $html;
    }

    public function materialCheckbox($options, $checked) {

        $inputAttributes = $this->checkboxAttributes($options);

        $labelOptions = [
            'class' => 'form-check-label ',
            'for' => $inputAttributes['id']
        ];

        $labelTT = AttributesManager::resolveTooltipPopover($options);

        if (!empty($options['tooltip'])) {

            $labelOptions['class'] .= 'material-tooltip-main tooltipped';
        }

        $labelOpt = array_merge($labelOptions, AttributesManager::resolveDataAttributes($labelTT));

        $html = '';

        //checkbox logic
        $html .= Tag::hiddenField([
                    'id' => null,
                    'name' => $options['attr']['name'],
                    'value' => $options['value']['default']
        ]);

        $checkboxOptions = [
            'type' => 'checkbox',
            'id' => $inputAttributes['id'],
            'value' => $options['value']['checked'],
            'class' => (!empty($options['class'])) ? $options['class'] . ' form-check-input' : 'form-check-input '
        ];

        if (empty($options['data']['on-text'])) {
            $checkboxOptions['data-on-text'] = 'On';
            $checkboxOptions['data-off-text'] = 'Off';
        }

        if ($options['value']['checked'] == $checked) {

            $checkboxOptions['checked'] = 'checked';
        }

        $html .= Tag::tagHtml('input', array_merge($inputAttributes, $checkboxOptions), true);

        $html .= Tag::tagHtml('label', $labelOpt);

        $html .= $options['label'];

        $html .= Tag::tagHtmlClose('label');

        return $html;
    }

    /**
     * resolve all input checkbox attributes
     */
    private function checkboxAttributes($options) {

        $attr = $options['attr'];
        // render element without attribute id
        if (empty($attr['id'])) {

            $attr['id'] = 'mt-checkbox-' . rand(999, 99999); //preg_replace('/[^a-zA-Z0-9]/', '', $attr['name']);
        }

        $attr['class'] = (!empty($attr['class'])) ? $attr['class'] . ' switch' : 'switch';

        if (isset($options['data'])) {

            return array_merge($attr, AttributesManager::resolveDataAttributes($options['data']));
        }

        return $attr;
    }

    /**
     * resolve all input attributes
     */
    private function inputAttributes($options) {

        $attr = $options['attr'];

        if (empty($attr['id'])) {

            $attr['id'] = null; //preg_replace('/[^a-zA-Z0-9]/', '', $attr['name']);
        }

        $attr['class'] = (!empty($attr['class'])) ? $attr['class'] . ' ' : '';

        $attr['class'] .= $this->formInputClass;

        if (isset($options['data'])) {

            return array_merge($attr, AttributesManager::resolveDataAttributes($options['data']));
        }

        return $attr;
    }

    private function group($options) {

        $class = $this->formGroupClass;

        if (!empty($options['icon'])) {
            $class .= ' has-feedback has-feedback-' . $options['icon']['pos'];
        }
        if (!empty($options['icon']['class'])) {
            $class .= ' ' . $options['icon']['class'];
        }
        if (!empty($options['class'])) {
            $class .= ' ' . $options['class'];
        }
        $formGroupAttr = [
            'class' => $class
        ];

        $html = Tag::tagHtml('div', $formGroupAttr);

        if (!empty($options['span'])) {
            $html .= Tag::tagHtml('span', ['id' => $options['span']]);
            $html .= Tag::tagHtmlClose('span');
        }

        return $html;
    }

}
