<?php

namespace PMP\Core\Plugins\Elements;

use Phalcon\Config;
use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Form {

    /**
     * @var $this
     */
    private static $instance = null;

    /**
     * @var \Phalcon\Config
     */
    public $styles;

    public function __construct($styles = false) {

        $this->styles = $styles instanceof \Phalcon\Config ? $styles : new Config([]);

        self::$instance = $this;
    }

    /**
     * Gets the instance of class
     * 
     * @return $this
     */
    public static function getInstance($styles) {

        if (self::$instance == null) {

            new self($styles);
        }

        return self::$instance;
    }

    public function start($parameters) {

        $opt = new Config($parameters);

        $class = [
            $this->styles->path('class'),
            $opt->path('class', false),
        ];

        $newData = new Config(['data' => [
                'timeout' => $opt->path('data.timeout', $this->styles->path('timeout')),
                'notify' => $opt->path('data.notify', $this->styles->path('notify')),
                'url' => $opt->path('data.url', '/')
        ]]);

        $data = $opt->merge($newData);

        $options = [
            'autocomplete' => $opt->path('autocomplete', $this->styles->path('autocomplete')),
            'class' => implode(' ', $class),
            'method' => $opt->path('method', $this->styles->path('method'))
        ];

        $resolved = AttributesManager::resolveDataAttributes($data->path('data')->toArray());

        $mergedoptions = array_merge($options, $resolved);

        return Tag::form($mergedoptions);
    }

    public function end() {

        return Tag::endForm();
    }

}
