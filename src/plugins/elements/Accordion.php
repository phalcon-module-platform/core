<?php

namespace PMP\Core\Plugins\Elements;

use Phalcon\Config;
use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Accordion {

    /**
     * @var $this
     */
    private static $instance = null;

    /**
     * @var \Phalcon\Config
     */
    public $styles;

    /**
     * @var \Phalcon\Config [parent, heading]
     */
    public $accordion_params;

    public function __construct($styles = false) {

        $this->styles = $styles instanceof \Phalcon\Config ? $styles : new Config([]);

        self::$instance = $this;
    }

    /**
     * Gets the instance of class
     * 
     * @return $this
     */
    public static function getInstance($styles) {

        if (self::$instance == null) {

            new self($styles);
        }

        return self::$instance;
    }

    public function start($parameters) {

        $opt = new Config($parameters);

        $this->accordion_params = $opt->path('params', new Config([]));

        $class = [
            $this->styles->path('styles.accordion-class'),
            $opt->path('class', false),
        ];

        $newData = new Config(['data' => []]);

        $data = $opt->merge($newData);

        $options = [
            'class' => implode(' ', array_filter($class))
        ];

        $resolved = AttributesManager::resolveDataAttributes($data->path('data')->toArray());

        $mergedoptions = array_merge($options, $resolved);

        $html = Tag::tagHtml('div', $mergedoptions);

        return $html;
    }

    public function header($parameters) {

        $opt = new Config($parameters);

        $headerclass = [
            $this->styles->path('styles.header-class'),
            $opt->path('class', false),
        ];

        $toggleclass = [
            $this->styles->path('styles.toggle-class'),
            $opt->path('params.class', false),
        ];

        $params = [
            'role' => 'tab',
            'class' => implode(' ', array_filter($headerclass)),
            'id' => 'heading-' . $this->accordion_params->path('heading-id')
        ];

        $html = Tag::tagHtml('div', $params);

        $aparams = [
            'class' => implode(' ', array_filter($toggleclass)),
            'data-toggle' => 'collapse',
            'href' => '#collapse-' . $this->accordion_params->path('heading-id'),
            'aria-expanded' => 'true',
            'aria-controls' => 'collapse-' . $this->accordion_params->path('heading-id')
        ];

        $html .= Tag::tagHtml('a', $aparams);
        $html .= trim($opt->path('title'));
        $html .= Tag::tagHtmlClose('a');

        $html .= Tag::tagHtmlClose('div');

        return $html;
    }

    public function bodyStart($parameters) {

        $opt = new Config($parameters);

        $collapsed = $opt->path('show', false) ? ' show' : '';

        $divParams = [
            'id' => 'collapse-' . $this->accordion_params->path('heading-id'),
            'class' => 'collapsed collapse' . $collapsed,
            'role' => 'tabpanel',
            'aria-labelledby' => 'heading-' . $this->accordion_params->path('heading-id'),
            'data-parent' => '#' . $this->accordion_params->path('parent')
        ];

        $bodyclass = [
            $this->styles->path('styles.body-class'),
            $opt->path('class', false),
        ];

        $html = Tag::tagHtml('div', $divParams);

        $html .= Tag::tagHtml('div', [
                    'class' => implode(' ', array_filter($bodyclass))
        ]);

        return $html;
    }

    public function bodyEnd() {

        $html = Tag::tagHtmlClose('div');

        $html .= Tag::tagHtmlClose('div');

        return $html;
    }

    public function end() {

        //reset parms for next accordion
        $this->accordion_params = new Config([]);

        return Tag::tagHtmlClose('div');
    }

}
