<?php

namespace PMP\Core\Plugins\Elements;

use Phalcon\Tag;
use PMP\Core\Library\AttributesManager;

class Button {

    /**
     * @var $this
     */
    private static $instance = null;

    /**
     * @var string Default button class if style is not set
     */
    private $styleDefault = 'btn text-white info-color-dark';

    /**
     * @var string Default button type if type is not set
     */
    private $typeDefault = 'btn btn-outline-primary text-info';

    /**
     * @var string Default button type if type is not set
     */
    private $btnClass = 'btn';

    /**
     * @var string Default auto-save button class ['autoform' class submitting form with java script]
     */
    private $classAutosave = 'ladda-button button-ladda-spinner autoform';

    /**
     * @var atring Default loading button class
     */
    private $classLoading = 'ladda-button button-ladda-spinner';

    /**
     * 
     */
    protected static $spinner = '';

    /**
     * @var atring Default button text
     */
    private $defaultText = 'Button';

    /**
     * @var \Phalcon\Config
     */
    public $styles;

    public function __construct($styles = false) {

        $this->styles = $styles instanceof \Phalcon\Config ? $styles : new Config([]);

        self::$instance = $this;
    }

    /**
     * Gets the instance of class
     * 
     * @return $this
     */
    public static function getInstance($styles) {

        if (self::$instance == null) {

            new self($styles);
        }

        return self::$instance;
    }

    /**
     * Auto-save Form button
     * 
     * @param array $options Button options
     * @return text HTML button element
     * 
     */
    public function autosave($options = []) {

        $class = [
            $this->resolveClass($options),
            $this->classAutosave
        ];

        $attributes = [
            'class' => implode(' ', $class),
            'type' => 'button'
        ];

        if (empty($options['data']['style'])) {
            $options['data']['style'] = 'zoom-in';
        }

        if (empty($options['data']['spinner-color'])) {
            $options['data']['spinner-color'] = '#000000';
        }

        if (!empty($options['data'])) {
            $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
        }

        $text = $this->getButtonText($attributes, $options);

        $button = Tag::tagHtml('button', $attributes) . $text . Tag::tagHtmlClose('button');

        return $button;
    }

    public function button($options = []) {

        $class = [
            $this->resolveClass($options)
        ];

        $attributes = [
            'class' => implode(' ', $class),
            'type' => 'button',
            'title' => !empty($options['title']) ? $options['title'] : ''
        ];

        if (!empty($options['id'])) {

            $attributes['id'] = $options['id'];
        }

        $options['data'] = AttributesManager::resolveTooltipPopover($options);

        if (!empty($options['data'])) {

            $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
        }

        $text = $this->getButtonText($attributes, $options);

        $button = Tag::tagHtml('button', $attributes) . $text . Tag::tagHtmlClose('button');

        return $button;
    }

    /**
     * Button with loading spinner
     */
    public function loading($options = []) {

        $class = [
            $this->classLoading,
            $this->resolveClass($options)
        ];

        $attributes = [
            'class' => implode(' ', $class),
            'type' => 'button'
        ];

        if (!empty($options['id'])) {
            $attributes['id'] = $options['id'];
        }

        $options['data'] = AttributesManager::resolveTooltipPopover($options);

        if (empty($options['data']['style'])) {
            $options['data']['style'] = 'zoom-in';
        }


        if (empty($options['data']['spinner-color'])) {

            $options['data']['spinner-color'] = '#000000';
        }

        if (!empty($options['data'])) {

            $attributes = array_merge($attributes, AttributesManager::resolveDataAttributes($options['data']));
        }

        $text = $this->getButtonText($attributes, $options);

        $button = Tag::tagHtml('button', $attributes) . $text . Tag::tagHtmlClose('button');

        return $button;
    }

    /**
     * Resolve button class
     * 
     * @param array $options Button options
     */
    private function resolveClass($options = []) {

        $class = [
            $this->btnClass,
            !empty($options['tooltip']) ? 'material-tooltip-main tooltipped' : false,
            $this->styles->path('effects', false),
        ];

        if (!empty($options['style'])) {

            $class[] = $this->styles->path('styles.' . $options['style'], $this->styleDefault);
        } else if (!empty($options['type'])) {

            $class[] = $this->styles->path('types.' . $options['type'], $this->typeDefault);
        } else {

            $class[] = $this->styleDefault;
        }

        $class[] = !empty($options['class']) ? $options['class'] : false;

        return implode(' ', array_filter($class));
    }

    /**
     * Search for button text in attributes or in options
     * 
     * @param array $options
     */
    private function getButtonText($attributes = [], $options = []) {

        if (empty($options['text'])) {

            $options['text'] = $this->defaultText;
        }

        return !empty($attributes['data-text']) ? $attributes['data-text'] : $options['text'];
    }

}
