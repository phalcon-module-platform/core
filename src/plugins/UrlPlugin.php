<?php

namespace PMP\Core\Plugins;

use Phalcon\Mvc\Url;

class UrlPlugin extends Url {

    private $security;

    public function __construct() {
        
    }

    public function getFor($name, $queryParams = []) {

        $re = $this->get(['for' => $name], $queryParams);

        return $re;
    }

    public function getCrypted($name, $queryParams = []) {

        /* @var $hash \Phalcon\Crypt */
        $hash = $this->getDI()->getShared('crypt');

        $crypted = $hash->encryptBase64(http_build_query($queryParams));

        $re = $this->get(['for' => $name], ['token' => $crypted]);

        return $re;
    }

}
