<?php

namespace PMP\Core\Plugins;

use Phalcon\Config;
use Phalcon\Cache\Frontend\Data;
use Phalcon\Cache\Backend\Factory as BFactory;
use PMP\Core\Library\FileSystem;

class Cache {

    /**
     * @var \Phalcon\Config
     */
    private $models;

    /**
     * @var \Phalcon\Cache\BackendInterface
     */
    protected $_cache;

    /**
     * @var string
     */
    protected $file_extension = '.cache';

    /**
     * @param \Phalcon\Config $config [cacheDir, prefix, lifetime, adapter, models=[user,lifetime,cache,key] ]
     */
    public function setConfig($config = []) {

        $cacheconfig = $config instanceof \Phalcon\Config || $config instanceof \Phalcon\Config\Adapter\Json ?
                $config :
                new Config($config);

        if (empty($cacheconfig->path('models'))) {

            die('You must configure the cache options to use the Cache plugin');
        }

        if ((!\extension_loaded('apc') || !\ini_get('apc.enabled')) && $cacheconfig->path('adapter') == 'apc') {

            die('Your server not support APC cache. Change the cache configuration');
        }

        $this->models = $cacheconfig->path('models');

        $cacheOptions = [
            'cacheDir' => FileSystem::makeDir($cacheconfig->path('cacheDir')),
            'prefix' => $cacheconfig->path('prefix'),
            'frontend' => new Data(['lifetime' => $cacheconfig->path('lifetime')]),
            'adapter' => $cacheconfig->path('adapter')
        ];
        
        $this->_cache = BFactory::load($cacheOptions);
        
    }

    /**
     * Stores cached content into the APC backend and stops the frontend
     * if Model is allowed to use cache
     * 
     * @param string $model Model, Plugin or Class Name Space
     * @param string $content
     * @param string|int $keyName
     * 
     * @return bool
     * @throws Exception
     */
    public function setCache($model, $content = null, $keyName = null) {


        if (!$this->allowed($model)) {

            return false;
        }

        $key = !empty($keyName) ? $keyName . $this->getCacheFileExt() : $this->getCacheKey($model);

        try {

            $this->_cache->save($key, $content, $this->models->path($model . '.lifetime'));
            
        } catch (\ErrorException $exc) {

            die($exc->getMessage());
        }

        return;
    }

    /**
     * Gets the cached model
     */
    public function getCache($key) {

        return $this->_cache->get($key . $this->getCacheFileExt());
    }

    /**
     * Checks if cache exists and it hasn't expired
     * Check if cache is allowed for model
     * 
     * @param string $model Model, Plugin or Class Name Space
     * @param string|int $keyName
     * @param int $lifetime
     * 
     * @return bool True if model is allowed to use cache and is cached contents already exists 
     */
    public function isCacheable($model, $keyName = null, $lifetime = null) {

        if (!$this->allowed($model)) {

            return false;
        }

        $key = !empty($keyName) ? $keyName . $this->getCacheFileExt() : $this->getCacheKey($model);

        $time = !empty($lifetime) ? $lifetime : $this->models->path($model . '.lifetime');

        return $this->_cache->exists($key, $time);
    }

    /**
     *
     * Check whenever APC is enabled on the server and class is allowed to use cache
     */
    public function allowed($classname) {

        return $this->models->path($classname . '.cache') ? true : false;
    }

    /**
     * Clear cache by Model and key name
     * 
     * @param string $model Model, Plugin or Class Name Space
     * @param string $keyName Key name
     * @param int $lifetime
     */
    public function clearCache($model, $keyName = null, $lifetime = null) {

        if (empty($model)) {

            return false;
        }

        $key = !empty($keyName) ? $keyName . $this->getCacheFileExt() : $this->getCacheKey($model);

        $time = !empty($lifetime) ? $lifetime : $this->models->path($model . '.lifetime');

        if ($this->_cache->exists($key)) {

            $this->_cache->delete($key);
        }
    }

    /**
     * @return string cache file extension
     */
    public function getCacheFileExt() {

        return $this->file_extension;
    }

    /**
     * @return string Model Cache key name from models configuration
     */
    public function getCacheKey($model) {

        return $this->models->path($model . '.key') . $this->getCacheFileExt();
    }

    /**
     * @return \Phalcon\Cache\Backend\File
     */
    public function getBackendFile() {


        $this->_cache;
    }

}
