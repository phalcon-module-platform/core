<?php

namespace PMP\Core\Plugins\Uploader;

use PMP\Core\Plugins\Uploader\Helpers\Message;
use PMP\Core\Plugins\Uploader\Helpers\Format;

/**
 * Validator class
 *
 * @package   Uploader
 * @since     PHP >=5.4
 * @version   1.0
 * @author    Stanislav WEB | Lugansk <stanisov@gmail.com>
 * @copyright Stanislav WEB
 */
class Validator {

   /**
    * Error message container
    * @var array $rules
    */
   public $errors = [];

   /**
    * Check minimum file size
    *
    * @param \Phalcon\Http\Request\File $file
    * @param $value
    * @return bool
    */
   public function checkMinsize(\Phalcon\Http\Request\File $file, $value) {

      if (is_array($value) === true) {
         $value = $value[key($value)];
      }

      if ($file->getSize() < (int) $value) {

         $this->errors[] = sprintf(Message::get('INVALID_MIN_SIZE'), $file->getName(), Format::bytes($value));
         
         return false;
      }

      return true;

   }

   /**
    * Check maximum file size
    *
    * @param \Phalcon\Http\Request\File $file
    * @param mixed $value
    * @return bool
    */
   public function checkMaxsize(\Phalcon\Http\Request\File $file, $value) {

      if (is_array($value) === true) {
         
         $value = $value[key($value)];
      }

      if ($file->getSize() > (int) $value) {

         $this->errors[] = sprintf(Message::get('INVALID_MAX_SIZE'), $file->getName(), Format::bytes($value));
         
         return false;
      }

      return true;

   }

   /**
    * Check file allowed extensions
    *
    * @param \Phalcon\Http\Request\File $file
    * @param mixed $value
    * @return bool
    */
   public function checkExtensions(\Phalcon\Http\Request\File $file, $value) {
      //conversion to the desired format

      if (is_array($value) === false) {
         $value = [$value];
      }

      if (in_array(strtolower($file->getExtension()), $value) === false) {

         $this->errors[] = sprintf(Message::get('INVALID_EXTENSION'), $file->getName(), implode(',', $value));

         return false;
      }

      return true;

   }

   /**
    * Check file allowed extensions
    *
    * @param \Phalcon\Http\Request\File $file
    * @param mixed $value
    * @return bool
    */
   public function checkMimes(\Phalcon\Http\Request\File $file, $value) {

      if (is_array($value) === false) {
         $value = [$value];
      }

      if (in_array($file->getRealType(), $value) === false) {

         $this->errors[] = sprintf(Message::get('INVALID_MIME_TYPES'), $file->getName(), implode(',', $value));

         return false;
      }

      return true;

   }

   /**
    * Check upload directory
    *
    * @param null|\Phalcon\Http\Request\File $file
    * @param mixed $value
    * @param $value
    * @return bool
    */
   public function checkDirectory(\Phalcon\Http\Request\File $file = null, $value) {

      if (is_array($value) === true) {
         $value = $value[key($value)];
      }

      if (file_exists($value) === false) {

         $this->errors[] = sprintf(Message::get('INVALID_UPLOAD_DIR'), $value);
         return false;
      }

      if (is_writable($value) === false) {

         $this->errors[] = sprintf(Message::get('INVALID_PERMISSION_DIR'), $value);
         return false;
      }

      return true;

   }

   /**
    * Create Directory if not exist
    *
    * @param null|\Phalcon\Http\Request\File $file
    * @param string $directory
    * @param int $permission
    * @version v1.4
    * @author Mahdi-Mohammadi
    * @return bool
    */
   public function checkDynamic($file = null, $directory, $permission = 0775) {
      
      if (!file_exists($directory)) {
         
         mkdir(rtrim($directory, '/') . '/', $permission, true);
         
      }

      return true;

   }

}
