<?php

namespace PMP\Core\Plugins;

use Phalcon\Flash\Session AS FlashSession;
use Phalcon\FlashInterface;

class FlashSessionPlugin extends FlashSession implements FlashInterface {


    /**
     * @var \Phalcon\Tag Description
     */
    private $tag;

    public function setConfig($options = []) {

        parent::__construct($options);

        parent::setImplicitFlush(true);

        parent::setAutomaticHtml(true);
        
        parent::setAutoescape(false);
        
        $this->tag = new \Phalcon\Tag();
        
    }

    public function success($message) {

        $newMessage = $this->setMessageInHtml($message);
        
        return parent::success($newMessage);

    }
    
    public function warning($message) {

        $newMessage = $this->setMessageInHtml($message);
        
        return parent::warning($newMessage);

    }
    
    public function notice($message) {

        $newMessage = $this->setMessageInHtml($message);
        
        return parent::notice($newMessage);

    }
    
    public function error($message) {

        $newMessage = $this->setMessageInHtml($message);
        
        return parent::error($newMessage);

    }

    public function output($remove = true) {
        
        parent::output($remove);
        
    }

    protected function setMessageInHtml($message) {
        
        $tag = $this->tag;
        
        $html = '';
        
        $html .= $message;

        $html .= $tag::tagHtml('button', [
                    'class' => 'close',
                    'type' => 'button',
                    'data-dismiss' => 'alert',
                    'aria-label' => 'Close'
        ]);

        $html .= $tag::tagHtml('span', [
                    'aria-hidden' => 'true'
        ]);

        $html .= '×';
        $html .= $tag::tagHtmlClose('span');
        $html .= $tag::tagHtmlClose('button');

        return $html;
    }

}
