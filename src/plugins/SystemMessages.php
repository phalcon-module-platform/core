<?php

namespace PMP\Core\Plugins;

use Phalcon\Config;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Models\SystemMessages AS SM;

/**
 * SystemMessages PLUGIN
 *
 * @author Dimitar Stanimirov <stanimirov.dimitar@gmail.com>
 */
class SystemMessages extends PluginInterface {

    /**
     * @var bool
     */
    private $use_db = false;

    /**
     * @var \Phalcon\Config
     */
    private $cached;

    /**
     * @var string
     */
    private $cache_key = '-system-messages';

    /**
     * @param \Phalcon\Config $config [location, domain, defaultLocale, source]
     */
    public function setConfig($config) {

        foreach ($config as $property => $value) {

            if (property_exists(get_class(), $property)) {

                $this->{$property} = $value;
            }
        }
    }

    /**
     * Gets the message from Database and translate it.
     * If cache option is available, it gets the variable from cache
     * 
     * @param string $variable_name variable name
     */
    public function get($variable_name = '') {

        if ($this->use_db) {

            /* @var $getmessage \PMP\Core\Models\SystemMessages */
            $getmessage = SM::findFirstByVariable($variable_name);

            return !empty($getmessage) ? $getmessage->getMessage() : $variable_name;
        }

        $messages = $this->getFromCache();

        $messages->path($variable_name, $variable_name);

        return $messages->path($variable_name, $variable_name);
    }

    /**
     * Volt Compiler function to get system message
     */
    public static function sysMessage($message) {

        return (new self)->get($message);
    }

    /**
     * Gets the variable replacement from cache
     * 
     * @return \Phalcon\Config Variables and replacements
     */
    private function getFromCache() {

        //checks if cache has been picked up
        if (!empty($this->cached)) {

            return $this->cached;
        }


        //continue and get from cacehe file if cache file exists
        if ($this->cache->isCacheable('system-messages', $this->cache_key)) {

            /* @var $encode string JSON STRING */
            $encode = $this->cache->getCache($this->cache_key);

            /* @var $encode array */
            $messages = json_decode($encode, true);
        } else {

            $getmessages = SM::find();

            /* @var $encode array */
            $messages = [];

            /* @var $variable \PMP\Core\Models\SystemMessages */
            foreach ($getmessages as $variable) {

                $messages[$variable->getVariable()] = $variable->getMessage();
            }

            /* @var $encode string  JSON STRING */
            $encode = json_encode($messages);

            $this->cache->setCache('system-messages', $encode, $this->cache_key);
        }

        $this->cached = is_array($messages) ? new Config($messages) : new Config([]);

        return $this->cached;
    }

}
