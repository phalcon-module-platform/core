<?php

namespace PMP\Core\Plugins;

use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use PMP\Core\Plugins\Profile\MenuMain;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Library\Constants\UserConstants;
use PMP\Core\Library\Constants;
use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config;

/**
 * 
 */
class DispatcherPlugin extends PluginInterface {

    /**
     * @var bool
     */
    protected $has_access;

    /**
     * Trigger an action before exception.If controller and action are not found, forward to Index Controller<br />
     * 
     * @param \Phalcon\Events\Event $event
     * @param \Phalcon\Mvc\Dispatcher $dispatcher
     * @param \Phalcon\Mvc\Dispatcher\Exception $exception
     */
    public function beforeException($event, $dispatcher, $exception) {

        //prevent loops as already have an exception and exception need to be dispatced to controller
        if ($dispatcher->wasForwarded()) {

            return false;
        }

        $params = [
            'namespace' => 'PMP\Core\Controllers',
            'controller' => 'error',
            'action' => 'exception',
            'params' => [$exception]
        ];

        $dispatcher->forward($params);

        return false;
    }

    /**
     * Triggered before entering in the dispatch loop. 
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatchLoop(Event $event, Dispatcher $dispatcher) {

        $actionName = lcfirst(\Phalcon\Text::camelize($dispatcher->getActionName(), "-"));

        $controllerName = lcfirst(\Phalcon\Text::camelize($dispatcher->getControllerName(), "-"));

        $moduleName = lcfirst(\Phalcon\Text::camelize($dispatcher->getModuleName(), "-"));

        //if module, controller and action exists in App , then change the namespace
        $ns = $this->systemConfig->isReusedModule($moduleName, $controllerName);

        if (!empty($ns)) {

            $dispatcher->setDefaultNamespace($ns);
        }

        //not working on most ENV
        //$dispatcher->setControllerName($controllerName);

        $dispatcher->setModuleName($moduleName);

        $dispatcher->setActionName($actionName);

        $translation = $this->translate;

        $translation->load();
    }

    /**
     * This action is executed before execute any action in the application
     *
     * @param Event $event
     * @param Dispatcher $dispatcher
     */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher) {

        //stop execute rooting if error - prevent Dispatcher has detected a cyclic routing causing stability problems
        if ($dispatcher->getControllerName() == 'error') {
            return true;
        }
        
        //stop executing route if user has no access, even if it is Guest
        if (!$this->acl->hasAccess()) {

            return $this->acl->redirect([
                        'message' => $this->systemMessage->get('REQUEST_ACCESS_DENIED')
            ]);
        }

        //If Guest, do not try to check session. 
        if ($this->acl->getRole() == UserConstants::role_guest) {

            return true;
        }

        //Checks the session and continue executing router, else redirect with error message
        return $this->checkSession() ?
                true :
                $this->acl->redirect([
                    'url' => Constants\Redirect::SE,
                    'message' => $this->systemMessage->get('SESSION_HAS_EXPIRED')
        ]);
    }

    /**
     * Automatically set response type when post
     */
    public function beforeExecuteRoute() {

        if ($this->request->isAjax()) {

            $this->response->set();
        }
    }

    public function afterExecuteRoute(Event $event, Dispatcher $dispatcher) {

        if ($this->response->isResponseSet()) {

            return $this->response->sendResponse();
        }

        //if function does not have return , render the template anyway
        if (empty($dispatcher->getReturnedValue())) {

            return $this->viewToAjax->returnToView();
        }
    }

    /**
     * @return bool TRUE if user is logged
     */
    private function checkSession() {

        if ($this->profile->isExiredSession()) {

            $this->session->destroy(true);

            return false;
        }

        //set user last visit time
        if (!$this->request->isAjax() || !$this->request->isPost()) {

            $this->user->setLastVisit(date('Y-m-d H:i:s'));

            $this->user->update();

            $this->profile->updateUserSession();
        }

        $menuFile = $this->systemConfig->getConfigResourcesDir() . '/menu.json';

        $menu = file_exists($menuFile) ?
                new ConfigJson($menuFile) :
                new Config([]);

        //make through plugin
        $mainMenu = new MenuMain();

        if ($this->user->isAdmin()) {
            
            $admin = $mainMenu->generateMenu();

            $this->view->setVar('adminMenu', $admin);
        }

        $this->view->setVar('userMenu', $menu);

        return true;
    }

}
