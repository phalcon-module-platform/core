<?php

namespace PMP\Core\Plugins;

use Phalcon\Config;
use Phalcon\Assets\Manager;
use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;
use PMP\Core\Library\FileSystem;

/**
 * 
 * 
 */
class Assets extends Manager {

    /**
     * @var string Header collection name
     */
    const HEADER_COLLECTION = 'header';

    /**
     * @var string Header collection name
     */
    const FOOTER_COLLECTION = 'footer';

    /**
     * @var string
     */
    private $public_path;

    /**
     * @var \Phalcon\Config
     */
    private $autoload;

    /**
     * @var string
     */
    private $autoload_path;

    /**
     * @var \Phalcon\Assets\Collection
     */
    private $footerCollection;

    /**
     * @var \Phalcon\Assets\Collection
     */
    private $headerCollection;

    /**
     * 
     * @var \Phalcon\Config Assets options
     * 
     */
    private $config;

    /**
     * @var \Phalcon\Config Files to load automatically
     */
    private $autoloadFiles;

    /**
     * Merge additional configuration
     * 
     * @param \Phalcon\Config $config 
     * @param array $autoload [module, controller, action]
     */
    public function setConfig(Config $config, Config $autoload = null) {

        $this->config = $config;

        $this->autoload = !empty($autoload) ? $autoload : new Config([]);
    }

    /**
     * @param string $option Option to retrieve from configuration
     * 
     * @return \Phalcon\Config Full configuration if parameter is not set
     * @return mixed specific value from configuration
     */
    public function getConfig($option = '') {

        if (empty($option)) {

            return $this->config;
        }

        return $this->config->path($option);
    }

    /**
     * @param string $path Full path to public folder
     */
    public function setPublicPath($path) {

        $this->public_path = $path;
    }

    /**
     * 
     * @return string Full path to public folder with trailing slash
     */
    public function getPublicPath() {

        if (!empty($this->public_path)) {

            return chop($this->public_path, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
        }

        $this->public_path = chop($this->getConfig('publicPath'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        return $this->public_path;
    }

    /**
     * 
     * @return string Full path
     */
    public function getAutoloadPath() {

        if (!empty($this->autoload_path)) {

            return $this->autoload_path;
        }

        $autoload = $this->getConfig('autoload');

        $path = $this->getPublicPath() . chop($autoload, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;

        FileSystem::makeDir($path);

        $this->autoload_path = $autoload;

        return $this->autoload_path;
    }

    /**
     * 
     * @return \Phalcon\Config JS and CSS collection autoloaded
     */
    public function getAutoload() {

        if (!empty($this->autoloadFiles)) {

            return $this->autoloadFiles;
        }

        $af = $this->getAutoloadPath();

        $module = $this->autoload->path('module', false);

        $controller = $this->autoload->path('controller');

        $action = $this->autoload->path('action');

        $files = [
            'js' => [
                $af . '/js/' . $module . '.js',
                $af . '/js/' . $module . '/' . $controller . '.js',
                $af . '/js/' . $module . '/' . $controller . '-' . $action . '.js',
            ],
            'css' => [
                $af . '/css/' . $module . '.css',
                $af . '/css/' . $module . '/' . $controller . '.css',
                $af . '/css/' . $module . '/' . $controller . '-' . $action . '.css',
            ]
        ];

        $this->autoloadFiles = new Config($files);

        return $this->autoloadFiles;
    }

    /**
     * Add file to footer collection
     * 
     * @param mixed $file Path to file relative to public folder or Array if has extra attributes
     * @param bool $local
     */
    public function addToFooter($file, $local = false) {

        $filename = is_array($file) && !empty($file['file']) ? $file['file'] : $file;

        if (empty($filename) || !$this->assetExists($filename)) {
            return false;
        }
        
        $attributes = is_array($file) && !empty($file['attributes']) ? $file['attributes'] : [];
        
        $this->getFooterCollection()->addJs($filename, $local, true, $attributes);
    }

    /**
     * Add file to header collection
     * 
     * @param mixed $file Path to file relative to public folder or Array if has extra attributes
     * @param bool $local
     */
    public function addToHeader($file, $local = false) {

        $filename = is_array($file) && !empty($file['file']) ? $file['file'] : $file;

        if (empty($filename) || !$this->assetExists($filename)) {
            return false;
        }

        $attributes = is_array($file) && !empty($file['attributes']) ? $file['attributes'] : [];

        $this->getHeaderCollection()->addCss($file, $local, true, $attributes);
    }

    /**
     * @return \Phalcon\Assets\Collection
     */
    public function getHeaderCollection() {

        if (!empty($this->headerCollection)) {

            return $this->headerCollection;
        }

        $this->headerCollection = $this->collection('header');

        return $this->headerCollection;
    }

    /**
     * @return \Phalcon\Assets\Collection
     */
    public function getFooterCollection() {

        if (!empty($this->footerCollection)) {

            return $this->footerCollection;
        }

        $this->footerCollection = $this->useImplicitOutput(false)->collection('footer');

        return $this->footerCollection;
    }

    /**
     * Load Header collection
     */
    public function loadHeaderCollection() {

        /**
         *  @var string Full path to minified file 
         */
        $targetFile = $this->getPublicPath() . $this->getConfig('options.css.file');

        //if minified file exists, add it to header, else generate new header collection
        if (!empty($this->getConfig('options.css.minify')) && !$this->isOlder($targetFile, $this->getConfig('options.css.time'))) {

            $this->addToHeader($this->getConfig('options.css.file'));

            $this->addAutoloadCssFiles();

            return;
        }

        $headerAssets = !empty($this->getConfig('files.css')) ?
                $this->getConfig('files.css')->toArray() :
                [];

        foreach ($headerAssets as $value) {

            $this->addToHeader($value);
        }

        // do not minify css file
        if (empty($this->getConfig('options.css.minify'))) {

            //add autoloaded files
            $this->addAutoloadCssFiles();

            return;
        }

        $this->getHeaderCollection()
                ->addFilter(new Cssmin())
                ->setTargetPath($this->getConfig('options.css.file'))
                ->setTargetUri($this->getConfig('options.css.file'));

        //add autoloaded files
        $this->addAutoloadCssFiles();

        return true;
    }

    /**
     * Load Footer collection
     */
    public function loadFooterCollection() {

        /**
         * @todo minify if production only.If min file exists check time 
         */
        $targetFile = $this->getPublicPath() . $this->getConfig('options.js.file');

        //if min file exists, do not loop through collection
        if (!empty($this->getConfig('options.js.minify')) && !$this->isOlder($targetFile, $this->getConfig('options.js.time'))) {

            $this->addToFooter($this->getConfig('options.js.file'));

            $this->addAutoloadJsFiles();

            return;
        }

        $footerAssets = !empty($this->getConfig('files.js')) ?
                $this->getConfig('files.js')->toArray() :
                [];

        foreach ($footerAssets as $value) {
            $this->addToFooter($value);
        }

        // do not minify js file, just return the collection
        if (empty($this->getConfig('options.js.minify'))) {

            $this->addAutoloadJsFiles();

            return;
        }

        $this->getFooterCollection()
                ->addFilter(new Jsmin())
                ->join(true)
                ->setTargetPath($this->getConfig('options.js.file'))
                ->setTargetUri($this->getConfig('options.js.file'));

        $this->addAutoloadJsFiles();

        return true;
    }

    /**
     * Load both Collections ( Header and Footer )to assets manager 
     */
    public function loadCollections() {

        $this->loadHeaderCollection();

        $this->loadFooterCollection();
    }

    /**
     * Create a custom CSS collection 
     * 
     * @param string $name Collection name
     * @param string $files Files to add to collection
     */
    public function createCssCollection($name = false, $files = []) {

        if (empty($name)) {
            return false;
        }

        $collection = $this->collection($name);

        foreach ($files as $value) {

            if (!$this->assetExists($value)) {
                continue;
            }

            $collection->addCss($value);
        }

        return $collection;
    }

    /**
     * Prints the HTML for CSS resources
     *
     * @param string $collectionName collection name to output
     * @return string
     */
    public function outputCss($collectionName = null) {

        //if collection is defined and does not exists into manager
        if ($collectionName && !$this->exists($collectionName)) {
            return false;
        }

        //check if collection name is defined and exists into manager
        if (!empty($collectionName) && $this->exists($collectionName)) {
            return parent::outputCss($collectionName);
        }

        return false;
    }

    /**
     * Prints the HTML for JS resources
     *
     * @param string $collectionName collection name to output
     * @return string
     */
    public function outputJs($collectionName = null) {

        //if collection is defined and does not exists into manager
        if ($collectionName && !$this->exists($collectionName)) {
            return false;
        }

        //check if collection name is defined and exists into manager
        if (!empty($collectionName) && $this->exists($collectionName)) {
            return parent::outputJs($collectionName);
        }

        return false;
    }

    /**
     * Prints in-line CSS style
     *
     * @param string $collectionName collection name to output
     * @return string
     */
    public function outputInlineCss($collectionName = null) {

        //if collection is defined and does not exists into manager
        if ($collectionName && !$this->exists($collectionName)) {
            return false;
        }

        //check if collection name is defined and exists into manager
        if (!empty($collectionName) && $this->exists($collectionName)) {
            return parent::outputInlineCss($collectionName);
        }

        return false;
    }

    /**
     * Prints In-line java script
     *
     * @param string $collectionName collection name to output
     * @return string
     */
    public function outputInlineJs($collectionName = null) {

        //if collection is defined and does not exists into manager
        if ($collectionName && !$this->exists($collectionName)) {
            return false;
        }

        //check if collection name is defined and exists into manager
        if (!empty($collectionName) && $this->exists($collectionName)) {
            return parent::outputInlineJs($collectionName);
        }

        return false;
    }

    /**
     * 
     * Private function only
     * 
     */

    /**
     * Check the minified file time. If older than cache configured time, minify again
     * 
     * @param string $minified Path to the minified file
     * @param int $time Time in seconds
     * 
     * @return bool false if is in time
     */
    private function isOlder($minified, $time) {

        // return to re-minify the file
        if (!file_exists($minified) || (file_exists($minified) && (time() - filemtime($minified)) > $time)) {

            return true;
        }

        return false;
    }

    /**
     * Check if asset file exists
     * @param string $file path to asset file relative to public folder
     */
    private function assetExists($file) {

        return $this->isExternalResource($file) || file_exists($this->getPublicPath() . $file);
    }

    /**
     * Check whenever the file is external resource
     */
    private function isExternalResource($file) {

        $external = [];

        preg_match('/https:|http:/', $file, $external);

        return !empty($external[0]) ? true : false;
    }

    /**
     * Add CSS files where dynamically loaded
     */
    private function addAutoloadCssFiles() {

        foreach ($this->getAutoload()->path('css') as $autocs) {

            $this->addToHeader($autocs);
        }
    }

    /**
     * Add CSS files where dynamically loaded
     */
    private function addAutoloadJsFiles() {

        foreach ($this->getAutoload()->path('js') as $autojs) {

            $this->addToFooter($autojs);
        }
    }

}
