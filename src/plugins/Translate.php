<?php

namespace PMP\Core\Plugins;

use Phalcon\Config\Adapter\Json AS ConfigJson;
use Phalcon\Config AS ConfigArray;
use PMP\Core\Plugins\PluginInterface;
use PMP\Core\Models\SystemTranslation;
use PMP\Core\Library\FileSystem;

class Translate extends PluginInterface {

    /**
     * @var bool Set whenever to use translation or not
     */
    public $useTranslation = false;

    /**
     * @var string Source type (db,json,php)
     */
    public $source = 'db';

    /**
     * @var string translated files named convention (messages.json, messages.php )
     */
    public $domain = 'messages';

    /**
     * @var string folder location where the files are stored
     */
    public $location;

    /**
     * @var string
     */
    public $defaultLocale = 'en';

    /**
     * @var \Phalcon\Config
     */
    static $translated = [];

    /**
     * @param \Phalcon\Config $config [location, domain, defaultLocale, source]
     */
    public function setConfig($config) {

        foreach ($config as $property => $value) {

            if (property_exists(get_class(), $property)) {

                $this->{$property} = $value;
            }
        }
    }

    /**
     * @return string Locale from cookie or default
     */
    public function getLocale() {

        return $this->cookies->has('lang') ?
                $this->cookies->get('lang')->getValue() :
                $this->defaultLocale;
    }

    /**
     * Set into cookie the preferred language
     * 
     * @return string Local or default if parameter is not set
     */
    public function setLocale($locale = false) {

        $lc = !empty($locale) ? $locale : $this->defaultLocale;

        $this->cookies->set('lang', $lc);
    }

    /**
     * Load language
     */
    public function load() {

        if (empty($this->useTranslation)) {

            return false;
        }

        $this->source == 'db' ?
                        $this->loadFromDatabase() :
                        $this->loadFromFile();
    }

    /**
     * Load translation from data base using cache
     * @param \Phalcon\Mvc\Model $modelClassName
     * 
     * @return \Phalcon\Config
     */
    private function loadFromDatabase() {

        $cacheKey = $this->getCacheKey();

        if ($this->cache->isCacheable('translate', $cacheKey)) {

            $translation = $this->cache->getCache($cacheKey);
        } else {

            //get translation from db
            $translation = SystemTranslation::findByCode($this->getLocale());

            $this->cache->setCache('translate', $translation, $cacheKey);
        }

        $translated = new ConfigArray([]);

        foreach ($translation as $value) {

            $translated->offsetSet($value['word'], $value['value']);
        }

        self::$translated = $translated;

        return $translated;
    }

    /**
     * Load translation from file<br />
     * If file not exists, get the languages from dB and write new file
     * 
     * app/languages/en/messages.json
     */
    private function loadFromFile() {

        $file = $this->location . '/' . $this->getLocale() . '/' . $this->domain . '.json';

        //if file with translation exists
        if (file_exists($file)) {

            self::$translated = new ConfigJson($file);

            return true;
        }

        $translation = $this->loadFromDatabase();

        FileSystem::writeFile(
                $file, json_encode($translation->toArray(), JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT)
        );
    }

    /**
     * @return string Cached key used when cache language
     */
    public function getCacheKey() {

        return '-translate-' . $this->getLocale();
    }

    /**
     * Volt function
     */
    static function t($var) {

        return self::$translated->path($var, $var);
    }

}
