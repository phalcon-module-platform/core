<?php

namespace PMP\Core;

use PMP\Core\Providers\Engine\VoltEngine;
use PMP\Core\ModulesInterface;
use Phalcon\Mvc\View;
use Phalcon\DiInterface;
use PMP\Core\Library\FileSystem;


class Api extends ModulesInterface {

    /**
     * Loads the shared services
     * @param \Phalcon\Di\FactoryDefault $di
     */
    public function registerServices(DiInterface $di = null) {

        //load shared services where managed from admin panel
        foreach ($this->systemConfig->path('sharedServices', []) as $key => $value) {

            $di->setShared($key, $value->toArray());
        }

        $services = [
            \PMP\Core\Services\Db::class,
            \PMP\Core\Services\SystemMessages::class,
            \PMP\Core\Services\Translate::class,
            \PMP\Core\Services\Console::class,
            \PMP\Core\Services\MailManagerService::class,
            \PMP\Core\Services\Filters::class,
            \PMP\Core\Services\Response::class
        ];

        foreach ($services as $service) {
            $di->register(new $service());
        }

        //share specific module services
        foreach ($this->modulConfig->path('sharedServices', []) as $specificService) {
            $di->register(new $specificService());
        }
        
        $this->registerApiDispatcher($di);

        $this->registerView($di);
    }

    /**
     * Setting up the view component
     */
    private function registerView(DiInterface $di = null) {

        $config = $this->modulConfig;

        $moduleDirs = $this->moduleDirs;

        $di->setShared('view', function () use ($config, $moduleDirs) {

            $view = new View();

            $sysconfig = $this->getSystemConfig();

            $view->setViewsDir($moduleDirs->path('layoutDir'));

            //sets layout based on module
            $view->setLayoutsDir($moduleDirs->path('layoutDir'));

            $view->setPartialsDir($moduleDirs->path('partialDir'));

            $view->setTemplateBefore($config->path('layout'));
            
            //start engine
            $volt = new VoltEngine($view, $this);

            $compiledPath = FileSystem::makeDir($sysconfig->getLayoutsCachelDir());

            $volt->setOptions([
                'always' => false,
                'extension' => '.volt',
                'compiledPath' => $compiledPath,
                'compiledSeparator' => '_'
            ]);

            $volt->registerFunctions();

            $engine = $volt->getEngine();

            $view->registerEngines($engine);

            return $view;
        });
    }

}
