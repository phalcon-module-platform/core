**Module-based platform written on Phalcon Framework**

**Installation**

1. Add these lines to your composer.json file 

````
    "require": {
        "phalcon-module-platform/core": "dev-master"
    },
    "require-dev": {
        "phalcon/ide-stubs": "3.4"
    },
    "autoload": {
        "psr-4": {
            "App\\": "app/",
            "Console\\": "console/"
        },
        "classmap": [
            "app/",
            "console/"
        ]
    },
    "scripts": {
        "pmp": "PMP\\Core\\Scripts\\ApplicationController::load"        
    }

````

2. Run composer update (ignore the warning about scan for classes inside "app/")

````
composer update
````

3. Install basic application with Admin Panel ('composer pmp help' shows all the options)

````
composer pmp install application
````

4. Create an empty database and execute the command bellow(answer all the questions)

````
composer pmp config init
````

5. Find the path to your phalcon-devtools and use it to migrate the core tables. Run the following command and answer Y

````
php /path/to/phalcon-devtools/phalcon.php migration run --migrations app/migrations/core/
````

